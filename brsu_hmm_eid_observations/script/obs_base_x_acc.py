#!/usr/bin/env python
import roslib; roslib.load_manifest('brsu_hmm_eid_observations')
import rospy
import math
import yaml
import collections
#from std_msgs.msg import String
from std_msgs.msg import *
from nav_msgs.msg import Odometry
from geometry_msgs.msg import *

from brsu_hmm_eid_messages.msg import obs_base_x_vel_diff, obs_base_x_vel_cmd, obs_base_x_acc, obs_base_x_jerk


class ob_base_x_acc():
    def __init__(self):

        self.duration_acc = 4
        self.duration_jerk = 8
        
        self.base_x_vel = collections.deque(maxlen = self.duration_jerk)
        self.base_x_acc = collections.deque(maxlen = self.duration_jerk)
        
        for i in xrange(self.duration_acc):
            self.base_x_vel.append(0)
            
        for i in xrange(self.duration_jerk):
            self.base_x_acc.append(0)
        
        self.base_x_vel_cmd = 0
        self.base_x_vel_diff = 0
        self.base_x_jerk = 0
        
    def listener(self):
        
        rospy.Subscriber("odom", Odometry, self.odom_callback)
        rospy.Subscriber("cmd_vel", Twist, self.cmd_vel_callback)
        
        
    def odom_callback(self, msg):

        t = rospy.Time.now().secs        
        self.base_x_vel.append(msg.twist.twist.linear.x)
        
        self.calc_base_x_acc(t)
        self.calc_base_x_jerk(t)
        
    
    def cmd_vel_callback(self, msg):
        
        if msg.linear.x is None:
            self.base_x_vel_cmd = 0
        else:
            self.base_x_vel_cmd = msg.linear.x
        
        self.calc_base_x_vel_diff()
        
    def calc_base_x_vel_diff(self):
        
        self.base_x_vel_diff = self.base_x_vel_cmd - self.base_x_vel[self.duration_acc - 1]
        
    def calc_base_x_acc(self, t):
        
        diff = int(t) % self.duration_acc
        
        if int(t) < self.duration_acc:
            self.base_x_acc = 0
        
        elif diff == 0 :
           acc_x = (self.base_x_vel[self.duration_acc - 1] - self.base_x_vel[0])/self.duration_acc
           self.base_x_acc.append(acc_x)
    
    def calc_base_x_jerk(self, t):
        
        diff = int(t) % self.duration_jerk
        
        if int(t) < self.duration_jerk:
            self.base_x_jerk = 0
        
        elif diff == 0 :
           #jerk_x = (self.base_x_acc[self.duration_jerk - 1] - self.base_x_acc[0])/self.duration_jerk
           jerk_x = ((self.base_x_vel[self.duration_jerk - 1] - self.base_x_vel[0])/self.duration_jerk)/self.duration_jerk
           self.base_x_jerk = jerk_x

    def talker(self):
        #rospy.loginfo("x_acceleration: %f"%acc_x)
        pub1 = rospy.Publisher('/brsu_hmm_eid_observations/base_x_vel_cmd', obs_base_x_vel_cmd)#, latch=True)
        pub2 = rospy.Publisher('/brsu_hmm_eid_observations/base_x_vel_diff', obs_base_x_vel_diff)
        pub3 = rospy.Publisher('/brsu_hmm_eid_observations/base_x_acc', obs_base_x_acc)
        pub4 = rospy.Publisher('/brsu_hmm_eid_observations/base_x_jerk', obs_base_x_jerk)
        
        msg2send1 = obs_base_x_vel_cmd()
        msg2send1.header.stamp.secs = rospy.Time.now().secs
        msg2send1.x_cmd_vel = self.base_x_vel_cmd
        pub1.publish(msg2send1)
        
        msg2send2 = obs_base_x_vel_diff()
        msg2send2.header.stamp.secs = rospy.Time.now().secs
        msg2send2.x_vel_diff = self.base_x_vel_diff
        pub2.publish(msg2send2)
        
        msg2send3 = obs_base_x_acc()
        msg2send3.header.stamp.secs = rospy.Time.now().secs
        msg2send3.x_acc = self.base_x_acc
        pub3.publish(msg2send3)

        msg2send4 = obs_base_x_jerk()
        msg2send4.header.stamp.secs = rospy.Time.now().secs
        msg2send4.x_jerk = self.base_x_jerk
        pub4.publish(msg2send4)
    
        rospy.loginfo('published base x all')
        rospy.sleep(1)


if __name__ == '__main__':
    rospy.init_node('brsu_hmm_eid_observations_node_x_acc', anonymous=True)
    
    obxa = ob_base_x_acc()
    
    while not rospy.is_shutdown():
        try:
            obxa.listener()
            obxa.talker()
            rospy.spin()
        except rospy.ROSInterruptException:
            pass
