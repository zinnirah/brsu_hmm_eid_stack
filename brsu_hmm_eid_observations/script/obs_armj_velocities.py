#!/usr/bin/env python

import roslib; roslib.load_manifest('brsu_hmm_eid_observations')
import roslib.message
import rospy
import yaml

from sensor_msgs.msg import JointState
from brsu_hmm_eid_messages.msg import obs_arm_JointVelocities_rel_diff

armj_vel_init = [0]*5
#armj_vel_rel_diff = [1]*5
def callback_1(msg):
    rospy.loginfo("Received joint velocities command:")# +str(msg.velocities)[1:-1])
    arm_joint_vel_cmd = msg.velocities
    print arm_joint_vel_cmd
    if arm_joint_vel_cmd == None:
        arm_joint_vel_cmd = [0] * len(arm_joint_vel_cmd)
    sajvc = store_arm_joint_vel_cmd()
    sajvc.write_file(arm_joint_vel_cmd)

def callback(msg):
    global armj_vel_init
    armj_vel_rel_diff =[0]*5
    t = rospy.Time.now().secs

    #rospy.loginfo("Received actual joint velocities actual: ")# +str(msg.velocity)[1:-1])
     
    names_array = msg.name
    arm_name_index = names_array.index('arm_joint_1')
    armj_vel_act = msg.velocity[arm_name_index:arm_name_index+5]
    #print armj_vel_act
    
    if t == 0:
        if armj_vel_init is None:
            armj_vel_init = [0] * len(armj_vel_act)
        armj_vel_init = [0]*5
        armj_vel_rel_diff = [0]*5
    else:
        for each in range(len(armj_vel_act)):
            armj_vel_rel_diff[each] = armj_vel_act[each] - armj_vel_init[each]
    
    talker(armj_vel_rel_diff)
    armj_vel_init = armj_vel_act 


def listener():
    rospy.Subscriber("/joint_states", JointState, callback)

def talker(armj_vel_rel_diff):

    time_now = rospy.Time.now().secs
    #print armj_vel_rel_diff
    pub = rospy.Publisher('/brsu_hmm_eid_observations/armj_diff_velocities', obs_arm_JointVelocities_rel_diff)#obs_arm.diff_vel)
    
    msg2send = obs_arm_JointVelocities_rel_diff()
    msg2send.header.stamp.secs = time_now
    msg2send.vel_rel_diff = armj_vel_rel_diff
    pub.publish(msg2send)

if __name__ == '__main__':
    rospy.init_node('armj_velocity_difference', anonymous=True)

    while not rospy.is_shutdown():
        try:
            listener()
            rospy.spin()
        except rospy.ROSInterruptException:
            pass
