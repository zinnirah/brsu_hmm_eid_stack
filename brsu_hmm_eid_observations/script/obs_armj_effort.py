#!/usr/bin/env python
import roslib; roslib.load_manifest('observation_monitor')
import roslib.message 
import rospy
import yaml
from types import *

from sensor_msgs.msg import JointState
from brics_actuator.msg import JointTorques

armj_effort_cmd = [0]*5
armj_effort_act = [0]*5

class store_init_effort:
    def __init__(self):
        self.eff_init= 0
        #print self.vel_init_x
        
    def write_file(self, eff_init):
        self.eff_init=eff_init
        eff_init_file = open('../objects/eff_init.yaml', 'w')
        eff_init_details = self.eff_init
        yaml.dump(eff_init_details,eff_init_file)
        eff_init_file.close()

    def read_file(self):
        read_yaml_file = open('../objects/eff_init.yaml', 'r')
        pass_eff_init = yaml.load(read_yaml_file)
        #rospy.loginfo("inthe file: %f", yaml.load(read_yaml_file))
        #print pass_eff_init
        read_yaml_file.close()
        return pass_eff_init

def callback_1(msg):
    
    rospy.loginfo("Received actual joint effort commanded: ")# +str(msg.velocity)[1:-1])
    #sie = store_init_effort()
    
    arm_joint_effort_cmd = msg.torques.value
    #if arm_joint_effort_cmd is None:
    #    arm_joint_effort_cmd = [0] * len(arm_joint_effort_cmd)

    #arm_joint_eff_act= sie.read_file()
    #effort_diff_ca = abs(arm_joint_effort_cmd - arm_joint_eff_act)


def callback_2(msg):
    rospy.loginfo("Received actual joint effort actual: ")# +str(msg.velocity)[1:-1])

    names_array = msg.name
    #print names_array
    arm_name_index = names_array.index('arm_joint_1')
    #print arm_name_index
    #sie = store_init_effort()
        
    if rospy.Time.now() == 0:
        arm_joint_eff_act_init = msg.effort[arm_name_index:arm_name_index+5]
        if arm_joint_eff_act_init is None:
            arm_joint_eff_act_init = [0] * len(arm_joint_eff_act_init)
        sie.write_file(arm_joint_eff_act_init)
        
    else:
        #how do i get the the vel_init?
        arm_joint_eff_act_init = armj_#sie.read_file()
        arm_joint_eff_act_last = msg.effort[arm_name_index:arm_name_index+5]
        effort_diff = arm_joint_eff_act_last - arm_joint_eff_act_init
        sie.write_file(arm_joint_eff_act_last)
    

def listener():
    rospy.Subscriber("/joint_states", JointState, callback_2) 
    rospy.Subscriber("/arm_controller/torque_command", JointTorques, callback_1)
    talker()
    
def talker():

    # 
    # difference = cmd -act 
    # rospy.loginfo("difference: %f"%difference)
    # pub = rospy.Publisher('x_velocity_difference', Float64 )
    # pub.publish(difference)
    #===========================================================================
    sajvc = store_arm_joint_vel_cmd()
    cmd = sajvc.read_file()
    rospy.loginfo(cmd)
    
    sajva = store_arm_joint_vel_act()
    act =sajva.read_file()
    rospy.loginfo(act)
    
if __name__ == '__main__':
    rospy.init_node('arm_effort', anonymous=True)
    
    while not rospy.is_shutdown():
        try:
            listener()
            rospy.sleep(1)
        except rospy.ROSInterruptException:
            pass

#TODO: 
#get joint numbers automatically