#!/usr/bin/env python
import roslib; roslib.load_manifest('brsu_hmm_eid_observations')
import rospy
#import rqt_plot

from hbrs_srvs.srv import ReturnBool
from brsu_hmm_eid_messages.msg import obs_gripper_status

class gripper_obs():
    def __init__(self):
        self.gripper_status_num = 1
                
    def openclose_obs(self):
        
        #rospy.loginfo("Wait for service << /gripper_controller/is_gripper_closed >>")#+/gripper_controller/is_gripper_closed)
        rospy.wait_for_service("/gripper_controller/is_gripper_closed")
    
        #TODO: get type of gripper status topic
        try:
            gripper_status = rospy.ServiceProxy('/gripper_controller/is_gripper_closed', ReturnBool)
            #print gripper_status.call()
            #state = gripper_status.call().value
            #print state
            
            if bool(gripper_status.call().value) == True:
                rospy.loginfo('Gripper closed')
                self.gripper_status_num = 1
                #return True
            
            
            elif bool(gripper_status.call().value) == False:
                rospy.loginfo('Gripper open')
                self.gripper_status_num = 0
                #return False
            
            else:
                print 'Midway'
                self.gripper_status_num = 2
                    
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e
        
        self.talker()
        
    def gripperposition_obs(self): #applies for grippers like schunk
        print "empty <<gripperposition_obs>> function"
        
    def tactile_obs(self): #applies for grippers like schunk
        print "empty <<tactile_obs>> function"
    

    def talker(self):
        
        pub1 = rospy.Publisher('/brsu_hmm_eid_observations/gripper_status',  obs_gripper_status)
        
        msg2send1 = obs_gripper_status()
        msg2send1.header.stamp.secs = rospy.Time.now().secs
        msg2send1.gripper_status = self.gripper_status_num
        pub1.publish(msg2send1)

    
if __name__ == '__main__':
    rospy.init_node('gripper_observation_monitor')
    
    go=gripper_obs()
    while not rospy.is_shutdown():
        try:      
            go.openclose_obs()
            rospy.sleep(1) #Comment this on a real robot to get faster response.
        except rospy.ROSInterruptException:
            pass