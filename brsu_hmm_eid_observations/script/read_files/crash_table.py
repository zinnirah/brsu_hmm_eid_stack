#!/usr/bin/env python

import roslib; roslib.load_manifest('brsu_hmm_eid_observations')
import roslib.message
import rospy
import yaml
import message_filters


from pylab import plot,show
from scipy import stats
import numpy as np
import collections
#from types import *

from rospy.numpy_msg import numpy_msg
from sensor_msgs.msg import JointState
from brsu_hmm_eid_messages.msg import obs_arm, obs_arm_JointPositions, obs_arm_JointVelocities, obs_arm_JointTorques, obs_arm_JointAccelerations, obs_arm_JointJerks, obs_arm_JointPositions_rel_diff, obs_arm_JointTorques_rel_diff, obs_gripper_pos_diff
from brics_actuator.msg import JointPositions, JointVelocities, JointTorques

class obs_armj_all():
    def __init__(self):
        
        #Youbot
        #self.joint_names = ["arm_joint_1", "arm_joint_2", "arm_joint_3", "arm_joint_4", "arm_joint_5","gripper_finger_joint_l","gripper_finger_joint_r"]
        #Jenny
        self.joint_names = ['arm_1_joint', 'arm_2_joint', 'arm_3_joint', 'arm_4_joint', 'arm_5_joint', 'arm_6_joint', 'arm_7_joint']
        

    def listener(self):

        rospy.Subscriber("/joint_states", JointState, self.callback)

    def callback(self, armj_act_states):
        
        a=[]
        names_array = armj_act_states.name
        if (names_array  == self.joint_names):
            arm_name_index = names_array.index('arm_1_joint')
        
            self.armj_names = names_array[arm_name_index:arm_name_index+5]
            a.append([armj_act_states.velocity[0],armj_act_states.velocity[1],armj_act_states.velocity[2],armj_act_states.velocity[3],armj_act_states.velocity[4],armj_act_states.velocity[5],armj_act_states.velocity[6],
                      armj_act_states.effort[0],armj_act_states.effort[1],armj_act_states.effort[2],armj_act_states.effort[3],armj_act_states.effort[4],armj_act_states.effort[5],armj_act_states.effort[6]])
        
        #f = open('crash_table.txt', 'a')
        #f = open('grasping_pull_out_of_hand.txt', 'a')
        #f = open('grasping_reference.txt', 'a')
        #f = open('grasping_shaking_fail.txt', 'a')
        #f = open('hard_stiffness_1st_run.txt', 'a')
        #f = open('hard_stiffness_2nd_run.txt', 'a')
        #f = open('hard_stiffness_no_forces.txt', 'a')
        #f = open('low_stiffness_1st_run.txt', 'a')
        #f = open('low_stiffness_2nd_run.txt', 'a')
        #f = open('low_stiffness_no_forces.txt', 'a')
        #f = open('medium_stiffness_1st_run.txt', 'a')
        #f = open('medium_stiffness_2nd_run.txt', 'a')
        f = open('medium_stiffness_no_forces.txt', 'a')
        
        for each in range(len(a)):
            aa = str(a[each][0])+','+str(a[each][1])+','+str(a[each][2])+','+str(a[each][3])+','+str(a[each][4])+','+str(a[each][5])+','+str(a[each][6])+','+str(a[each][7])+','+str(a[each][8])+','+str(a[each][9])+','+str(a[each][10])+','+str(a[each][11])+','+str(a[each][12])+','+str(a[each][13])+'\n'
            f.write(aa)
        f.close()
            
if __name__ == '__main__':

    rospy.init_node('brsu_hmm_eid_node_armj_observation_all', anonymous=True)
    oaa = obs_armj_all()
    #r = rospy.Rate(1)#100
    while not rospy.is_shutdown():
        try:
            oaa.listener()
            #r.sleep()
            rospy.sleep(1)
            #rospy.spin()
        except rospy.ROSInterruptException:
            pass