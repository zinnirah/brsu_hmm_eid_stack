#!/usr/bin/env python
import roslib; roslib.load_manifest('brsu_hmm_eid_observations')
import rospy
import math
import yaml
#from std_msgs.msg import String
from std_msgs.msg import *
from nav_msgs.msg import Odometry
from geometry_msgs.msg import *

from brsu_hmm_eid_messages.msg import obs_base_y_acc

vel_init_y = 0 # base initial velocities in the x direction
acc_y = 0 # base acceleration in the x direction
duration = 4


    
def callback(msg):
    #rospy.loginfo("Received actual side velocity <</odom>> message! The value is %f" %msg.twist.twist.linear.y)
    #svi = store_vel_init_y()
    #say = store_acc_y()
    #svi.write_file(vel_init_x = 0)ros
    global vel_init_y
    global acc_y
    
    t = rospy.Time.now().secs
    #duration = 4
    diff = int(t) % duration
    
    if int(t) < duration:
        acc_y = 0
        if t == 0 :
            vel_init_y = msg.twist.twist.linear.y
            if vel_init_y is None:
                vel_init_y = 0
        else :
            vel_init_y = 0
        
        #svi.write_file(vel_init_y)
        #acceleration_y = 0
        #say.write_file(acceleration_y)
       
    elif diff == 0 :
        #vel_init_y = svi.read_file()
        if vel_init_y is None:
            vel_init_y = 0
        vel_last_y = msg.twist.twist.linear.y
        if vel_last_y is None:
            vel_last_y = 0
        acc_y = (vel_last_y - vel_init_y) / duration
        vel_init_y = vel_last_y  
        #say.write_file(acceleration_y)
        #svi.write_file(vel_last_y)

    else :
        #acceleration_y = say.read_file()
        pass    
      
    talker(acc_y)

def talker(acc_y):
    #rospy.loginfo("y_acceleration: %f"%acc_y)
    pub = rospy.Publisher('/brsu_hmm_eid_observations/base_y_acc', obs_base_y_acc, latch= True)

    msg2send = obs_base_y_acc()
    msg2send.header.stamp.secs = rospy.Time.now().secs
    msg2send.y_acc = acc_y
    pub.publish(msg2send)

def listener():
    rospy.Subscriber("odom", Odometry, callback)

if __name__ == '__main__':
    rospy.init_node('brsu_hmm_eid_observations_node_base_y_acc', anonymous=True)

    while not rospy.is_shutdown():
        try:
            listener()
            rospy.sleep(1)
        except rospy.ROSInterruptException:
            pass
