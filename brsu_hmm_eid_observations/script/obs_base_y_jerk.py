#!/usr/bin/env python
import roslib; roslib.load_manifest('brsu_hmm_eid_observations')
import rospy
import math
import yaml
#from std_msgs.msg import String
from std_msgs.msg import *
from nav_msgs.msg import Odometry
from geometry_msgs.msg import *

from brsu_hmm_eid_messages.msg import obs_base_y_jerk, obs_base_y_acc

acc_init_y = 0
jerk_y = 0
duration = 8
    
def callback(msg):
    #rospy.loginfo("Received actual forward acc <</odom>> message! The value is %f" %msg.y_acc)
    
    #saiy = store_acc_init_y()
    #sjy = store_jerk_y()
    global acc_init_y
    global jerk_y
    
    t = rospy.Time.now().secs
    #duration = 8
    diff = int(t) % duration
    
    if int(t) < duration:
        jerk_y = 0
        if t == 0 :
            acc_init_y = msg.y_acc
        else :
            acc_init_y = 0
        #print acc_init
        #saiy.write_file(acc_init_y)
        #sjy.write_file(jerk_y)
    elif diff == 0:
        #how do i get the the vel_init?
        #acc_init_y = saiy.read_file()
        acc_last_y = msg.y_acc
        #print acc_last_y
        jerk_y = (acc_last_y - acc_init_y) / duration
        acc_ini_y = acc_last_y
        #saiy.write_file(acc_last_y)
        #sjy.write_file(jerk_y)
    else:
        #jerk_y = sjy.read_file()
        pass
    
    talker(jerk_y)

def talker(jerk_y):
    rospy.loginfo("y_jerk: %f"%jerk_y)
    pub = rospy.Publisher('/brsu_hmm_eid_observations/base_y_jerk', obs_base_y_jerk)

    msg2send = obs_base_y_jerk()
    msg2send.header.stamp.secs = rospy.Time.now().secs
    msg2send.y_jerk= jerk_y
    pub.publish(msg2send)

def listener():
    rospy.Subscriber('/brsu_hmm_eid_observations/base_y_acc', obs_base_y_acc, callback)

if __name__ == '__main__':
    rospy.init_node('brsu_hmm_eid_observations_node_base_y_jerk', anonymous=True)
    while not rospy.is_shutdown():
        try:
            listener()
            rospy.sleep(duration)
        except rospy.ROSInterruptException:
            pass
