#!/usr/bin/env python
import roslib; roslib.load_manifest('brsu_hmm_eid_observations')
import rospy
import collections
import numpy
import pylab

from nav_msgs.msg import Odometry
from geometry_msgs.msg import Twist

from brsu_hmm_eid_messages.msg import obs_base_x_vel_diff, obs_base_x_vel_cmd, obs_base_x_acc, obs_base_x_jerk,  obs_base_x_vel_act
class get_obs_base_x_all():
    def __init__(self):

        self.duration_acc = 2
        self.duration_jerk = 4
        self.beta = [0.125, 0.25,0.5,1,2,4,8,16,32]
        self.sz_acc = 60#125
        self.sz_jerk = 100#200

        self.base_x_vel = collections.deque(maxlen = self.sz_acc)
        self.base_x_acc = collections.deque(maxlen = self.sz_jerk)

        for i in xrange(self.sz_acc):
            self.base_x_vel.append(0)

        for i in xrange(self.sz_jerk):
            self.base_x_acc.append(0)

        self.base_x_vel_cmd = 0
        self.base_x_vel_diff = 0
        self.base_x_jerk = 0
        
    def smooth(self,x,beta):
        #""" kaiser window smoothing """
        window_len=50
        print x
        # extending the data at beginning and at the end
        # to apply the window at the borders
        #numpy.r
        s = numpy.r_[x[window_len-1],x,x[-window_len]]
        w = numpy.kaiser(window_len,beta)
        y = numpy.convolve(w/w.sum(),s,mode='valid')
        return y[5:len(y)-5]

    def listener(self):
        
        rospy.sleep(1)
        rospy.Subscriber("odom", Odometry, self.odom_callback)
        rospy.Subscriber("cmd_vel", Twist, self.cmd_vel_callback)
        #self.talker()

    def odom_callback(self, msg):

        t = rospy.Time.now().secs
        self.base_x_vel.append(msg.twist.twist.linear.x)
        y = self.base_x_vel
        #print y

        # smoothing the data
        pylab.figure(1)
        pylab.plot(y,'-k',label="original signal",alpha=.3)
        for b in self.beta:
            yy = self.smooth(y,b) 
            pylab.plot(yy,label="filtered (beta = "+str(b)+")")
        pylab.legend()
        pylab.show()
        print self.base_x_vel[self.duration_jerk-1]
        self.calc_base_x_acc(t)
        self.calc_base_x_jerk(t)
        


    def cmd_vel_callback(self, msg):

        if msg.linear.x is None:
            self.base_x_vel_cmd = 0
        else:
            self.base_x_vel_cmd = msg.linear.x
            

        self.calc_base_x_vel_diff()

    def calc_base_x_vel_diff(self):
        
        #rospy.sleep(1)
        self.base_x_vel_diff = self.base_x_vel_cmd - self.base_x_vel[self.sz_acc - 1]
        #print 'vel_diff', self.base_x_vel_diff

    def calc_base_x_acc(self, t):
        
        #rospy.sleep(self.duration_acc)
        #diff = int(t) % self.duration_acc

        #if int(t) < self.duration_acc:
        #    self.base_x_acc.append(0)

        #elif diff == 0 :
        #else:
        #rospy.sleep(1)
        acc_x = (self.base_x_vel[self.sz_acc - 1] - self.base_x_vel[0])#/self.duration_acc
        #print self.base_x_vel[self.sz_acc - 1] 
        #print self.base_x_vel[0]
            #print 'acc_x', acc_x
            #rospy.sleep(1)
        self.base_x_acc.append(acc_x)
            #print self.base_x_acc[self.duration_jerk - 1]

    def calc_base_x_jerk(self, t):

        #diff = int(t) % self.duration_jerk

        #if int(t) < self.duration_jerk:
        #    self.base_x_jerk = 0

        #elif diff == 0 :
        jerk_x = (self.base_x_acc[self.sz_jerk - 1] - self.base_x_acc[0])#/self.duration_jerk
        #jerk_x = (self.base_x_vel[self.sz_acc - 1] - self.base_x_vel[0])#/self.duration_jerk)/self.duration_jerk
           #print 'jerk_x',jerk_x
        self.base_x_jerk = jerk_x
           #print self.base_x_jerk


    def talker(self):
        #rospy.loginfo("Publishing obs_base_x_all")

        time_now = rospy.Time.now().secs
        pub1 = rospy.Publisher('/brsu_hmm_eid_observations/base_x_vel_cmd', obs_base_x_vel_cmd)#, latch=True)
        pub2 = rospy.Publisher('/brsu_hmm_eid_observations/base_x_vel_diff', obs_base_x_vel_diff)
        pub3 = rospy.Publisher('/brsu_hmm_eid_observations/base_x_acc', obs_base_x_acc)
        pub4 = rospy.Publisher('/brsu_hmm_eid_observations/base_x_jerk', obs_base_x_jerk)
        pub5 = rospy.Publisher('/brsu_hmm_eid_observations/base_x_vel_act', obs_base_x_vel_act)

        msg2send1 = obs_base_x_vel_cmd()
        msg2send1.header.stamp.secs = rospy.Time.now().secs
        msg2send1.x_cmd_vel = self.base_x_vel_cmd
        pub1.publish(msg2send1)
        #print 'self.base_x_vel_cmd', self.base_x_vel_cmd

        msg2send2 = obs_base_x_vel_diff()
        msg2send2.header.stamp.secs = rospy.Time.now().secs
        msg2send2.x_vel_diff = self.base_x_vel_diff
        pub2.publish(msg2send2)
        #print 'self.base_x_vel_dif', self.base_x_vel_diff

        msg2send3 = obs_base_x_acc()
        msg2send3.header.stamp.secs = time_now
        msg2send3.x_acc = self.base_x_acc[self.sz_jerk - 1 ]#[self.duration_jerk - 1]
        pub3.publish(msg2send3)
        #print 'self.base_x_acc[self.sz_jerk - 1]', self.base_x_acc[self.sz_jerk - 1]

        msg2send4 = obs_base_x_jerk()
        msg2send4.header.stamp.secs = time_now
        msg2send4.x_jerk = self.base_x_jerk
        pub4.publish(msg2send4)
        #print 'self.base_x_jerk', self.base_x_jerk
        
        msg2send5 = obs_base_x_vel_act()
        msg2send5.header.stamp.secs = time_now
        msg2send5.x_vel_act = self.base_x_vel[self.sz_acc - 1 ]#[self.duration_jerk - 1]
        pub5.publish(msg2send5)
        #print 'self.base_x_jerk', self.base_x_jerk

        rospy.loginfo('published base x all')
        
        



if __name__ == '__main__':
    rospy.init_node('brsu_hmm_eid_observations_node_x_all', anonymous=True)

    gobxa = get_obs_base_x_all()
    r = rospy.Rate(1)
    while not rospy.is_shutdown():
        try:
            gobxa.listener()
            #gobxa.talker()
            #rospy.sleep(1)
        except rospy.ROSInterruptException:
            pass