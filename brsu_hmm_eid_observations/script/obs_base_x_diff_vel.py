#!/usr/bin/env python
import roslib; roslib.load_manifest('brsu_hmm_eid_observations')
import rospy
import math
import yaml
#from std_msgs.msg import String
from std_msgs.msg import *
from nav_msgs.msg import Odometry
from geometry_msgs.msg import *

from brsu_hmm_eid_messages.msg import obs_base_x_vel_diff, obs_base_x_vel_cmd

vel_cmd_x = 0
vel_act_x = 0

def callback_1(msg):
    #rospy.loginfo("Received forward velocity <</cmd_vel>> message! The value is %f" %msg.linear.x)
    global vel_cmd_x
    vel_cmd_x = msg.linear.x
    #scx = store_cmd_x()

    if vel_cmd_x is None:
        if rospy.Time.now().secs == 0:
            #scx.write_file(vel_cmd_x=0)
            vel_cmd_x = 0
        else:
            pass
    else:
        vel_cmd_x = msg.linear.x
        #scx.write_file(vel_cmd_x)

def callback_2(msg):
    #rospy.loginfo("Received actual x <</odom>> message! The value is %f"%msg.twist.twist.linear.x)
    global vel_act_x
    vel_act_x = msg.twist.twist.linear.x
    #sax = store_act_x()
    
    if vel_act_x is None:
        #scx.write_file(vel_act_x=0)
        vel_act_x = 0
    else:
        vel_act_x = msg.twist.twist.linear.x
        #sax.write_file(vel_act_x)

def listener():
    rospy.Subscriber("cmd_vel", Twist, callback_1)
    rospy.Subscriber("odom", Odometry, callback_2)
def talker():
    #scx = store_cmd_x()
    #sax = store_act_x()

    cmd = vel_cmd_x #scx.read_file()
    print 'cmd:',cmd
    act = vel_act_x #sax.read_file()
    print 'act:',act

    #try:
    if cmd is None:
        cmd = 0
    if act is None:
        act = 0
            
    difference = cmd - act

    rospy.loginfo("difference: %f"%difference)
    pub1 = rospy.Publisher('/brsu_hmm_eid_observations/base_x_vel_diff', obs_base_x_vel_diff)
    pub2 = rospy.Publisher('/brsu_hmm_eid_observations/base_x_vel_cmd', obs_base_x_vel_cmd)#, latch=True)
    
    msg2send1 = obs_base_x_vel_diff()
    msg2send1.header.stamp.secs = rospy.Time.now().secs
    msg2send1.x_vel_diff = difference
    pub1.publish(msg2send1)
    
    msg2send2 = obs_base_x_vel_cmd()
    msg2send2.header.stamp.secs = rospy.Time.now().secs
    msg2send2.x_cmd_vel = cmd
    pub2.publish(msg2send2)


if __name__ == '__main__':
    rospy.init_node('brsu_hmm_eid_observations_node_base_x_vel_diff', anonymous=True)

    while not rospy.is_shutdown():
        try:
            listener()
            talker()
            rospy.sleep(1)
        except rospy.ROSInterruptException:
            pass


