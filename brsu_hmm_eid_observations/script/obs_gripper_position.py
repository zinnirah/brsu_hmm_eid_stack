#!/usr/bin/env python
import roslib; roslib.load_manifest('brsu_hmm_eid_observations')
import roslib.message
import rospy
import yaml
#from std_msgs.msg import String
from brics_actuator.msg import JointPositions
from sensor_msgs.msg import JointState

from brsu_hmm_eid_messages.msg import obs_gripper_pos_diff

gripper_names = rospy.get_param('/script_server/gripper/joint_names') 

class store_gripper_cmd_position():
    def __init__(self):
        self.gr_cmd_pos = [0]*len(gripper_names)

    def write_file(self, gr_cmd_pos):
        self.gr_cmd_pos = gr_cmd_pos
        gr_cmd_pos_file = open('/home/zinnirah/ros/workspace/thesis/brsu_hmm_eid_observations/objects/gr_cmd_pos.yaml', 'w')
        gr_cmd_pos_details = self.gr_cmd_pos
        yaml.dump(gr_cmd_pos_details,gr_cmd_pos_file)
        gr_cmd_pos_file.close()

    def read_file(self):
        read_yaml_file = open('/home/zinnirah/ros/workspace/thesis/brsu_hmm_eid_observations/objects/gr_cmd_pos.yaml', 'r')
        pass_gr_cmd_pos = yaml.load(read_yaml_file)
        read_yaml_file.close()
        return pass_gr_cmd_pos
    
class store_gripper_act_position():
    def __init__(self):
        self.gr_act_pos = []

    def write_file(self, gr_act_pos):
        self.gr_act_pos = gr_act_pos
        gr_act_pos_file = open('/home/zinnirah/ros/workspace/thesis/brsu_hmm_eid_observations/objects/gr_act_pos.yaml', 'w')
        gr_act_pos_details = self.gr_act_pos
        yaml.dump(gr_act_pos_details,gr_act_pos_file)
        gr_act_pos_file.close()

    def read_file(self):
        read_yaml_file = open('/home/zinnirah/ros/workspace/thesis/brsu_hmm_eid_observations/objects/gr_act_pos.yaml', 'r')
        pass_gr_act_pos = yaml.load(read_yaml_file)
        read_yaml_file.close()
        return pass_gr_act_pos
    
def listener():

    rospy.Subscriber("/gripper_controller/position_command", JointPositions, callback_1)
    rospy.Subscriber("/joint_states", JointState, callback_2)

def callback_1(msg):
    sgcp = store_gripper_cmd_position()
    gripper_cmd_positions = [0]*2

    gripper_cmd_positions[0] = msg.positions[0].value
    gripper_cmd_positions[1] = msg.positions[1].value
    
    if gripper_cmd_positions is None:
        if rospy.Time.now().secs == 0:
            gripper_cmd_positions = [0]*len(gripper_names)
            sgp.write_file(gripper_cmd_positions)
        else:
            pass
    else:
        sgcp.write_file(gripper_cmd_positions)
    

def callback_2(msg):
    
    sgap = store_gripper_act_position()
    gripper_act_positions = [0]*2
    
    for k in range(2):
        for i in range(len(msg.name)):
            gripper_name = gripper_names[k]
            if(msg.name[i] == gripper_name):
                gripper_act_positions[k] = msg.position[i]

    #names_array = msg.name
    #gripper_index = names_array.index(gripper_names[0])
    #gripper_act_positions[0] = msg.position[gripper_index]
    #gripper_act_positions[1] = msg.position[gripper_index+1]
    #positions = msg.position(gripper_index:gripper_index+1)
 
    sgap.write_file(gr_act_pos=gripper_act_positions)
  
def talker():

    sgcp = store_gripper_cmd_position()
    sgap = store_gripper_act_position()
    difference = [0]*2
    
    cmd = sgcp.read_file()
    if cmd is None:
        cmd = [0]*2
    print 'cmd',cmd
    act = sgap.read_file()
    if act is None:
        act = [0]*2
    print 'act', act
    
    #try:
    for k in range(2):
        difference[k] = cmd[k] - act[k]
    rospy.loginfo("difference: " +str(difference))
    pub1 = rospy.Publisher('/brsu_hmm_eid_observations/gripper_pos_diff', obs_gripper_pos_diff)
    
    msg2send1 = obs_gripper_pos_diff()
    msg2send1.header.stamp.secs = rospy.Time.now().secs
    msg2send1.gripper_pos_diff = difference
    pub1.publish(msg2send1)


if __name__ == '__main__':
    rospy.init_node('gripper_position', anonymous=True)
    
    while not rospy.is_shutdown():
        try:
            listener()
            talker()
            #print gripper_names[1]
            rospy.sleep(1)
        except rospy.ROSInterruptException:
            pass