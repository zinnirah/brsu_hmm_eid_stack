

#===============================================================================
# BASE X
#===============================================================================


class store_cmd_x():
    def __init__(self):
        self.vel_cmd_x= 0
        #print self.vel_init_x

    def write_file(self, vel_cmd_x):
        #amount = raw_input("How many objects would you like to spawn? ")
        self.vel_cmd_x = vel_cmd_x
        vel_cmd_file = open('/home/zinnirah/ros/workspace/thesis/brsu_hmm_eid_observations/objects/vel_cmd_x.yaml', 'w')
        vel_cmd_details = self.vel_cmd_x
        yaml.dump(vel_cmd_details,vel_cmd_file)
        vel_cmd_file.close()

    def read_file(self):
        read_yaml_file = open('/home/zinnirah/ros/workspace/thesis/brsu_hmm_eid_observations/objects/vel_cmd_x.yaml', 'r')
        pass_velocity_cmd = yaml.load(read_yaml_file)
        #rospy.loginfo("inthe file: %f", yaml.load(read_yaml_file))
        #print pass_velocity_init
        read_yaml_file.close()
        return pass_velocity_cmd

class store_act_x:
    def __init__(self):
        self.vel_act_x= 0
        #print self.vel_init_x

    def write_file(self, vel_act_x):
        #amount = raw_input("How many objects would you like to spawn? ")
        self.vel_act_x=vel_act_x
        vel_act_file = open('/home/zinnirah/ros/workspace/thesis/brsu_hmm_eid_observations/objects/vel_act_x.yaml', 'w')
        vel_act_details = self.vel_act_x
        yaml.dump(vel_act_details,vel_act_file)
        vel_act_file.close()

    def read_file(self):
        read_yaml_file = open('/home/zinnirah/ros/workspace/thesis/brsu_hmm_eid_observations/objects/vel_act_x.yaml', 'r')
        pass_velocity_act = yaml.load(read_yaml_file)
        #rospy.loginfo("inthe file: %f", yaml.load(read_yaml_file))
        #print pass_velocity_init
        read_yaml_file.close()
        return pass_velocity_act

class store_vel_init_x():
    def __init__(self):
        self.vel_init_x= 0
        #print self.vel_init_x

    def write_file(self, vel_init_x):
        #amount = raw_input("How many objects would you like to spawn? ")
        self.vel_init_x=vel_init_x
        vel_init_file = open('/home/zinnirah/ros/workspace/thesis/brsu_hmm_eid_observations/objects/vel_init_x.yaml', 'w')
        vel_init_details = self.vel_init_x
        yaml.dump(vel_init_details,vel_init_file)
        vel_init_file.close()

    def read_file(self):
        read_yaml_file = open('/home/zinnirah/ros/workspace/thesis/brsu_hmm_eid_observations/objects/vel_init_x.yaml', 'r')
        pass_velocity_init = yaml.load(read_yaml_file)
        #rospy.loginfo("inthe file: %f", yaml.load(read_yaml_file))
        print pass_velocity_init
        read_yaml_file.close()
        return pass_velocity_init

class store_acc_x():
    def __init__(self):
        self.acc_x= 0
        #print self.acc_x

    def write_file(self, acc_x):
        #amount = raw_input("How many objects would you like to spawn? ")
        self.acc_x=acc_x
        acc_file = open('/home/zinnirah/ros/workspace/thesis/brsu_hmm_eid_observations/objects/acc_x.yaml', 'w')
        acc_details = self.acc_x
        yaml.dump(acc_details,acc_file)
        acc_file.close()

    def read_file(self):
        read_yaml_file = open('/home/zinnirah/ros/workspace/thesis/brsu_hmm_eid_observations/objects/acc_x.yaml', 'r')
        pass_acceleration_init = yaml.load(read_yaml_file)
        #rospy.loginfo("inthe file: %f", yaml.load(read_yaml_file))
        print pass_acceleration_init
        read_yaml_file.close()
        return pass_acceleration_init
    
class store_acc_init_x():
    def __init__(self):
        self.acc_init_x = 0
        #print self.acc_init_x

    def write_file(self, acc_init_x):
        #amount = raw_input("How many objects would you like to spawn? ")
        self.acc_init_x = acc_init_x
        acc_init_file = open('/home/zinnirah/ros/workspace/thesis/brsu_hmm_eid_observations/objects/acc_init_x.yaml', 'w')
        acc_init_details = self.acc_init_x
        yaml.dump(acc_init_details, acc_init_file)
        acc_init_file.close()

    def read_file(self):
        read_yaml_file = open('/home/zinnirah/ros/workspace/thesis/brsu_hmm_eid_observations/objects/acc_init_x.yaml', 'r')
        pass_acc_init = yaml.load(read_yaml_file)
        #rospy.loginfo("inthe file: %f", yaml.load(read_yaml_file))
        #print pass_acc_init
        read_yaml_file.close()
        return pass_acc_init

class store_jerk_x():
    def __init__(self):
        self.jerk_x = 0
        #print self.jerk_x

    def write_file(self, jerk_x):
        #amount = raw_input("How many objects would you like to spawn? ")
        self.jerk_x = jerk_x
        jerk_file = open('/home/zinnirah/ros/workspace/thesis/brsu_hmm_eid_observations/objects/jerk_x.yaml', 'w')
        jerk_details = self.jerk_x
        yaml.dump(jerk_details, jerk_file)
        jerk_file.close()

    def read_file(self):
        read_yaml_file = open('/home/zinnirah/ros/workspace/thesis/brsu_hmm_eid_observations/objects/jerk_x.yaml', 'r')
        pass_jerk = yaml.load(read_yaml_file)
        #rospy.loginfo("inthe file: %f", yaml.load(read_yaml_file))
        #print pass_jerk
        read_yaml_file.close()
        return pass_jerk
    

#===============================================================================
# BASE Y
#===============================================================================

class store_cmd_y:
    def __init__(self):
        self.vel_cmd_y= 0
        #print self.vel_init_y

    def write_file(self, vel_cmd_y):
        #amount = raw_input("How many objects would you like to spawn? ")
        self.vel_cmd_y = vel_cmd_y
        #vel_cmd_file = open('../objects/vel_cmd_y.yaml', 'w')
        vel_cmd_file = open('/home/zinnirah/ros/workspace/thesis/brsu_hmm_eid_observations/objects/vel_cmd_y.yaml', 'w')
        vel_cmd_details = self.vel_cmd_y
        yaml.dump(vel_cmd_details,vel_cmd_file)
        vel_cmd_file.close()

    def read_file(self):
        read_yaml_file = open('/home/zinnirah/ros/workspace/thesis/brsu_hmm_eid_observations/objects/vel_cmd_y.yaml', 'r')
        pass_velocity_cmd = yaml.load(read_yaml_file)
        #rospy.loginfo("inthe file: %f", yaml.load(read_yaml_file))
        #print pass_velocity_init
        read_yaml_file.close()
        return pass_velocity_cmd

class store_act_y:
    def __init__(self):
        self.vel_act_y= 0
        #print self.vel_init_y

    def write_file(self, vel_act_y):
        #amount = raw_input("How many objects would you like to spawn? ")
        self.vel_act_y=vel_act_y
        #vel_act_file = open('../objects/vel_act_y.yaml', 'w')
        vel_act_file = open('/home/zinnirah/ros/workspace/thesis/brsu_hmm_eid_observations/objects/vel_act_y.yaml', 'w')
        vel_act_details = self.vel_act_y
        yaml.dump(vel_act_details,vel_act_file)
        vel_act_file.close()

    def read_file(self):
        read_yaml_file = open('/home/zinnirah/ros/workspace/thesis/brsu_hmm_eid_observations/objects/vel_act_y.yaml', 'r')
        pass_velocity_act = yaml.load(read_yaml_file)
        #rospy.loginfo("inthe file: %f", yaml.load(read_yaml_file))
        #print pass_velocity_init
        read_yaml_file.close()
        return pass_velocity_act


class store_vel_init_y():
    def __init__(self):
        self.vel_init_y= 0
        print self.vel_init_y

    def write_file(self, vel_init_y):
        #amount = raw_input("How many objects would you like to spawn? ")
        self.vel_init_y=vel_init_y
        vel_init_file = open('/home/zinnirah/ros/workspace/thesis/brsu_hmm_eid_observations/objects/vel_init_y.yaml', 'w')
        vel_init_details = self.vel_init_y
        yaml.dump(vel_init_details,vel_init_file)
        vel_init_file.close()

    def read_file(self):
        read_yaml_file = open('/home/zinnirah/ros/workspace/thesis/brsu_hmm_eid_observations/objects/vel_init_y.yaml', 'r')
        pass_velocity_init = yaml.load(read_yaml_file)
        #rospy.loginfo("inthe file: %f", yaml.load(read_yaml_file))
        #print pass_velocity_init
        read_yaml_file.close()
        return pass_velocity_init

class store_acc_y():
    def __init__(self):
        self.acc_y= 0
        #print self.acc_y

    def write_file(self, acc_y):
        #amount = raw_input("How many objects would you like to spawn? ")
        self.acc_y=acc_y
        acc_file = open('/home/zinnirah/ros/workspace/thesis/brsu_hmm_eid_observations/objects/acc_y.yaml', 'w')
        acc_details = self.acc_y
        yaml.dump(acc_details,acc_file)
        acc_file.close()

    def read_file(self):
        read_yaml_file = open('/home/zinnirah/ros/workspace/thesis/brsu_hmm_eid_observations/objects/acc_y.yaml', 'r')
        pass_acc_x = yaml.load(read_yaml_file)
        #rospy.loginfo("inthe file: %f", yaml.load(read_yaml_file))
        print pass_acc_x
        read_yaml_file.close()
        return pass_acc_x
    
class store_acc_init_y:
    def __init__(self):
        self.acc_init_y= 0
        print self.acc_init_y

    def write_file(self, acc_init_y):
        #amount = raw_input("How many objects would you like to spawn? ")
        self.acc_init_y=acc_init_y
        acc_init_file = open('/home/zinnirah/ros/workspace/thesis/brsu_hmm_eid_observations/objects/acc_init_y.yaml', 'w')
        acc_init_details = self.acc_init_y
        yaml.dump(acc_init_details,acc_init_file)
        acc_init_file.close()

    def read_file(self):
        read_yaml_file = open('/home/zinnirah/ros/workspace/thesis/brsu_hmm_eid_observations/objects/acc_init_y.yaml', 'r')
        pass_acc_init = yaml.load(read_yaml_file)
        #rospy.loginfo("inthe file: %f", yaml.load(read_yaml_file))
        print pass_acc_init
        read_yaml_file.close()
        return pass_acc_init

class store_jerk_y():
    def __init__(self):
        self.jerk_y = 0
        #print self.jerk_y

    def write_file(self, jerk_y):
        #amount = raw_input("How many objects would you like to spawn? ")
        self.jerk_y = jerk_y
        jerk_file = open('/home/zinnirah/ros/workspace/thesis/brsu_hmm_eid_observations/objects/jerk_y.yaml', 'w')
        jerk_details = self.jerk_y
        yaml.dump(jerk_details, jerk_file)
        jerk_file.close()

    def read_file(self):
        read_yaml_file = open('/home/zinnirah/ros/workspace/thesis/brsu_hmm_eid_observations/objects/jerk_y.yaml', 'r')
        pass_jerk = yaml.load(read_yaml_file)
        #rospy.loginfo("inthe file: %f", yaml.load(read_yaml_file))
        #print pass_jerk
        read_yaml_file.close()
        return pass_jerk
 
 #==============================================================================
 # 
 # 
 # ArmJointVelocities 
 # 
 # 
 #==============================================================================
   
class PrettySafeLoader(yaml.SafeLoader):
    def construct_python_tuple(self, node):
        return tuple(self.construct_sequence(node))

class store_arm_joint_vel_cmd:
    def __init__(self):
        self.arm_joint_vel_cmd= []

    def write_file(self, arm_joint_vel_cmd=[]):
        vel_cmd_file = open('../objects/arm_joint_vel_cmd.yaml', 'w')
        vel_cmd_details = arm_joint_vel_cmd
        yaml.dump(vel_cmd_details,vel_cmd_file)
        vel_cmd_file.close()

    def read_file(self):
        PrettySafeLoader.add_constructor(u'tag:yaml.org,2002:python/tuple', PrettySafeLoader.construct_python_tuple)
        read_yaml_file = open('../objects/arm_joint_vel_cmd.yaml', 'r')
        pass_velocity_cmd = yaml.load(read_yaml_file, Loader=PrettySafeLoader)
        read_yaml_file.close()
        return pass_velocity_cmd

class store_arm_joint_vel_act:
    def __init__(self):
        self.arm_joint_vel_act=[]

    def write_file(self, arm_joint_vel_act=[]):

        vel_act_file = open('../objects/arm_joint_vel_act.yaml', 'w')
        vel_act_details = arm_joint_vel_act#self.arm_joint_vel_act

        yaml.dump(tuple(vel_act_details),vel_act_file)
        vel_act_file.close()

    def read_file(self):
        PrettySafeLoader.add_constructor(u'tag:yaml.org,2002:python/tuple', PrettySafeLoader.construct_python_tuple)
        read_yaml_file = open('../objects/arm_joint_vel_act.yaml', 'r')
        pass_velocity_act = yaml.load(read_yaml_file, Loader=PrettySafeLoader)

        read_yaml_file.close()
        return pass_velocity_act