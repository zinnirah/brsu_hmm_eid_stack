#!/usr/bin/env python

import roslib; roslib.load_manifest('brsu_hmm_eid_observations')
import roslib.message
import rospy
import yaml
import message_filters


from pylab import plot,show
from scipy import stats
import numpy as np
import collections
#from types import *

from rospy.numpy_msg import numpy_msg
from sensor_msgs.msg import JointState
from brsu_hmm_eid_messages.msg import obs_arm, obs_arm_JointPositions, obs_arm_JointVelocities, obs_arm_JointTorques, obs_arm_JointAccelerations, obs_arm_JointJerks, obs_arm_JointPositions_rel_diff, obs_arm_JointTorques_rel_diff, obs_gripper_pos_diff
from brics_actuator.msg import JointPositions, JointVelocities, JointTorques
#from std_msgs.msg import Time

#from message_filters import TimeSynchronizer, Subscriber

class obs_armj_all():
    def __init__(self):
        
        #Youbot
        self.joint_names = ["arm_joint_1", "arm_joint_2", "arm_joint_3", "arm_joint_4", "arm_joint_5","gripper_finger_joint_l","gripper_finger_joint_r"]
        #Jenny
        #self.joint_names = ['arm_1_joint', 'arm_2_joint', 'arm_3_joint', 'arm_4_joint', 'arm_5_joint', 'arm_6_joint', 'arm_7_joint']
        
        self.gripper_pos_rel_diff = []
        self.armj_pos_rel_diff = []
        self.armj_eff_rel_diff = []
        self.armj_names = []
        self.zerolist = np.array([0]*5)

        self.duration_acc = 4
        self.duration_jerk = 2 # or 2 because 2x4=8
        
        self.sz_pos = 800
        self.sz_acc = 800
        self.sz_jerk = 2#2#8#12

        self.armj_pos = collections.deque(maxlen = self.sz_pos)
        #self.armj_vel = collections.deque(maxlen = self.duration_acc)
        self.armj_vel = collections.deque(maxlen = self.sz_acc)
        self.armj_eff = collections.deque(maxlen = 2)
        self.armj_acc = collections.deque(maxlen = self.sz_jerk)
        self.gripper_pos = collections.deque(maxlen = self.sz_pos)
        self.armj_jerk = np.array([0]*5) #collections.deque(maxlen = duration_jerk)

        #for i in xrange(self.sz_pos):
        #    self.armj_pos.append(self.zerolist)
        
        #for i in xrange(self.sz_acc):
        #    self.armj_vel.append(self.zerolist)
        
        self.armj_eff.append(self.zerolist)
        
        for i in xrange(self.sz_jerk):
            self.armj_acc.append(self.zerolist)
        
        #self.armj_jerk = self.zerolist
        #self.t = rospy.Time.now().secs

        #self.listener()

    def listener(self):
 
        rospy.Subscriber("/joint_states", JointState, self.callback)

    def callback(self, armj_act_states):
        
        #print 'OK1'
        names_array = armj_act_states.name

        arm_name_index = names_array.index('arm_joint_1')
        
        #youbot
        self.armj_names = names_array[arm_name_index:arm_name_index+5]
        self.armj_pos.append(np.array(armj_act_states.position[arm_name_index:arm_name_index+5]))
        self.armj_vel.append(np.array(armj_act_states.velocity[arm_name_index:arm_name_index+5]))
        self.armj_eff.append(np.array(armj_act_states.effort[arm_name_index:arm_name_index+5]))
        
        #jenny
        #self.armj_names = names_array[arm_name_index:arm_name_index+7]
        #print armj_act_states.velocity[0]
        #self.armj_pos.append(np.array(armj_act_states.position[arm_name_index:arm_name_index+7]))
        #print self.armj_pos
        #self.armj_vel.append(np.array(armj_act_states.velocity[arm_name_index:arm_name_index+7]))
        #self.armj_eff.append(np.array(armj_act_states.effort[arm_name_index:arm_name_index+7]))
        self.calc_armj_acc()# t)
        #print 'armj_acc: ', armj_acc, 't: ', t
        self.calc_armj_jerk()#t)
        #print 'armj_jerk: ', armj_jerk
        self.calc_armj_eff_rel_diff()# t)

        self.talker()


    def calc_armj_pos_rel_diff(self):#, t):

        #armj_pos_rel_diff = [0]*5
        #self.armj_pos.append(armj_act_positions)
        #if t == 0:
        #    self.armj_pos_rel_diff = self.zerolist

        #else:
            self.armj_pos_rel_diff = self.armj_pos[self.sz_pos - 1] - self.armj_pos[0]#armj_act_positions[each] - armj_pos_init[each]
            self.gripper_pos_rel_diff =  self.gripper_pos[self.sz_pos - 1] - self.gripper_pos[0]
            #print self.gripper_pos_rel_diff
            #print self.armj_pos[self.sz_pos - 1]
            #print self.armj_pos[0] 
            #print self.armj_pos_rel_diff


    def calc_armj_acc(self):#, t):

        #if (int(t) % self.duration_acc) == 0 :
        #self.armj_vel.append(armj_act_vel)
            #armj_acc = (self.armj_vel[self.duration_acc - 1] - self.armj_vel[0])/self.duration_acc
        #print self.armj_vel[self.sz_acc- 1]
        #print self.armj_vel[0]
        armj_acc = (self.armj_vel[self.sz_acc- 1] - self.armj_vel[0])#/self.duration_acc
        #print len(self.armj_vel) 
        #print armj_acc
        self.armj_acc.append(armj_acc)
        #print self.armj_vel[self.sz_acc -1 ]

        #else:
        #    self.armj_vel.append(armj_act_vel)


    def calc_armj_jerk(self):#, t):

        #if (int(t) % self.duration_jerk) == 0 :
            #self.armj_jerk = (self.armj_acc[self.duration_jerk-1] -  self.armj_acc[0])/self.duration_jerk
            self.armj_jerk = (self.armj_acc[self.sz_jerk-1] -  self.armj_acc[0])#/self.duration_jerk)/self.duration_jerk

    def calc_armj_eff_rel_diff(self):#, t):

       # self.armj_eff.append(armj_effort_act)

        #if t == 0:
        #    self.armj_eff_rel_diff = self.zerolist

        #else:
            self.armj_eff_rel_diff = self.armj_eff[1] - self.armj_eff[0]



    def talker(self):

        time_now =  rospy.Time.now()
        #print 'time_now: ', time_now.secs
        #print '1:', armj_acc
        #print '2:', armj_pos_rel_diff

        #pub1 = rospy.Publisher('/brsu_hmm_eid_observations/armj_cmd_positions', obs_arm_JointPositions)#obs_arm.jcmd_pos)
        pub2 = rospy.Publisher('/brsu_hmm_eid_observations/armj_positions', obs_arm_JointPositions)#obs_arm.jact_pos)
        pub3 = rospy.Publisher('/brsu_hmm_eid_observations/armj_rel_diff_positions', obs_arm_JointPositions_rel_diff)#obs_arm.diff_pos)
        #pub4 = rospy.Publisher('/brsu_hmm_eid_observations/armj_cmd_velocities', obs_arm_JointVelocities)#obs_arm.jcmd_vel)
        pub5 = rospy.Publisher('/brsu_hmm_eid_observations/armj_velocities', obs_arm_JointVelocities)#obs_arm.jact_vel)
        #pub6 = rospy.Publisher('/brsu_hmm_eid_observations/armj_rel_diff_velocities', obs_arm_JointVelocities_rel_diff)#obs_arm.diff_vel)
        #pub7 = rospy.Publisher('/brsu_hmm_eid_observations/armj_cmd_effort', obs_arm_JointTorques)#obs_arm.jcmd_effort)
        pub8 = rospy.Publisher('/brsu_hmm_eid_observations/armj_effort', obs_arm_JointTorques)#obs_arm.jact_effort)
        pub9 = rospy.Publisher('/brsu_hmm_eid_observations/armj_rel_diff_effort', obs_arm_JointTorques_rel_diff)#obs_arm.diff_effort)
        pub10 = rospy.Publisher('/brsu_hmm_eid_observations/armj_accelerations', obs_arm_JointAccelerations)
        pub11 = rospy.Publisher('/brsu_hmm_eid_observations/armj_jerk',  obs_arm_JointJerks)
        pub12 = rospy.Publisher('/brsu_hmm_eid_observations/gr_rel_diff_positions', obs_gripper_pos_diff)

        #pub1.publish(armj_cmd_positions)
        msg2send2 = obs_arm_JointPositions()
        msg2send2.header.stamp.secs = time_now.secs
        msg2send2.header.stamp.nsecs = time_now.nsecs
        msg2send2.positions = self.armj_pos[self.sz_pos-1]  # armj_act_positions
        pub2.publish(msg2send2)
        #rospy.sleep(1)
        msg2send3 = obs_arm_JointPositions_rel_diff()
        msg2send3.header.stamp.secs = time_now.secs
        msg2send3.header.stamp.nsecs = time_now.nsecs
        msg2send3.pos_rel_diff = self.armj_pos_rel_diff
        pub3.publish(msg2send3)
        #rospy.sleep(1)
        msg2send5 = obs_arm_JointVelocities()
        msg2send5.header.stamp.secs = time_now.secs
        msg2send5.header.stamp.nsecs = time_now.nsecs
        msg2send5.velocities = self.armj_vel[self.sz_acc - 1]
        pub5.publish(msg2send5)
        #rospy.sleep(1)
        #pub6.publish(armj_diff_vel)
        #pub7.publish(armj_cmd_effort)
        msg2send8 = obs_arm_JointTorques()
        msg2send8.header.stamp.secs = time_now.secs
        msg2send8.header.stamp.nsecs = time_now.nsecs
        msg2send8.torques = self.armj_eff[1]  # armj_effort_act
        pub8.publish(msg2send8)
        #rospy.sleep(1)
        #pub9.publish(armj_diff_effort)
        msg2send9 = obs_arm_JointTorques_rel_diff()
        msg2send9.header.stamp.secs = time_now.secs
        msg2send9.header.stamp.nsecs = time_now.nsecs
        msg2send9.torques_rel_diff = self.armj_eff_rel_diff
        pub9.publish(msg2send9)
        #rospy.sleep(1)

        msg2send10 = obs_arm_JointAccelerations()
        msg2send10.header.stamp.secs = time_now.secs
        msg2send10.header.stamp.nsecs = time_now.nsecs
        msg2send10.accelerations = self.armj_acc[self.sz_jerk - 1]
        pub10.publish(msg2send10)
        #rospy.sleep(1)

        msg2send11 = obs_arm_JointJerks()
        msg2send11.header.stamp.secs = time_now.secs
        msg2send11.header.stamp.nsecs = time_now.nsecs
        msg2send11.jerks = self.armj_jerk
        pub11.publish(msg2send11)
        
        msg2send12 = obs_gripper_pos_diff()
        msg2send12.header.stamp.secs = time_now.secs
        msg2send12.header.stamp.nsecs = time_now.nsecs
        msg2send12.gripper_pos_diff = self.gripper_pos_rel_diff
        pub12.publish(msg2send12)
        

        rospy.loginfo('Published obs_armj_all')
        #===========================================================================
        # msg2send12 = obs_arm()
        # msg2send12.header.stamp.secs = time_now.secs
        # msg2send12.jname = armj_names
        # msg2send12.jact_position = armj_act_positions
        # msg2send12.jact_vel = armj_act_velocities
        # msg2send12.jacc = armj_acc
        # msg2send12.jjerk = armj_jerk
        # msg2send12.jact_effort = armj_effort_act
        # pub12.publish(msg2send12)
        #===========================================================================

if __name__ == '__main__':

    rospy.init_node('brsu_hmm_eid_node_armj_observation_all', anonymous=True)
    oaa = obs_armj_all()
    #r = rospy.Rate(10)#100
    while not rospy.is_shutdown():
        try:
            oaa.listener()
            #r.sleep()
            rospy.sleep(1) # Comment this on a real robot wto get faster response
        except rospy.ROSInterruptException:
            pass
