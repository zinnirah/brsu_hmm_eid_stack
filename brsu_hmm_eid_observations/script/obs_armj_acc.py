#!/usr/bin/env python

import roslib; roslib.load_manifest('brsu_hmm_eid_observations')
import roslib.message
import rospy
import yaml
import message_filters

from yaml_safe_loader import PrettySafeLoader
#from types import *

from sensor_msgs.msg import JointState
from brsu_hmm_eid_messages.msg import obs_arm_JointPositions, obs_arm_JointVelocities, obs_arm_JointTorques, obs_arm_JointAccelerations, obs_arm_JointJerks
from brics_actuator.msg import JointPositions, JointVelocities, JointTorques

#from std_msgs.msg import Time

#from message_filters import TimeSynchronizer, Subscriber


class store_armj_velocities():
    def __init__(self):
        self.armj_vel_init = [0]*5

    def write_file(self, armj_vel_init):
        self.armj_vel_init = armj_vel_init
        vel_init_file = open('/home/zinnirah/ros/workspace/thesis/brsu_hmm_eid_observations/objects/armj_vel_init.yaml', 'w')
        vel_init_details = {'arm_joint_velocity_init' : self.armj_vel_init}
        #print 'A', vel_init_details
        yaml.dump(vel_init_details,vel_init_file)
        vel_init_file.close()

    def read_file(self):
        PrettySafeLoader.add_constructor(u'tag:yaml.org,2002:python/tuple', PrettySafeLoader.construct_python_tuple)
        
        read_yaml_file = open('/home/zinnirah/ros/workspace/thesis/brsu_hmm_eid_observations/objects/armj_vel_init.yaml', 'r')
        pass_velocity_init = yaml.load(read_yaml_file, Loader=PrettySafeLoader)
        #rospy.loginfo("inthe file: %f", yaml.load(read_yaml_file))
        print 'C', pass_velocity_init['arm_joint_velocity_init']
        read_yaml_file.close()
        return pass_velocity_init['arm_joint_velocity_init']

class store_armj_acc():
    def __init__(self):
        self.armj_acc_init = [0]*5

    def write_file(self, armj_acc_init):
        self.armj_acc_init = armj_acc_init
        acc_init_file = open('/home/zinnirah/ros/workspace/thesis/brsu_hmm_eid_observations/objects/armj_acc_init.yaml', 'w')
        acc_init_details = {'arm_joint_acceleration_init': self.armj_acc_init}
        #print 'D', acc_init_details
        yaml.dump(acc_init_details,acc_init_file)
        acc_init_file.close()

    def read_file(self):
        PrettySafeLoader.add_constructor(u'tag:yaml.org,2002:python/tuple', PrettySafeLoader.construct_python_tuple)
        read_yaml_file = open('/home/zinnirah/ros/workspace/thesis/brsu_hmm_eid_observations/objects/armj_acc_init.yaml', 'r')
        pass_acc_init = yaml.load(read_yaml_file, Loader=PrettySafeLoader)
        #rospy.loginfo("inthe file: %f", yaml.load(read_yaml_file))
        print 'B', pass_acc_init['arm_joint_acceleration_init']
        read_yaml_file.close()
        return pass_acc_init['arm_joint_acceleration_init']
    
def listener():

    rospy.Subscriber("/joint_states", JointState, callback)

def callback(armj_act_states):

    rospy.loginfo("received commanded and actual arm joint states")

    names_array = armj_act_states.name
    arm_name_index = names_array.index('arm_joint_1')
    armj_pos_act = armj_act_states.position[arm_name_index:arm_name_index+5]

    armj_act_velocities = armj_act_states.velocity[arm_name_index:arm_name_index+5]
    
    armj_acc, t  = calc_armj_acc(armj_act_velocities)
    print 'armj_acc: ', armj_acc, 't: ', t
    #talker(armj_cmd_positions, armj_cmd_vel, armj_cmd_effort, armj_act_states, armj_diff_positions, armj_diff_vel, armj_diff_effort, armj_acc, armj_jerk)
    talker(armj_act_velocities, armj_acc)

def calc_armj_acc(armj_act_vel):
    sajv = store_armj_velocities()
    saja = store_armj_acc()
    armj_acc = [1,2,3,4,5]
    duration = 4
    t = rospy.Time.now().secs
    testt = int(t) % duration
    print 'testt: ', testt
    #print int(t)
    
    if int(t) < duration:
        print 'OK'
        if t == 0:
            for i in range(len(armj_act_vel)):
                vel_init[i] = armj_act_vel[i]
            sajv.write_file(vel_init)
        else :
           for i in range(len(armj_act_vel)):
              vel_init[i] = 0
        armj_acc = [0] * len(armj_act_vel)
        saja.write_file(armj_acc)
    elif (int(t) % duration) == 0 :
        vel_init = sajv.read_file()
        print 'vel int: ', vel_init
        vel_last = armj_act_vel
        print 'vel_last: ', vel_last
        for i in range(len(armj_act_vel)):
            armj_acc[i] = (vel_last[i] - vel_init[i]) / duration
        print 'armj_acc', armj_acc
        sajv.write_file(vel_last)
        saja.write_file(armj_acc)
    else:
       #rospy.loginfo("Error in calculating armj_acc, will return armj_acc = [0]*len(armj_act_vel)")
       armj_acc = saja.read_file()
    return armj_acc, t


#def talker(armj_cmd_positions, armj_cmd_vel, armj_cmd_effort,
#           armj_act_states, armj_diff_positions, armj_diff_vel, armj_diff_effort, armj_acc, armj_jerk) :
def talker(armj_act_velocities, armj_acc) :
    #rospy.loginfo("difference: %f" %armj_diff_positions)

    time_now =  rospy.Time.now().secs
    print 'time_now: ', time_now

    pub5 = rospy.Publisher('/brsu_hmm_eid_observations/armj_act_velocities', obs_arm_JointVelocities)#obs_arm.jact_vel)
    pub10 = rospy.Publisher('/brsu_hmm_eid_observations/armj_acc', obs_arm_JointAccelerations)

    msg2send5 = obs_arm_JointVelocities()
    msg2send5.header.stamp.secs = time_now
    msg2send5.velocities = armj_act_velocities
    pub5.publish(msg2send5)
    
    msg2send10 = obs_arm_JointAccelerations()
    msg2send10.header = time_now
    msg2send10.accelerations = armj_acc
    pub10.publish(msg2send10)
    
if __name__ == '__main__':

    val_init = [0,0,0,0,0]
    rospy.init_node('brsu_hmm_eid_node_armj_observation_acc', anonymous=True)
    store_armj_velocities().write_file(val_init)
    store_armj_acc().write_file(val_init)

    while not rospy.is_shutdown():
        try:
            #g=get_predefined_joint_positions()
            #g.execute()
            listener()
            rospy.sleep(4)
        except rospy.ROSInterruptException:
            pass
       
