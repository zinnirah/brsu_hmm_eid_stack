#!/usr/bin/env python

import roslib; roslib.load_manifest('brsu_hmm_eid_observations')
import roslib.message
import rospy
import message_filters

from sensor_msgs.msg import JointState
from brsu_hmm_eid_messages.msg import obs_base, obs_base_x_acc, obs_base_x_jerk, obs_base_x_vel_diff, obs_base_x_vel_cmd, obs_base_y_acc, obs_base_y_jerk, obs_base_y_vel_diff, obs_base_y_cmd_vel
from brics_actuator.msg import JointPositions, JointVelocities, JointTorques


#t = rospy.Time.now().secs

def listener():
    
    base_x_vel_cmd_sub = message_filters.Subscriber('/brsu_hmm_eid_observations/base_x_vel_cmd', obs_base_x_vel_cmd)
    base_x_vel_diff_sub = message_filters.Subscriber('/brsu_hmm_eid_observations/base_x_vel_diff', obs_base_x_vel_diff)
    base_x_acc_sub = message_filters.Subscriber('/brsu_hmm_eid_observations/base_x_acc', obs_base_x_acc)
    base_x_jerk_sub = message_filters.Subscriber('/brsu_hmm_eid_observations/base_x_jerk', obs_base_x_jerk)
    base_y_vel_cmd_sub = message_filters.Subscriber('/brsu_hmm_eid_observations/base_y_vel_cmd', obs_base_y_cmd_vel)
    base_y_vel_diff_sub = message_filters.Subscriber('/brsu_hmm_eid_observations/base_y_vel_diff', obs_base_y_vel_diff)
    base_y_acc_sub = message_filters.Subscriber('/brsu_hmm_eid_observations/base_y_acc', obs_base_y_acc)
    base_y_jerk_sub = message_filters.Subscriber('/brsu_hmm_eid_observations/base_y_jerk', obs_base_y_jerk)
    
    ts = message_filters.TimeSynchronizer([base_x_vel_cmd_sub, base_x_vel_diff_sub, base_x_acc_sub , base_x_jerk_sub, 
                                           base_y_vel_cmd_sub, base_y_vel_diff_sub , base_y_acc_sub , base_y_jerk_sub ], 1)
    ts.registerCallback(callback)

def callback(base_x_vel_cmd_sub, base_x_vel_diff_sub, base_x_acc_sub , base_x_jerk_sub, 
             base_y_vel_cmd_sub, base_y_vel_diff_sub , base_y_acc_sub , base_y_jerk_sub):
    print 'OK'
    
if __name__ == '__main__':

    rospy.init_node('brsu_hmm_eid_node_armj_republish', anonymous=True)

    while not rospy.is_shutdown():
        try:
            listener()
            rospy.sleep(1)
        except rospy.ROSInterruptException:
            pass
