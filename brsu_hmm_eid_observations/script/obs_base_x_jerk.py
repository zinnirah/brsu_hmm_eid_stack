#!/usr/bin/env python
import roslib; roslib.load_manifest('brsu_hmm_eid_observations')
import rospy
import math
import yaml

from brsu_hmm_eid_messages.msg import obs_base_x_jerk, obs_base_x_acc

acc_init_x = 0
jerk_x = 0
duration = 8

def callback(msg):
    #rospy.loginfo("Received actual forward acc <</odom>> message! The value is %f " % msg.x_acc)
    #saix = store_acc_init_x()
    #sjx = store_jerk_x()
    global acc_init_x
    global jerk_x
    t = rospy.Time.now().secs
    #duration = 8
    diff = int(t) % duration
    
    if int(t) < duration:
        jerk_x = 0
        if t == 0 :
            acc_init_x = msg.x_acc
        else :
            acc_init_x = 0
        #print acc_init
        #saix.write_file(acc_init_x)
        #sjx.write_file(jerk_x)
    elif diff == 0:
        #how do i get the the vel_init?
        #acc_init_x = saix.read_file()
        acc_last_x = msg.x_acc
        #print acc_last_x
        jerk_x = (acc_last_x - acc_init_x) / duration
        acc_init_x = acc_last_x
        #saix.write_file(acc_last_x)
        #sjx.write_file(jerk_x)
    else:
        #jerk_x = sjx.read_file()
        pass
    
    talker(jerk_x)


def talker(jerk_x):
    rospy.loginfo("x_jerk: %f" % jerk_x)
    pub = rospy.Publisher('/brsu_hmm_eid_observations/base_x_jerk', obs_base_x_jerk)

    msg2send = obs_base_x_jerk()
    msg2send.header.stamp.secs = rospy.Time.now().secs
    msg2send.x_jerk = jerk_x
    pub.publish(msg2send)


def listener():
    rospy.Subscriber("/brsu_hmm_eid_observations/base_x_acc", obs_base_x_acc, callback)

if __name__ == '__main__':
    rospy.init_node('brsu_hmm_eid_observations_node_base_x_jerk', anonymous=True)
    while not rospy.is_shutdown():
        try:
            listener()
            rospy.sleep(duration)
        except rospy.ROSInterruptException:
            pass
