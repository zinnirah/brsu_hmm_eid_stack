#!/usr/bin/env python
import roslib; roslib.load_manifest('brsu_hmm_eid_observations')
import rospy
import collections
from scipy.signal import kaiserord, lfilter, firwin, freqz
from numpy import cos, sin, pi, absolute, arange, array
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Twist

from brsu_hmm_eid_messages.msg import obs_base_x_vel_diff, obs_base_x_vel_cmd, obs_base_x_acc, obs_base_x_jerk,  obs_base_x_vel_act


class get_obs_base_x_all():
    def __init__(self):

        self.duration_acc = 2
        self.duration_jerk = 4
        
        self.sz_acc = 720
        self.sz_jerk = 2

        self.base_x_vel = collections.deque(maxlen = self.sz_acc)
        self.base_x_acc = collections.deque(maxlen = self.sz_jerk)

        for i in xrange(self.sz_acc):
            self.base_x_vel.append(0)

        for i in xrange(self.sz_jerk):
            self.base_x_acc.append(0)

        self.base_x_vel_cmd = 0
        self.base_x_vel_diff = 0
        self.base_x_jerk = 0
        self.t = rospy.Time.now()
        self.filtered_vel = [0]*self.sz_acc
        
    def low_pass_filter(self):
        # The Nyquist rate of the signal.
        sample_rate = 2
        nyq_rate = sample_rate / 2.0
        
        # The desired width of the transition from pass to stop,
        # relative to the Nyquist rate.  We'll design the filter
        # with a 5 Hz transition width.
        width = 5/nyq_rate
        
        # The desired attenuation in the stop band, in dB.
        ripple_db = 30.0#60.0
        
        # Compute the order and Kaiser parameter for the FIR filter.
        N, beta = kaiserord(ripple_db, width)
        
        # The cutoff frequency of the filter.
        cutoff_hz = 0.001
        
        # Use firwin with a Kaiser window to create a lowpass FIR filter.
        taps = firwin(N, cutoff_hz/nyq_rate, window=('kaiser', beta))
        
        # Use lfilter to filter x with the FIR filter.
        filtered_x = lfilter(taps, 1.0, self.base_x_vel)
        #print len(filtered_x)
        self.filtered_vel = filtered_x
        #return filtered_x


    def listener(self):
        
        #rospy.sleep(1)
        rospy.Subscriber("odom", Odometry, self.odom_callback)
        rospy.Subscriber("cmd_vel", Twist, self.cmd_vel_callback)
        self.talker()

    def odom_callback(self, msg):
        
        
        self.t = rospy.Time.now()
        #print 'callback:', msg.twist.twist.linear.x
        self.base_x_vel.append(msg.twist.twist.linear.x)
        #print 'copied:', self.base_x_vel[self.sz_acc-1]
        #self.low_pass_filter()
        #print self.base_x_vel[self.duration_jerk-1]
        self.calc_base_x_acc()
        self.calc_base_x_jerk()
        
        #self.talker()
    def cmd_vel_callback(self, msg):

        if msg.linear.x is None:
            self.base_x_vel_cmd = 0
        else:
            self.base_x_vel_cmd = msg.linear.x
            

        self.calc_base_x_vel_diff()

    def calc_base_x_vel_diff(self):
        
        #rospy.sleep(1)
        self.base_x_vel_diff = self.base_x_vel_cmd - self.base_x_vel[self.sz_acc - 1]
        #self.base_x_vel_diff = self.base_x_vel_cmd - self.filtered_vel[self.sz_acc - 1]
        #print 'vel_diff', self.base_x_vel_diff

    def calc_base_x_acc(self):
        
        #rospy.sleep(self.duration_acc)
        #diff = int(t) % self.duration_acc

        #if int(t) < self.duration_acc:
        #    self.base_x_acc.append(0)

        #elif diff == 0 :
        #else:
        #rospy.sleep(1)
        
        acc_x = (self.base_x_vel[self.sz_acc - 1] - self.base_x_vel[0])#/self.duration_acc
        #acc_x = (self.filtered_vel[1] - self.filtered_vel[0])#/self.duration_acc
        #print self.base_x_vel[self.sz_acc - 1] 
        #print self.base_x_vel[0]
        #print 'acc_x', acc_x
            #rospy.sleep(1)
        self.base_x_acc.append(acc_x)
        
        #if acc_x > 0.01:
        #    print 'accelerating'
        #if acc_x < -0.01:
        #    print 'decelerating'
       # else:
        #    print 'constant'
            #print self.base_x_acc[self.duration_jerk - 1]

    def calc_base_x_jerk(self):

        #diff = int(t) % self.duration_jerk

        #if int(t) < self.duration_jerk:
        #    self.base_x_jerk = 0

        #elif diff == 0 :
        jerk_x = (self.base_x_acc[self.sz_jerk - 1] - self.base_x_acc[0])#/self.duration_jerk
        #jerk_x = (self.base_x_vel[self.sz_acc - 1] - self.base_x_vel[0])#/self.duration_jerk)/self.duration_jerk
        #print 'jerk_x',jerk_x
        self.base_x_jerk = jerk_x
        
        #if jerk_x > 0.01 and jerk_x < -0.01:
        #    print 'jerk!'
        #else:
        #    print 'no jerk!'
           #print self.base_x_jerk


    def talker(self):
        #rospy.loginfo("Publishing obs_base_x_all")

        time_now = rospy.Time.now()
        pub1 = rospy.Publisher('/brsu_hmm_eid_observations/base_x_vel_cmd', obs_base_x_vel_cmd)#, latch=True)
        pub2 = rospy.Publisher('/brsu_hmm_eid_observations/base_x_vel_diff', obs_base_x_vel_diff)
        pub3 = rospy.Publisher('/brsu_hmm_eid_observations/base_x_acc', obs_base_x_acc)
        pub4 = rospy.Publisher('/brsu_hmm_eid_observations/base_x_jerk', obs_base_x_jerk)
        pub5 = rospy.Publisher('/brsu_hmm_eid_observations/base_x_vel_act', obs_base_x_vel_act)

        msg2send1 = obs_base_x_vel_cmd()
        msg2send1.header.stamp.secs = time_now.secs
        msg2send1.header.stamp.nsecs = time_now.nsecs
        msg2send1.x_cmd_vel = self.base_x_vel_cmd
        pub1.publish(msg2send1)
        #print 'self.base_x_vel_cmd', self.base_x_vel_cmd

        msg2send2 = obs_base_x_vel_diff()
        msg2send2.header.stamp.secs = time_now.secs
        msg2send2.header.stamp.nsecs = time_now.nsecs
        msg2send2.x_vel_diff = self.base_x_vel_diff
        pub2.publish(msg2send2)
        #print 'self.base_x_vel_dif', self.base_x_vel_diff

        msg2send3 = obs_base_x_acc()
        msg2send3.header.stamp.secs = time_now.secs
        msg2send3.header.stamp.nsecs = time_now.nsecs
        msg2send3.x_acc = self.base_x_acc[self.sz_jerk - 1 ]#[self.duration_jerk - 1]
        pub3.publish(msg2send3)
        #print 'self.base_x_acc[self.sz_jerk - 1]', self.base_x_acc[self.sz_jerk - 1]

        msg2send4 = obs_base_x_jerk()
        msg2send4.header.stamp.secs = time_now.secs
        msg2send4.header.stamp.nsecs = time_now.nsecs
        msg2send4.x_jerk = self.base_x_jerk
        pub4.publish(msg2send4)
        #print 'self.base_x_jerk', self.base_x_jerk
        
        msg2send5 = obs_base_x_vel_act()
        msg2send5.header.stamp.secs = time_now.secs
        msg2send5.header.stamp.nsecs = time_now.nsecs
        msg2send5.x_vel_act = self.base_x_vel[self.sz_acc-1]#self.filtered_vel[self.sz_acc - 1 ]#[self.duration_jerk - 1]
        #print self.base_x_vel[self.sz_acc - 1]
        pub5.publish(msg2send5)
        #print 'self.base_x_jerk', self.base_x_jerk

        rospy.loginfo('published base x all ')#%f', self.base_x_vel[self.sz_acc-1])
        #print len(self.base_x_vel)
        
        



if __name__ == '__main__':
    rospy.init_node('brsu_hmm_eid_observations_node_x_all', anonymous=True)

    gobxa = get_obs_base_x_all()
    #r = rospy.Rate(1)#0000)
    while not rospy.is_shutdown():
        try:
            gobxa.listener()
            #gobxa.talker()
            #r.sleep()
            rospy.sleep(1) #comment this on a real robot to get faster response
        except rospy.ROSInterruptException:
            pass
