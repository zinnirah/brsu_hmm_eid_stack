#!/usr/bin/env python
import roslib; roslib.load_manifest('observation_monitor')
import rospy

from geometry_msgs.msg import Twist
from nav_msgs.msg import _Odometry 


def torque_diff_obs():
    print "empty <<torque_diff_obs>> function"    
def torque_cmd_obs():
    print "empty <<torque_cmd_obs>> function"

    
    
if __name__ == '__main__':
    try:
        rospy.init_node('manipulator_observation_monitor')
        torque_diff_obs()
        torque_cmd_obs()
    except rospy.ROSInterruptException:
        pass