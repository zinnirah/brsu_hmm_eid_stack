from pylab import *
import numpy as NP
import re

data = [2000]
gaussian = lambda x: 3*exp(-(30-x)**2/20.)

with open('/home/zinnirah/ros/workspace/thesis/data/Base/accelerating/odom_x.txt') as f:
  #data = NP.loadtxt(f, delimiter=",", dtype='float', comments="%", skiprows=1, usecols=None)
  print f
  for line in f:
      columns = line.split(',')
      if len(columns) >= 2:
        plot(columns[1])


plot(data)

X = arange(data.size)
x = sum(X*data)/sum(data)
width = sqrt(abs(sum((X-x)**2*data)/sum(data)))

max = data.max()

fit = lambda t : max*exp(-(t-x)**2/(2*width**2))

plot(fit(X))

show()