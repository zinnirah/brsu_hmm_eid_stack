#!/usr/bin/env python
import roslib; roslib.load_manifest('observation_monitor')
import rospy

from geometry_msgs.msg import Twist
from nav_msgs.msg import _Odometry 


def velocity_diff_obs():
    rospy.Subscriber("/base_odometry/odometer", data_class, callback, callback_args, queue_size, buff_size, tcp_nodelay)
    rospy.Subscriber("/base_controller/state")
    print "empty <<velocity_diff_obs>> function"    
def velocity_cmd_obs():
    rospy.Subscriber("/cmd_vel")
    print "empty <<velocity_cmd_obs>> function"
def acceleration_obs():
    print "empty <<acceleration_obs>> function"
def jerk_obs():
    print "empty <<jerk_obs>> function"

rospy.get_published_topics()
#TODO: def base_internal_obs();
    
    
if __name__ == '__main__':
    try:
        rospy.init_node('base_observation_monitor')
        velocity_diff_obs()
        velocity_cmd_obs()
        acceleration_obs()
        jerk_obs()
    except rospy.ROSInterruptException:
        pass