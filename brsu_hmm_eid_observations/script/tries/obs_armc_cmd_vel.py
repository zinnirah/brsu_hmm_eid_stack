#!/usr/bin/env python

import roslib; roslib.load_manifest('observation_monitor')
import rospy
import PyKDL
from geometry_msgs.msg import *

#PyKDL.
duration = 1
class store_arm_cmd_x:
    def __init__(self):
        self.vel_cmd_x= 0
        #print self.vel_init_x

    def write_file(self, vel_cmd_x):
        #amount = raw_input("How many objects would you like to spawn? ")
        self.vel_cmd_x=vel_cmd_x
        vel_cmd_file = open('../objects/vel_cmd_x.yaml', 'w')
        vel_cmd_details = self.vel_cmd_x
        yaml.dump(vel_cmd_details,vel_cmd_file)
        vel_cmd_file.close()

    def read_file(self):
        read_yaml_file = open('../objects/vel_cmd_x.yaml', 'r')
        pass_velocity_cmd = yaml.load(read_yaml_file)
        #rospy.loginfo("inthe file: %f", yaml.load(read_yaml_file))
        #print pass_velocity_init
        read_yaml_file.close()
        return pass_velocity_cmd

def callback(msg):
    rospy.loginfo("Received arm commanded cartesian velocity. The value is %f"%msg.twist.linear.x)
    rospy.loginfo("Received arm commanded cartesian velocity. The value is %f"%msg.twist.linear.y)
    rospy.loginfo("Received arm commanded cartesian velocity. The value is %f"%msg.twist.linear.z)

def talker(acc_x):
    rospy.loginfo("x_acceleration: %f"%acc_x)
    pub = rospy.Publisher('x_acceleration', Float64 )
    pub.publish(acc_x)

def listener():
    rospy.Subscriber("/hbrs_manipulation/arm_cart_control/cartesian_velocity_command", TwistStamped, callback)

if __name__ == '__main__':
    rospy.init_node('arm_commanded_cart_vel', anonymous=True)
    while not rospy.is_shutdown():
        try:
            listener()
            rospy.sleep(duration)
        except rospy.ROSInterruptException:
            pass
