#get predefined positions from ros param
#get current position
#compare and have a threshold
class get_difference_with_predefined_joint_positions:
    def __init__(self):
        self.allowance1 = 0.05
        self.allowance2 = -0.05
        self.predefined_joint_positions_element = []

    def get_predefined_joint_positions(self):
        predefined_joint_positions_prefix = '/script_server/arm'
        predefined_joint_positions = rospy.get_param(predefined_joint_positions_prefix)

        for position_name in range(len(predefined_joint_positions)):
            predefined_joint_positions_element[position_name] = predefined_joint_positions.values()[position_name]
            if type(predefined_joint_positions_element[position_name]) is StringTypes:
                predefined_joint_positions_element[position_name] = predefined_joint_positions.values()[position_name][0]
        print predefined_joint_positions_element
        #YOUBOT
        #candle = rospy.get_param('/script_server/arm/')
        #print candle.values()[0][0]
    def execute(self, *args):
        for each in (len(args)):
            for each_element in range(len(predefined_joint_positions_element)):
                difference = predefined_joint_positions_element[each_element] - args[each]
                if difference > allowance1 or difference < allowance2:
                    return ("somewhere else")
                else:
                    return ("predefined positions")

def callback_1():
    rospy.loginfo("Empty Callback 1")

def callback_2(msg):
    names_array = msg.name
    #print names_array
    arm_name_index = names_array.index('arm_joint_1')
    #print arm_name_index
    arm_joint_pos_act = msg.position[arm_name_index:arm_name_index+5]
    #print arm_joint_vel_act
    if arm_joint_pos_act is None:
        arm_joint_pos_act = [0] * len(arm_joint_pos_act)

    diff = get_difference_with_predefined_joint_positions()
    pos = diff.execute(arm_joint_pos_act)
    print pos
