#!/usr/bin/env python
import roslib; roslib.load_manifest('kee_use_cases')
import rospy
import math
import yaml
#from std_msgs.msg import String
#import observation.msg


class store_observations:
    def __init__(self):
        
        self.observations= {}
        self.cmd_base_vel=[]
        self.act_base_velocity=[]
        self.base_vel_diff=[]
        self.base_acc=[]
        self.base_jerk=[]
        self.cmd_arm_joint_pos=[]
        self.act_arm_joint_pos=[]
        self.arm_joint_pos_diff=[]
        self.cmd_arm_joint_vel=[]
        self.act_arm_joint_vel=[]
        self.arm_joint_vel_diff=[]
        self.arm_joint_acc=[]
        self.arm_joint_jerk=[]
        self.cmd_arm_joint_eff=[]
        self.act_arm_joint_eff=[]
        self.arm_joint_eff_diff=[]
        self.gripper_open=[]
        self.grasping=[]
        
    def observation_dictionary():
        
        if (self.cmd_base_vel and self.act_base_velocity and self.base_vel_diff 
        and self.base_acc and self.base_jerk and self.cmd_arm_joint_pos and 
        self.act_arm_joint_pos and self.arm_joint_pos_diff and self.cmd_arm_joint_vel 
        and self.act_arm_joint_vel and self.arm_joint_vel_diff and self.arm_joint_acc and 
        self.arm_joint_jerk and self.cmd_arm_joint_eff and self.act_arm_joint_eff and 
        self.arm_joint_eff_diff and self.gripper_open and self.grasping):
        
            self.observations = {'Base':{'Commanded Velocity': self.cmd_base_vel,
        'Actual Velocity': self.act_base_velocity,
        'Velocity Difference': self.base_vel_diff,
        'Accelerations': self.base_acc,
        'Jerk': self.base_jerk},
        'Manipulator': {'Commanded Joint Positions': self.cmd_arm_joint_pos, 
        'Actual Joint Positions': self.act_arm_joint_pos,
        'Commanded Joint Velocity': self.cmd_joint_vel,
        'Actual Joint Velocity': self.act_joint_vel,
        'Joint Velocity Difference': self.joint_vel_diff,
        'Joint Acceleration': self.joint_acc,
        'Joint Jerk': self.joint_jerk,
        'Commanded Joint Effort': self.cmd_joint_eff,
        'Actual Joint Effort': self.act_joint_eff,
        'Joint Effort Difference': self.joint_eff_diff},
        'Gripper': {'Open': self.gripper_open, 'Grasping': self.grasping}}
        
    def write_file(self):
        obs_file = open('../objects/observations.yaml', 'w')
        yaml.dump(self.observations,obs_file)
        obs_file.close()

    def read_file(self):
        obs_file = open('../objects/observations.yaml', 'r')
        obs_from_file = yaml.load(obs_file)
        obs_file.close()
        return obs_from_file

def callback_1(msg):
    self.cmd_base_vel=[]
def callback_2(msg):
    self.act_base_velocity=[]
def callback_3(msg):
    self.base_vel_diff=[]
def callback_4(msg):
    self.base_acc=[]
def callback_5(msg):
    self.base_jerk=[]
def callback_6(msg):
    self.cmd_arm_joint_pos=[]
def callback_7(msg):
    self.cmd_arm_joint_vel=[]
def callback_8(msg):
    self.cmd_arm_joint_eff=[]
def callback_9(msg):
    self.act_arm_joint_eff=[]
    self.act_arm_joint_vel=[]
    self.arm_joint_acc=[]
    self.arm_joint_jerk=[]
def callback_10(msg):
    self.arm_joint_pos_diff=[]
def callback_11(msg):
    self.arm_joint_vel_diff=[]
def callback_12(msg):
    self.arm_joint_eff_diff=[]
def callback_13(msg):
    self.gripper_open=[]
def callback_14(msg):
    self.grasping=[]


#def callback_15(msg):
#def callback_16(msg):
    
def listener(): 
    rospy.Subscriber("cmd_vel", Twist, callback_1)
    rospy.Subscriber("odom", Odometry, callback_2)
    rospy.Subscriber("/BRSU_HMM_EED_observations/base_velocity_difference", Twist, callback_3)
    rospy.Subscriber("/BRSU_HMM_EED_observations/base_acceleration", Twist, callback_4)
    rospy.Subscriber("/BRSU_HMM_EED_observations/base_jerk", Twist, callback_5)
    rospy.Subscriber("/arm_controller/position_command", Twist, callback_6)
    rospy.Subscriber("/arm_controller/velocity_command", Twist, callback_7)
    rospy.Subscriber("/arm_controller/torque_command", Twist, callback_8)
    rospy.Subscriber("/joint_states", Twist, callback_9)
    rospy.Subscriber("/BRSU_HMM_EED_observations/arm_joint_position_difference", Twist, callback_10)
    rospy.Subscriber("/BRSU_HMM_EED_observations/arm_joint_velocity_difference", Twist, callback_11)
    rospy.Subscriber("/BRSU_HMM_EED_observations/arm_joint_effort_difference", Twist, callback_12)
    rospy.Subscriber("/BRSU_HMM_EED_observations/gripper_status", Twist, callback_13)
    rospy.Subscriber("/BRSU_HMM_EED_observations/grasping_status", Twist, callback_14)
    #rospy.Subscriber("cmd_vel", Twist, callback_15)
    #rospy.Subscriber("cmd_vel", Twist, callback_16)

def talker():
    so = store_observations()
    pub = rospy.Publisher('BRSU_HMM_EED_observations/all', Float64 ) #What type??
    pub.publish(so.observation_dictionary)
    #rospy.sleep(5.0)

if __name__ == '__main__':
    rospy.init_node('HMM_observations', anonymous=True)
    
    while not rospy.is_shutdown():
        try:
            listener()
            talker()
            rospy.sleep(1)
        except rospy.ROSInterruptException:
            pass