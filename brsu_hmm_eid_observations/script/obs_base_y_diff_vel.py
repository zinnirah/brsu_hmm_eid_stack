#!/usr/bin/env python
import roslib; roslib.load_manifest('brsu_hmm_eid_observations')
import rospy
import math
import yaml
#from std_msgs.msg import String
from std_msgs.msg import *
from nav_msgs.msg import Odometry
from geometry_msgs.msg import *

from brsu_hmm_eid_messages.msg import obs_base_y_vel_diff, obs_base_y_cmd_vel
vel_cmd_y = 0
vel_act_y = 0


def callback_1(msg):
    #rospy.loginfo("Received forward velocity <</cmd_vel>> message! The value is %f" %msg.linear.y)
    global vel_cmd_y
    vel_cmd_y = msg.linear.y
    #scy = store_cmd_y()

    if vel_cmd_y is None:
        if rospy.Time.now().secs == 0:
            #scy.write_file(vel_cmd_y=0)
            vel_cmd_y=0
        else:
            pass
    else:
        vel_cmd_y = msg.linear.y
        #scy.write_file(vel_cmd_y)

def callback_2(msg):
    #rospy.loginfo("Received actual y <</odom>> message! The value is %f"%msg.twist.twist.linear.y)
    global vel_act_y
    vel_act_y = msg.twist.twist.linear.y
    #say = store_act_y()
    
    if vel_act_y is None:
        #scy.write_file(vel_act_y=0)
        vel_act_y = 0
    else:
        vel_act_y = msg.twist.twist.linear.y
        #say.write_file(vel_act_y)

def listener():
    rospy.Subscriber("cmd_vel", Twist, callback_1)
    rospy.Subscriber("odom", Odometry, callback_2)

def talker():
    scy = store_cmd_y()
    say = store_act_y()

    cmd = vel_cmd_y # scy.read_file()
    print 'cmd:',cmd
    act = vel_cmd_y # say.read_file()
    print 'act:',act

    #try:
    if cmd is None:
        cmd = 0
    if act is None:
        act = 0
        
    difference = cmd - act
    
    rospy.loginfo("difference: %f"%difference)
    pub1 = rospy.Publisher('/brsu_hmm_eid_observations/base_y_vel_diff', obs_base_y_vel_diff)
    pub2 = rospy.Publisher('/brsu_hmm_eid_observations/base_y_vel_cmd', obs_base_y_cmd_vel)#, latch=True)

    msg2send1 = obs_base_y_vel_diff()
    msg2send1.header.stamp.secs = rospy.Time.now().secs
    msg2send1.y_vel_diff = difference
    pub1.publish(msg2send1)
    
    msg2send2 = obs_base_y_cmd_vel()
    msg2send2.header.stamp.secs = rospy.Time.now().secs
    msg2send2.y_cmd_vel = cmd
    pub2.publish(msg2send2)


if __name__ == '__main__':
    rospy.init_node('brsu_hmm_eid_observations_node_base_y_vel_diff', anonymous=True)

    while not rospy.is_shutdown():
        try:
            listener()
            talker()
            rospy.sleep(1)
        except rospy.ROSInterruptException:
            pass


