#!/usr/bin/env python

import roslib; roslib.load_manifest('brsu_hmm_eid_observations')
import roslib.message
import rospy

from sensor_msgs.msg import JointState
from brsu_hmm_eid_messages.msg import obs_arm_JointPositions, obs_arm_JointVelocities, obs_arm_JointTorques, obs_arm_JointAccelerations, obs_arm_JointJerks
from brics_actuator.msg import JointPositions, JointVelocities, JointTorques

#t = rospy.Time.now().secs

def subscribe_cmd_pvt():
    

    rospy.Subscriber("/arm_controller/position_command", JointPositions, callback1)
    rospy.Subscriber("/arm_controller/velocity_command", JointVelocities, callback2)
    rospy.Subscriber("/arm_controller/torque_command", JointTorques, callback3)


def callback1(msg):

    msg_to_send = obs_arm_JointPositions()
    msg_to_send.header.stamp.secs = rospy.Time.now().secs  # rospy.Time.now()
    msg_to_send.positions = msg.positions
    print msg.positions
    pub = rospy.Publisher('/brsu_hmm_eid_observations/armj_cmd_position', obs_arm_JointPositions)
    pub.publish(msg_to_send)


def callback2(msg):

    msg_to_send = obs_arm_JointVelocities()
    msg_to_send.header.stamp.secs = rospy.Time.now().secs  # rospy.Time.now()
    msg_to_send.velocities = msg.velocities
    print msg.velocities
    pub = rospy.Publisher('/brsu_hmm_eid_observations/armj_cmd_velocity', obs_arm_JointVelocities)
    pub.publish(msg_to_send)


def callback3(msg):

    msg_to_send = obs_arm_JointTorques()
    msg_to_send.header.stamp.secs = rospy.Time.now().secs  # rospy.Time.now()
    msg_to_send.torques = msg.torques
    print msg.torques
    pub = rospy.Publisher('/brsu_hmm_eid_observations/armj_cmd_effort', obs_arm_JointTorques)
    pub.publish(msg_to_send)

if __name__ == '__main__':

    rospy.init_node('brsu_hmm_eid_node_armj_republish', anonymous=True)

    while not rospy.is_shutdown():
        try:
            subscribe_cmd_pvt()
            rospy.sleep(1)
        except rospy.ROSInterruptException:
            pass
