#!/usr/bin/env python

import numpy as np
#import ghmm as ghmm
import yaml
import rospy

#HMM library
from sklearn import hmm

from brsu_hmm_eid_msgs import obs_base.msg

class get_continuous_HMM:
    def __init__(self):
        #self.nos = 4
        self.a = [0]*4
        self.b_means = [0]*4
        self.b_covar = .5 * np.tile(np.identity(3), (4, 1, 1))
        self.pai = [0]*4
        self.model = [0]*4

    def write_model_file(self):#, pai, a, b_means, b_covar):
        #self.pai= pai
        #self.a = a
        #self.b_means = b_means
        #self.b_covar = b_covar
        hmm_file = open('/home/zinnirah/ros/workspace/thesis/HMM_Diagnosis_Engine/scripts/base_parallel_hmm.yaml', 'w')
        hmm_details = {'PI-initial_state_distribution':[self.pai],
            'A-state_transition_matrix':[self.a],
            'B_means-emission_probabilities':[self.b_means]}#,
            #'B_covar-emission_probabilities': [self.b_covar]}
        yaml.dump(hmm_details,hmm_file)
        hmm_file.close()

    def read_model_file(self):
        hmm_file = open('/home/zinnirah/ros/workspace/thesis/HMM_Diagnosis_Engine/scripts/base_parallel_hmm.yaml', 'r')
        hmm_details = yaml.load(hmm_file)
        self.a = hmm_details["A-state_transition_matrix"][0]
        print 'first a: ', self.a
        self.b_means = hmm_details["B_means-emission_probabilities"][0]
       # self.b_covar =hmm_details["B_covar-emission_probabilities"]
        self.pai = hmm_details["PI-initial_state_distribution"][0]
        hmm_file.close()

    def get_initial_state_probabilties(self):
        while True:
            pai = raw_input("Enter initial state probabilities: ")#python 2.7, use input for python 3.x
            while True:
                ans = raw_input("You have entered: " + pai + "\nIs this correct?(y/n) ")
                if ans == 'y':
                    print("Storing initial state probabilities...")
                    self.pai=pai
                    break
                elif ans == 'n':
                    break
                elif ans is not ('n' or 'y'):
                    print("Please enter only 'y' or 'n': ")
                    continue
            if ans == 'n':
                continue
            else:
                break

    def get_state_transition_matrix(self):
        while True:
            a = raw_input("Enter state transition matrix (A): ")#python 2.7, use input for python 3.x
            while True:
                ans = raw_input("You have entered: " + a + "\nIs this correct?(y/n) ")
                if ans == 'y':
                    print("Storing state transition matrix (A)...")
                    self.a=a
                    break
                elif ans=='n':
                    break
                elif ans is not ('n' or 'y'):
                    print("Please enter only 'y' or 'n': ")
                    continue
            if ans == 'n':
                continue
            else:
                break

    def get_output_probabilities(self):
        while True:
            b_means = raw_input("Enter emission probabilities (B): ")#python 2.7, use input for python 3.x
            while True:
                ans = raw_input("You have entered: " + b_means + "\nIs this correct?(y/n) ")
                if ans == 'y':
                    print("Storing initial state probabilities...")
                    self.b_means=b_means
                    break
                elif ans=='n':
                    break
                elif ans is not ('n' or 'y'):
                    print("Please enter only 'y' or 'n': ")
                    continue
            if ans == 'n':
                continue
            else:
                break

    def set_model(self):
        print 'A= ', self.a
        print 'B_means= ', self.b_means
        print 'B_covar= ', self.b_covar
        print 'PI= ', self.pai
        #all_in = np.array(self.a, self.b_means, self.pai
        self.a = np.array(self.a)
        self.b_means = np.array(self.b_means)
        self.pai = np.array(self.pai)
        #print len(self.pai)
        #print len(self.a)
        ncomps = len(self.a)
        self.model = hmm.GaussianHMM(5, covariance_type="full", startprob=self.pai, transmat=self.a)
        self.model.means_ = self.b_means
        self.model.covars_= self.b_covar
        print 'MODEL= ', self.model

class get_observations():
    def __init__(self):
        self.obs = []
        self.m = []

    def from_sample(self,model):
        print '\nSample Single:'
        self.m =model

        self.obs, predictme = self.m.sample(10)
        #print self.m
        print 'Observed: ', self.obs
        print 'I predict: ', predictme
        w=self.m.predict_proba(self.obs)
        print 'w= ', w

    def from_sqd_file(self):
        print('OK')

    def from_ros_topics(self):
        rospy.Subscriber('brsu_hmm_eid_observations/gripper', obs_gripper, self.callback)


    def callback(msg):
        obs = msg.cmd_finger_position + act_finger_position
        obs = obs.append(msg.grasping_status)
        self.obs = np.array(obs)

class predict_state():
    def __init_(self):
        self.predict_matrix = []
        self.predict = ["grasping", "not_grasping", "exo_intervention"]# better define this in hmm.yaml
        self.model= []
        self.obs=[]

    def execute(self, model, obs):
        self.model = model
        self.obs = obs

        self.predict_matrix = self.model.predict_proba(self.obs)
        max_prob = max(self.predict_matrix)
        predict_index = predict_matrix.index(max_prob)

        print 'I observe: ', self.obs
        print 'I diagnose: ', self.predict[predict_index]

class update_model():
    def __init__(self):
        self.model=get_continuous_HMM().model

    def update_parameters(self):
        print('OK')
        self.v=9
        model=3
    def update_structure():
        print('OK')

def talker(prediction):
    pub = rospy.Publisher('brsu_hmm_eid_observations/gripper_diagnosis', base_diagnosis)
    rospy.loginfo("I predict: ", prediction)
    pub.publish(prediction)


if __name__ == '__main__':
    #initialization
    rospy.init_node("hmm_brsu_eid_node/gripper_diagnosis")

    gcHMM=get_continuous_HMM()
    go = get_observations()
    ps = predict_state()
    um = update_model()

    #Get the model
    print("Please enter your model in the following file: base_hmm.yaml")
    #gcHMM.write_model_file()
    gcHMM.read_model_file()
    gcHMM.set_model()

    #Get Observations
    try:
        rospy.loginfo("Getting observations from rostopic: /brsu_hmm_eid_observations/gripper" )
        go.from_ros_topics()
        go.from_sample(gcHMM.model)
        #Predict States
        ps.execute(gcHMM.model, go.obs)
        #publish ourfmdiagnosis
        talker(ps.predict)
        rospy.sleep(1)
    except rospy.ROSInterruptException:
        pass

