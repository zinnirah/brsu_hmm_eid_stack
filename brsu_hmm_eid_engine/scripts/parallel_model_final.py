#!/usr/bin/env python

import roslib; roslib.load_manifest('brsu_hmm_eid_engine')
import numpy as np
import message_filters
#import ghmm as ghmm
import yaml
import rospy

#HMM library
from sklearn import hmm
from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry
from sensor_msgs.msg import JointState
from brsu_hmm_eid_messages.msg import  diag_base, diag_arm, diag_final

class get_continuous_HMM:
    def __init__(self):
        #self.nos = 4
        #STOP, ACC, DEC, CONST
        self.states = ['stop', 'executing_task', 'exo_int']
        self.a = [[0.4, 0.4, 0.2], [0.4, 0.4, 0.2],[0.5, 0.1, 0.4]]
        self.b_means = [[0, 0],[2, 1],[4,2]]
        stop_covar = 0.0001*np.identity(2)
        exec_covar = 1*np.identity(2)
        ei_covar = 0.0001*np.identity(2)
        self.b_covar =  0.5 *np.tile(np.identity(2), (2, 1, 1))
        self.pai = [0.4, 0.4, 0.2]#[0]*3
        self.model = [0]*3
        self.obs=[]
        self.vel_cmd_status =0
        self.prediction_matrix = 0
        self.predicted_state_no = 0
        self.obs_seq= []
        self.prob_dist = []
        self.base_diagnosis = 0
        self.arm_diagnosis = 0


    def write_model_file(self):#, pai, a, b_means, b_covar):
        #self.pai= pai
        #self.a = a
        #self.b_means = b_means
        #self.b_covar = b_covar
        hmm_file = open('/home/zinnirah/ros/workspace/thesis/brsu_hmm_eid_engine/scripts/arm_parallel_hmm.yaml', 'w')
        hmm_details = {'PI-initial_state_distribution':[self.pai],
            'A-state_transition_matrix':[self.a],
            'B_means-emission_probabilities':[self.b_means]}#,
            #'B_covar-emission_probabilities': [self.b_covar]}
        yaml.dump(hmm_details,hmm_file)
        hmm_file.close()

    def read_model_file(self):
        hmm_file = open('/home/zinnirah/ros/workspace/thesis/brsu_hmm_eid_engine/scripts/arm_parallel_hmm.yaml', 'r')
        hmm_details = yaml.load(hmm_file)
        self.a = hmm_details["A-state_transition_matrix"][0]
        print 'first a: ', self.a
        self.b_means = hmm_details["B_means-emission_probabilities"][0]
        self.pai = hmm_details["PI-initial_state_distribution"][0]
        hmm_file.close()

    def set_model(self):

        self.a = np.array(self.a)
        self.b_means = np.array(self.b_means)
        self.pai = np.array(self.pai)
        ncomps = len(self.a)
        self.model = hmm.GaussianHMM(3, covariance_type="full", startprob=self.pai, transmat=self.a, random_state=42)
        self.model.means_ = self.b_means
        self.model.covars_= self.b_covar

    def from_ros_topics(self):

        #base_diagnosis = message_filters.Subscriber('brsu_hmm_eid_engine/parallel_base_diagnosis', diag_base)
        #arm_diagnosis = message_filters.Subscriber('brsu_hmm_eid_engine/parallel_arm_diagnosis', diag_arm)
        #ts = message_filters.TimeSynchronizer([base_diagnosis, arm_diagnosis], 1)
        #ts.registerCallback(self.callback)
        #print 'OK'
        
        rospy.Subscriber('brsu_hmm_eid_engine/parallel_base_diagnosis', diag_base, self.diag_base_callback)
        rospy.Subscriber('brsu_hmm_eid_engine/parallel_arm_diagnosis', diag_arm, self.diag_arm_callback)
        

    def diag_base_callback(self,msg):
        
        if msg is None:
            self.base_diagnosis = 0
        else:
            self.base_diagnosis = msg.diagnosis_base_no
    
    def diag_arm_callback(self,msg):
        
        if msg is None:
            self.arm_diagnosis = 0
        else:
            self.arm_diagnosis = msg.diagnosis_arm_no
            
    def callback(self, base_diagnosis, arm_diagnosis):
        
        assert base_diagnosis.header.stamp == arm_diagnosis.header.stamp
        
        bd = base_diagnosis.diagnosis_base_no
        ad = arm_diagnosis.diagnosis_arm_no
        #print x_jerk
        print 'OK'
        obs = [0]*2
        obs[0] = self.base_diagnosis
        obs[1] = ad
        obs_array= np.array([obs])
        
        #print obs_array
        self.prediction_matrix=self.model.predict_proba(obs_array)
        print 'wf= ', self.prediction_matrix
        #self.predicted_state_no = self.prediction_matrix.argmax()
        self.predicted_state_no = w[len(w)-1].argmax()
        print self.predicted_state_no
        self.prob_dist =self.prediction_matrix[len(self.prediction_matrix)-1]
        print self.prob_dist
        #print self.states[self.predicted_state_no]
        self.talker()

    def predict(self):
        
        self.obs = [0]*2
        self.obs[0] = self.base_diagnosis
        self.obs[1] = self.arm_diagnosis
        #obs_array= np.array([obs])
        
        self.obs_seq.append(self.obs)
        obs_array = np.array(self.obs_seq)
        #print obs_array
        self.prediction_matrix=self.model.predict_proba(obs_array)
        print 'wf= ', self.prediction_matrix
        self.predicted_state_no = self.prediction_matrix[len(self.prediction_matrix)-1].argmax()
        print self.predicted_state_no
        self.prob_dist =self.prediction_matrix[len(self.prediction_matrix)-1]
        print self.prob_dist
        #print self.states[self.predicted_state_no]
        self.talker()
        
    def talker(self):
        time_now = rospy.Time.now()
        pub = rospy.Publisher('brsu_hmm_eid_engine/parallel_final_diagnosis', diag_final)
        msg2send = diag_final()
        msg2send.header.stamp.secs = time_now.secs
        msg2send.header.stamp.nsecs = time_now.nsecs
        msg2send.diagnosis_final = self.states[self.predicted_state_no]  # armj_act_positions
        msg2send.diagnosis_final_no = self.predicted_state_no
        msg2send.prob_dist = self.prob_dist
        pub.publish(msg2send)#rospy.loginfo("I predict: ", prediction)




if __name__ == '__main__':
    #initialization
    rospy.init_node("hmm_brsu_eid_engine_parallel_final_diagnosis")

    gcHMM=get_continuous_HMM()
    gcHMM.set_model()

    #Get Observations
    r = rospy.Rate(1)
    while not rospy.is_shutdown():
        try:

            gcHMM.from_ros_topics()
            gcHMM.predict()
            r.sleep()
        except rospy.ROSInterruptException:
            pass

