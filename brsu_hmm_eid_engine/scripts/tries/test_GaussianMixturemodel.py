from ghmm import *

print "\n\n\n *** GaussianMixture Model ***"
F = Float()#
m2 = HMMFromMatrices(F,GaussianDistribution(F),
                         [[0.0,1.0,0],[0.5,0.0,0.5],[0.3,0.3,0.4]],
                         [[0.0,1.0],[-1.0,0.5], [1.0,0.2]],
                         [1.0,0,0])            		   
m_mix = HMMFromMatrices(F,GaussianMixtureDistribution(F),
                         [[0.0,1.0,0],[0.5,0.0,0.5],[0.3,0.3,0.4]],
                         
                         [  [ [10.0,40.0], [1,1], [0.5,0.5]  ],
                            [ [-10.0,-0.5], [2,2], [0.5,0.5] ],
                            [ [1.0,0.2], [3,3],  [0.5,0.5]  ]
                         ],

                         [1.0,0,0])

print m_mix


trans = m2.getTransition(2,0)
print "a[2,0] = " + str(trans)
                         
print "\nSample:"
mseq1 = m_mix.sample(4,5)                         
print str(mseq1) + "\n"


print "\nSampleSingle:"
mseq2 = m_mix.sampleSingle(1)                         
print str(mseq2) + "\n"

print "\nViterbi"
mixpath = m_mix.viterbi(mseq2)
print mixpath

juju=m_mix.getEmission(0,0)
print '\njuju: ', juju

print "\nForward"
logp = m_mix.loglikelihood(mseq2)    
print "logp = " + str(logp) + "\n"

print "\nForward matrices"
(salpha,sscale) = m_mix.forward(mseq2)
print "alpha:\n" + str(salpha) + "\n"
print "scale = " + str(sscale) + "\n"	

print "\nBackward matrices"
beta = m_mix.backward(mseq2,sscale)
print "beta = \n " + str(beta) + "\n"
