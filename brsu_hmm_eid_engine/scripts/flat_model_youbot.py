#!/usr/bin/env python

import roslib; roslib.load_manifest('brsu_hmm_eid_engine')
import numpy as np
import message_filters
#import ghmm as ghmm
import yaml
import rospy

#HMM library
from sklearn import hmm
from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry
from sensor_msgs.msg import JointState
from brsu_hmm_eid_messages.msg import obs_arm, obs_arm_JointPositions, obs_arm_JointVelocities, obs_arm_JointTorques, obs_arm_JointAccelerations, obs_arm_JointJerks, obs_arm_JointPositions_rel_diff, obs_arm_JointTorques_rel_diff, diag_flat

class get_continuous_HMM:
    def __init__(self):
        
        #Youbot
        self.joint_names = ["arm_joint_1", "arm_joint_2", "arm_joint_3", "arm_joint_4", "arm_joint_5","gripper_finger_joint_l","gripper_finger_joint_r"]
        #Jenny
        #self.joint_names = ['arm_1_joint', 'arm_2_joint', 'arm_3_joint', 'arm_4_joint', 'arm_5_joint', 'arm_6_joint', 'arm_7_joint']
        
        #self.nos = 4
        #STOP, ACC, DEC, CONST
        self.a = [[0.4, 0.4, 0.2], [0.4, 0.4, 0.2],[0.5, 0.1, 0.4]]
        self.states = ['stop', 'executing_task', 'exo_int']
        
        self.b_means = [[0.000097,0.000089],[0.125825,0.843481],[0.003578,0.934186]]
        #self.b_means = [[0.000097,0.000089,0.000419,0.000503,-0.001028,0.004717],[0.125825,0.843481,1.26198,1.441885,1.237277,0.698006],[0.003578,0.934186,0.000419,0.000503,-0.001028,0.004717]]
        #self.b_means = [[0.000089,0.000419,0.000503,-0.001028,0.004717], [0.843481,1.26198,1.441885,1.237277,0.698006],[0.934186,0.000419,0.000503,-0.001028,0.004717]]
        self.b_covar = .05 * np.tile(np.identity(2), (2, 1, 1))
        self.pai = [0.4,0.4, 0.2]#[0]*3
        self.model = [0]*3
        self.obs_arm1 = 0
        self.obs_arm2 = 0#armj_vel[1]
        self.obs_arm3 = 0#armj_vel[2]
        self.obs_arm4 = 0#armj_vel[3]
        self.obs_arm5 = 0#armj_vel[4]
        self.obs_arm = []
        self.obs_base = 0
        self.obs=[]
        self.predicted_state_no = 0
        self.prob_dist = []
        self.obs_seq=[]

    def write_model_file(self):#, pai, a, b_means, b_covar):
        #self.pai= pai
        #self.a = a
        #self.b_means = b_means
        #self.b_covar = b_covar
        hmm_file = open('/home/zinnirah/ros/workspace/thesis/brsu_hmm_eid_engine/scripts/arm_parallel_hmm.yaml', 'w')
        hmm_details = {'PI-initial_state_distribution':[self.pai],
            'A-state_transition_matrix':[self.a],
            'B_means-emission_probabilities':[self.b_means]}#,
            #'B_covar-emission_probabilities': [self.b_covar]}
        yaml.dump(hmm_details,hmm_file)
        hmm_file.close()

    def read_model_file(self):
        hmm_file = open('/home/zinnirah/ros/workspace/thesis/brsu_hmm_eid_engine/scripts/arm_parallel_hmm.yaml', 'r')
        hmm_details = yaml.load(hmm_file)
        self.a = hmm_details["A-state_transition_matrix"][0]
        print 'first a: ', self.a
        self.b_means = hmm_details["B_means-emission_probabilities"][0]
       # self.b_covar =hmm_details["B_covar-emission_probabilities"]
        self.pai = hmm_details["PI-initial_state_distribution"][0]
        hmm_file.close()

    def set_model(self):
        #print 'A= ', self.a
        #print 'B_means= ', self.b_means
        #print 'B_covar= ', self.b_covar
        #print 'PI= ', self.pai
        #all_in = np.array(self.a, self.b_means, self.pai
        self.a = np.array(self.a)
        self.b_means = np.array(self.b_means)
        self.pai = np.array(self.pai)
        #print len(self.pai)
        #print len(self.a)
        #ncomps = len(self.a)
        self.model = hmm.GaussianHMM(3, covariance_type="full", startprob=self.pai, transmat=self.a, random_state=42)
        self.model.means_ = self.b_means
        self.model.covars_= self.b_covar
        #print 'MODEL= ', self.model
        
        #obs, predictme = self.model.sample(100)
        #print obs
        #predict = self.model.predict_proba(obs)
        #print 'I predict', predictme

    def from_ros_topics(self):
        #rospy.Subscriber('/brsu_hmm_eid_observations/armj_all', obs_arm, self.callback)
        #ARM
        #pub1 = message_filters.Subscriber('/brsu_hmm_eid_observations/armj_cmd_positions', obs_arm_JointPositions)#obs_arm.jcmd_pos)
        #armj_act_pos = message_filters.Subscriber('/brsu_hmm_eid_observations/armj_positions', obs_arm_JointPositions)#obs_arm.jact_pos)
        #armj_rel_diff_pos = message_filters.Subscriber('/brsu_hmm_eid_observations/armj_rel_diff_positions', obs_arm_JointPositions_rel_diff)#obs_arm.diff_pos)
        #armj_act_vel = message_filters.Subscriber('/brsu_hmm_eid_observations/armj_velocities', obs_arm_JointVelocities)#obs_arm.jact_vel)
        #armj_act_eff = message_filters.Subscriber('/brsu_hmm_eid_observations/armj_effort', obs_arm_JointTorques)#obs_arm.jact_effort)
        #armj_rel_diff_eff = message_filters.Subscriber('/brsu_hmm_eid_observations/armj_rel_diff_effort', obs_arm_JointTorques_rel_diff)#obs_arm.diff_effort)
        #armj_acc = message_filters.Subscriber('/brsu_hmm_eid_observations/armj_accelerations', obs_arm_JointAccelerations)
        #armj_jerk = message_filters.Subscriber('/brsu_hmm_eid_observations/armj_jerk',  obs_arm_JointJerks)
        #pub12 = message_filters.Subscriber('/brsu_hmm_eid_observations/armj_all', obs_arm)
        
        #ts = message_filters.TimeSynchronizer([armj_act_pos, armj_rel_diff_pos, armj_act_vel, armj_act_eff, armj_rel_diff_eff, armj_acc, armj_jerk], 2)
        #ts.registerCallback(self.callback)
        #rospy.Subscriber("cmd_vel", Twist, self.cmd_vel_callback)
        rospy.Subscriber("/odom", Odometry, self.odom_callback)
        #rospy.Subscriber('/brsu_hmm_eid_observations/base_x_acc', obs_base_x_acc, self.base_x_acc_callback)
        #rospy.Subscriber('/brsu_hmm_eid_observations/base_x_jerk', obs_base_x_jerk, self.base_x_jerk_callback)
        rospy.Subscriber('/joint_states', JointState,self.JScallback )
        
    def JScallback(self, armj_act_states):
        self.obs_arm =[0]*5
        names_array = armj_act_states.name
        if (names_array  == self.joint_names):
            arm_name_index = names_array.index('arm_joint_1') 
            armj_vel = armj_act_states.velocity[arm_name_index:arm_name_index+5]
            #print len(armj_vel)
            for each in range(len(armj_vel)):
            #armj_vel_mat[each] = armj_vel[each]
                self.obs_arm[each] = armj_vel[each]
            self.obs_arm1 = armj_vel[0]
        #self.obs_arm2 = armj_vel[1]
        #self.obs_arm3 = armj_vel[2]
        #self.obs_arm4 = armj_vel[3]
        #self.obs_arm5 = armj_vel[4]
        #print armj_vel
        #self.obs_arm = armj_vel_mat
        #print self.obs_arm
        
    
    def cmd_vel_callback(self,msg):
        
        if msg is None:
            self.base_x_vel_cmd = 0
        else :
            self.base_x_vel_cmd = msg.linear.x
            self.base_x_vel_diff = self.base_x_vel_cmd - self.base_x_vel_act
        
    def odom_callback(self, msg):
        
       
        #self.base_x_vel_act = msg.twist.twist.linear.x
        #obs= np.array(msg.twist.twist.linear.x)
        self.obs_base = msg.twist.twist.linear.x
        #print obs

    def predict(self):
        #del self.obs[:]
        #print self.obs_arm
        self.obs = [0]*2#6
        self.obs[0] = self.obs_base
        self.obs[1] = self.obs_arm1
        #self.obs[2] = self.obs_arm2
        #self.obs[3] = self.obs_arm3
        #self.obs[4] = self.obs_arm4
        #self.obs[5] = self.obs_arm5

        #print self.obs
        #obs_array = np.array([self.obs])
        self.obs_seq.append(self.obs)
        obs_array = np.array(self.obs_seq)
        #print obs_array
        #obs_array= np.array([[-0.00288281, -0.01809231,  0.10805829, -0.01802594, -0.07465142,  0.02857961]])
        #w=self.model.predict_proba(y)
        w=self.model.predict_proba(obs_array)
        #print 'w= ', w
        self.predicted_state_no = w[len(w)-1].argmax()
        self.prob_dist = w[len(w)-1]
        print self.prob_dist
        print self.predicted_state_no
        
        self.talker()
    
    def talker(self):
        time_now = rospy.Time.now()
        pub = rospy.Publisher('brsu_hmm_eid_engine/flat_diagnosis', diag_flat)
        msg2send = diag_flat()
        msg2send.header.stamp.secs = time_now.secs
        msg2send.header.stamp.nsecs = time_now.nsecs
        msg2send.diagnosis_flat = self.states[self.predicted_state_no]  # armj_act_positions
        msg2send.diagnosis_flat_no = self.predicted_state_no
        msg2send.prob_dist = self.prob_dist
        pub.publish(msg2send)#rospy.loginfo("I predict: ", prediction)

if __name__ == '__main__':
    #initialization
    rospy.init_node("hmm_brsu_eid_engine_flat_diagnosis")

    gcHMM=get_continuous_HMM()
    #go = get_observations()
    #ps = predict_state()
    #um = update_model()

    #Get the model
    #print("Please enter your model in the following file: base_hmm.yaml")
    #gcHMM.write_model_file()
    #gcHMM.read_model_file()
    gcHMM.set_model()

    #Get Observations
    r = rospy.Rate(1)
    while not rospy.is_shutdown():
        try:
            #rospy.loginfo("Getting observations from rostopic: /brsu_hmm_eid_observations/base" )
            #gcHMM.set_model()
            gcHMM.from_ros_topics()
            gcHMM.predict()
            #go.from_sample(gcHMM.model)
            #Predict States
            #ps.execute(gcHMM.model, go.obs)
            #publish ourfmdiagnosis
            #talker(ps.predict)
            #r.sleep()
            rospy.sleep(1) #Comment this on the real robot to get faster response
        except rospy.ROSInterruptException:
            pass

