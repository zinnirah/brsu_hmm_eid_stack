 def write_model_file(self):#, pai, a, b_means, b_covar):
        #self.pai= pai
        #self.a = a
        #self.b_means = b_means
        #self.b_covar = b_covar
        hmm_file = open('/home/zinnirah/ros/workspace/thesis/HMM_Diagnosis_Engine/scripts/flat_hmm.yaml', 'w')
        hmm_details = {'PI-initial_state_distribution':[self.pai],
            'A-state_transition_matrix':[self.a],
            'B_means-emission_probabilities':[self.b_means]
            #'B_covar-emission_probabilities': [self.b_covar]}
        yaml.dump(hmm_details,hmm_file)
        hmm_file.close()

def get_initial_state_probabilties(self):
        while True:
            pai = raw_input("Enter initial state probabilities: ")#python 2.7, use input for python 3.x
            while True:
                ans = raw_input("You have entered: " + pai + "\nIs this correct?(y/n) ")
                if ans == 'y':
                    print("Storing initial state probabilities...")
                    self.pai=pai
                    break
                elif ans == 'n':
                    break
                elif ans is not ('n' or 'y'):
                    print("Please enter only 'y' or 'n': ")
                    continue
            if ans == 'n':
                continue
            else:
                break

    def get_state_transition_matrix(self):
        while True:
            a = raw_input("Enter state transition matrix (A): ")#python 2.7, use input for python 3.x
            while True:
                ans = raw_input("You have entered: " + a + "\nIs this correct?(y/n) ")
                if ans == 'y':
                    print("Storing state transition matrix (A)...")
                    self.a=a
                    break
                elif ans=='n':
                    break
                elif ans is not ('n' or 'y'):
                    print("Please enter only 'y' or 'n': ")
                    continue
            if ans == 'n':
                continue
            else:
                break

def get_output_probabilities(self):
        while True:
            b_means = raw_input("Enter emission probabilities (B): ")#python 2.7, use input for python 3.x
            while True:
                ans = raw_input("You have entered: " + b_means + "\nIs this correct?(y/n) ")
                if ans == 'y':
                    print("Storing initial state probabilities...")
                    self.b_means=b_means
                    break
                elif ans=='n':
                    break
                elif ans is not ('n' or 'y'):
                    print("Please enter only 'y' or 'n': ")
                    continue
            if ans == 'n':
                continue
            else:
                break
