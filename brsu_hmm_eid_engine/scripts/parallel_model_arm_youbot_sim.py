#!/usr/bin/env python

import roslib; roslib.load_manifest('brsu_hmm_eid_engine')
import numpy as np
import message_filters
#import ghmm as ghmm
import yaml
import rospy

#HMM library
from sklearn import hmm

from sensor_msgs.msg import JointState
from brsu_hmm_eid_messages.msg import obs_arm, obs_arm_JointPositions, obs_arm_JointVelocities, obs_arm_JointTorques, obs_arm_JointAccelerations, obs_arm_JointJerks, obs_arm_JointPositions_rel_diff, obs_arm_JointTorques_rel_diff,diag_arm

class get_continuous_HMM:
    def __init__(self):
        
        #Youbot
        self.joint_names = ["arm_joint_1", "arm_joint_2", "arm_joint_3", "arm_joint_4", "arm_joint_5","gripper_finger_joint_l","gripper_finger_joint_r"]
        #Jenny
        #self.joint_names = ['arm_1_joint', 'arm_2_joint', 'arm_3_joint', 'arm_4_joint', 'arm_5_joint', 'arm_6_joint', 'arm_7_joint']
        #self.nos = 4
        self.a =[[0.698642683, 0.00193902388712, 0.299418293],[0.673771857, 0.03746877602, 0.288759367],[0.25, 0.25, 0.5]]#[0]*3
        #self.b_means = [[0.000089,0.000089,0.000419,0.000419,0.000503,0.000503,-0.001028,-0.001028,0.004717,0.004717], [0.843481,-0.835568,1.26198,-0.867638,1.441885,-1.359551,1.237277,-1.273718,0.698006,-0.388471],[0,0,0,0,0,0,0,0,0,0]]
        #self.b_means = [[0.000089,0.000419,0.000503,-0.001028,0.004717], [0.843481,1.26198,1.441885,1.237277,0.698006],[0.934186,0.000419,0.000503,-0.001028,0.004717]]
        #self.b_means = [[0.000089,0.000419,0.000503], [0.843481,1.26198,1.441885],[0.934186,0.000419,0.000503]]
        self.b_means = [[0.000089, 0, 0], [0.843481, 1, 0],[0.5, 0.5, 1]]#0.934186
        
        self.b_covar = .02 * np.tile(np.identity(3), (3, 1, 1))
        self.pai = [0.4, 0.3, 0.3]#[0]*3
        self.model = [0]*3
        self.obs=[]
        self.obs_seq=[]
        self.prediction_matrix = 0
        self.predicted_state_no = 0
        self.states = ['stop', 'moving', 'exo_int']
        self.prob_dist = []
        self.armj_vel =0
        self.armj_acc =0
        self.armj_jerk =0

    def write_model_file(self):#, pai, a, b_means, b_covar):
        #self.pai= pai
        #self.a = a
        #self.b_means = b_means
        #self.b_covar = b_covar
        hmm_file = open('/home/zinnirah/ros/workspace/thesis/brsu_hmm_eid_engine/scripts/arm_parallel_hmm.yaml', 'w')
        hmm_details = {'PI-initial_state_distribution':[self.pai],
            'A-state_transition_matrix':[self.a],
            'B_means-emission_probabilities':[self.b_means]}#,
            #'B_covar-emission_probabilities': [self.b_covar]}
        yaml.dump(hmm_details,hmm_file)
        hmm_file.close()

    def read_model_file(self):
        hmm_file = open('/home/zinnirah/ros/workspace/thesis/brsu_hmm_eid_engine/scripts/arm_parallel_hmm.yaml', 'r')
        hmm_details = yaml.load(hmm_file)
        self.a = hmm_details["A-state_transition_matrix"][0]
        print 'first a: ', self.a
        self.b_means = hmm_details["B_means-emission_probabilities"][0]
       # self.b_covar =hmm_details["B_covar-emission_probabilities"]
        self.pai = hmm_details["PI-initial_state_distribution"][0]
        hmm_file.close()

    def set_model(self):
        #print 'A= ', self.a
        #print 'B_means= ', self.b_means
        #print 'B_covar= ', self.b_covar
        #print 'PI= ', self.pai
        #all_in = np.array(self.a, self.b_means, self.pai
        self.a = np.array(self.a)
        self.b_means = np.array(self.b_means)
        self.pai = np.array(self.pai)
        #print len(self.pai)
        #print len(self.a)
        ncomps = len(self.a)
        self.model = hmm.GaussianHMM(3, covariance_type="full", startprob=self.pai, transmat=self.a, random_state=42)
        self.model.means_ = self.b_means
        self.model.covars_= self.b_covar
        #print 'MODEL= ', self.model
        
        #obs, predictme = self.model.sample(2)
        #print obs
        #predict = self.model.predict_proba(obs)
        #print 'I predict', predictme

    def from_ros_topics(self):
        #rospy.Subscriber('/brsu_hmm_eid_observations/armj_all', obs_arm, self.callback)
        #ARM
        #pub1 = message_filters.Subscriber('/brsu_hmm_eid_observations/armj_cmd_positions', obs_arm_JointPositions)#obs_arm.jcmd_pos)
        #armj_act_pos = message_filters.Subscriber('/brsu_hmm_eid_observations/armj_positions', obs_arm_JointPositions)#obs_arm.jact_pos)
        #armj_rel_diff_pos = message_filters.Subscriber('/brsu_hmm_eid_observations/armj_rel_diff_positions', obs_arm_JointPositions_rel_diff)#obs_arm.diff_pos)
        #armj_act_vel = message_filters.Subscriber('/brsu_hmm_eid_observations/armj_velocities', obs_arm_JointVelocities)#obs_arm.jact_vel)
        #armj_act_eff = message_filters.Subscriber('/brsu_hmm_eid_observations/armj_effort', obs_arm_JointTorques)#obs_arm.jact_effort)
        #armj_rel_diff_eff = message_filters.Subscriber('/brsu_hmm_eid_observations/armj_rel_diff_effort', obs_arm_JointTorques_rel_diff)#obs_arm.diff_effort)
        #armj_pos_vel = message_filters.Subscriber('/joint_states', JointState)
        
        #armj_acc = message_filters.Subscriber('/brsu_hmm_eid_observations/armj_accelerations', obs_arm_JointAccelerations)
        #armj_jerk = message_filters.Subscriber('/brsu_hmm_eid_observations/armj_jerk',  obs_arm_JointJerks)
        #pub12 = message_filters.Subscriber('/brsu_hmm_eid_observations/armj_all', obs_arm)
        #ts = message_filters.TimeSynchronizer([armj_pos_vel,armj_acc,armj_jerk], 1)
        
        #ts.registerCallback(self.callback)
        #print 'ok'
        #rospy.Subscriber('/joint_states', JointState,self.JScallback )
        
        rospy.Subscriber('/joint_states', JointState, self.js_callback)
        rospy.Subscriber('/brsu_hmm_eid_observations/armj_accelerations', obs_arm_JointAccelerations, self.obs_armj_acc_callback)
        rospy.Subscriber('/brsu_hmm_eid_observations/armj_jerk',  obs_arm_JointJerks, self.obs_armj_jerk_callback)
        
    def js_callback(self, msg):
        
        names_array = msg.name
        #if (names_array  == self.joint_names):
        arm_name_index = names_array.index('arm_joint_1')
        self.armj_vel = msg.velocity[arm_name_index]#:arm_name_index+5]
    
    def obs_armj_acc_callback(self,msg):
        
        if msg is None:
            self.armj_acc = 0
        else :
            self.armj_acc = msg.accelerations[0]
            
    def obs_armj_jerk_callback(self,msg):
        
        if msg is None:
            self.armj_jerk = 0
        else :
            self.armj_jerk = msg.jerks[0]        
    
    def callback(self,armj_pos_vel,armj_acc,armj_jerk):
        #print 'ok'
        assert armj_pos_vel.header.stamp == armj_acc.header.stamp == armj_jerk.header.stamp
        
        names_array = armj_pos_vel.name
        #if (names_array  == self.joint_names):
        arm_name_index = names_array.index('arm_joint_1')
        
    #aj_pos = armj_pos_vel.position[8]
        aj_vel = armj_pos_vel.velocity[8]#8for youbot
   # aj_eff = armj_pos_vel.effort[8]
        #aj_acc = armj_acc.accelerations[0]
        #aj_jerk =armj_jerk.jerks[0]
    
        obs = [0]*1
        obs[0] = aj_vel
        #obs[1] = aj_acc
        #obs[2] = aj_jerk
        obs_array= np.array([obs])
        #print obs_array
        #w=self.model.predict_proba(obs_array)
        #print 'w= ', w
        self.prediction_matrix=self.model.predict_proba(obs_array)
        print 'wa= ', self.prediction_matrix
        self.predicted_state_no = self.prediction_matrix.argmax()
        print self.predicted_state_no
        #print self.states[self.predicted_state_no]
        
        self.talker()
    
    def predict(self):
        
        self.obs = [0]*3
        self.obs[0] = self.armj_vel
        self.obs[1] = self.armj_acc
        self.obs[2] = self.armj_jerk
        #obs_array = np.array([obs])
        #print obs_array
        #w=self.model.predict_proba(obs_array)
        #print 'w= ', w
        self.obs_seq.append(self.obs)
        obs_array = np.array(self.obs_seq)
        self.prediction_matrix=self.model.predict_proba(obs_array)
        print 'wa= ', self.prediction_matrix
        #self.predicted_state_no = self.prediction_matrix.argmax()
        self.predicted_state_no = self.prediction_matrix[len(self.prediction_matrix)-1].argmax()
        print self.predicted_state_no
        self.prob_dist =self.prediction_matrix[len(self.prediction_matrix)-1]
        print self.prob_dist
        #print self.states[self.predicted_state_no]
        
        self.talker()
    def talker(self):
        
        time_now = rospy.Time.now()
        pub = rospy.Publisher('brsu_hmm_eid_engine/parallel_arm_diagnosis', diag_arm)
        msg2send = diag_arm()
        msg2send.header.stamp.secs = time_now.secs
        msg2send.header.stamp.nsecs = time_now.nsecs
        msg2send.diagnosis_arm = self.states[self.predicted_state_no]  # armj_act_positions
        msg2send.diagnosis_arm_no = self.predicted_state_no
        msg2send.prob_dist = self.prob_dist
        pub.publish(msg2send)#rospy.loginfo("I predict: ", prediction)

if __name__ == '__main__':
    #initialization
    rospy.init_node("hmm_brsu_eid_engine_parallel_arm_diagnosis")

    gcHMM=get_continuous_HMM()
    gcHMM.set_model()


    while not rospy.is_shutdown():
        try:
            #rospy.loginfo("Getting observations from rostopic: /brsu_hmm_eid_observations/base" )
            #gcHMM.set_model()
            gcHMM.from_ros_topics()
            gcHMM.predict()
            rospy.sleep(1)
        except rospy.ROSInterruptException:
            pass

