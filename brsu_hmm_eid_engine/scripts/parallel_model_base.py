#!/usr/bin/env python

import roslib; roslib.load_manifest('brsu_hmm_eid_engine')
import numpy as np
import message_filters
#import ghmm as ghmm
import yaml
import rospy

#HMM library
from sklearn import hmm
from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry
from sensor_msgs.msg import JointState
from brsu_hmm_eid_messages.msg import  obs_base_x_vel_diff, obs_base_x_vel_cmd, obs_base_x_acc, obs_base_x_jerk,  obs_base_x_vel_act, diag_base

class get_continuous_HMM:
    def __init__(self):
        #self.nos = 4
        #STOP, ACC, DEC, CONST
        self.a = [[0.00254171, 0.005083419, 0.017791968, 0.899765247, 0.074817656], 
                  [0.0, 0.0, 0.0, 0.925182345,0.074817655],
                  [0.0, 0.0, 0.0, 0.925182345,0.074817655],
                  [0.321416515, 0.110098475, 0.323192297, 0.170475058, 0.074817655],
                  [0.2, 0, 0.4, 0, 0.4]]

        self.b_means = [[0.000097, 0, 0.000, 0.0, 0],[0.125825, 0.02, 0.02, 0.00, 1],[0.102268, 0.02, -0.02, 0.0, 1],[0.102268, 0.02, -0.02, 0.0, 1],[0.003578, 0.05, -0.02, 0.1, 1]]
        #self.b_means = [[0.000089,0.000419,0.000503,-0.001028,0.004717], [0.843481,1.26198,1.441885,1.237277,0.698006],[0.934186,0.000419,0.000503,-0.001028,0.004717]]
        self.b_covar = .0026 * np.tile(np.identity(5), (5, 1, 1))
        self.pai = [0.2, 0.2,0.2,0.3, 0.1]#[0]*3
        self.model = [0]*3
        self.obs=[]
        self.obs_seq=[]
        self.vel_cmd_status =0
        self.prediction_matrix = 0
        self.predicted_state_no = 0
        self.prob_dist = []
        self.base_x_vel = 0
        self.base_x_vel_diff = 0
        self.base_x_acc = 0
        self.base_x_jerk = 0
        
        self.states = ['stop', 'accelerating', 'decelerating', 'constant', 'exo_int']

    def write_model_file(self):#, pai, a, b_means, b_covar):
        #self.pai= pai
        #self.a = a
        #self.b_means = b_means
        #self.b_covar = b_covar
        hmm_file = open('/home/zinnirah/ros/workspace/thesis/brsu_hmm_eid_engine/scripts/arm_parallel_hmm.yaml', 'w')
        hmm_details = {'PI-initial_state_distribution':[self.pai],
            'A-state_transition_matrix':[self.a],
            'B_means-emission_probabilities':[self.b_means]}#,
            #'B_covar-emission_probabilities': [self.b_covar]}
        yaml.dump(hmm_details,hmm_file)
        hmm_file.close()

    def read_model_file(self):
        hmm_file = open('/home/zinnirah/ros/workspace/thesis/brsu_hmm_eid_engine/scripts/arm_parallel_hmm.yaml', 'r')
        hmm_details = yaml.load(hmm_file)
        self.a = hmm_details["A-state_transition_matrix"][0]
        print 'first a: ', self.a
        self.b_means = hmm_details["B_means-emission_probabilities"][0]
       # self.b_covar =hmm_details["B_covar-emission_probabilities"]
        self.pai = hmm_details["PI-initial_state_distribution"][0]
        hmm_file.close()

    def set_model(self):
        #print 'A= ', self.a
        #print 'B_means= ', self.b_means
        #print 'B_covar= ', self.b_covar
        #print 'PI= ', self.pai
        #all_in = np.array(self.a, self.b_means, self.pai
        self.a = np.array(self.a)
        self.b_means = np.array(self.b_means)
        self.pai = np.array(self.pai)
        #print len(self.pai)
        #print len(self.a)
        ncomps = len(self.a)
        self.model = hmm.GaussianHMM(5, covariance_type="full", startprob=self.pai, transmat=self.a, random_state=42)
        self.model.means_ = self.b_means
        self.model.covars_= self.b_covar
        #print 'MODEL= ', self.model
        
        #obs, predictme = self.model.sample(2)
        #print obs
        #predict = self.model.predict_proba(obs)
        #print 'I predict', predictme

    def from_ros_topics(self):
        
        rospy.Subscriber("cmd_vel", Twist, self.cmd_vel_callback)
        rospy.Subscriber("/odom", Odometry, self.odom_callback)
        rospy.Subscriber('/brsu_hmm_eid_observations/base_x_vel_diff', obs_base_x_vel_diff, self.base_x_vel_diff_callback)
        rospy.Subscriber('/brsu_hmm_eid_observations/base_x_acc', obs_base_x_acc, self.base_x_acc_callback)
        rospy.Subscriber('/brsu_hmm_eid_observations/base_x_jerk', obs_base_x_jerk, self.base_x_jerk_callback)
        
        #base_vel = message_filters.Subscriber("/odom", Odometry)
        #base_x_vel_diff = message_filters.Subscriber('/brsu_hmm_eid_observations/base_x_vel_diff', obs_base_x_vel_diff)
        #base_x_acc = message_filters.Subscriber('/brsu_hmm_eid_observations/base_x_acc', obs_base_x_acc)
        #base_x_jerk = message_filters.Subscriber('/brsu_hmm_eid_observations/base_x_jerk', obs_base_x_jerk)
        #ts = message_filters.TimeSynchronizer([base_vel, base_x_vel_diff, base_x_acc,base_x_jerk], 1)
        #ts.registerCallback(self.callback)
        
        #rospy.Subscriber("cmd_vel", Twist, self.cmd_vel_callback)
         
    def cmd_vel_callback(self,msg):
        
        if msg is None:
            self.base_x_vel_cmd = 0
            self.vel_cmd_status = 0
        else :
            self.base_x_vel_cmd = msg.linear.x
            self.vel_cmd_status = 1
            #print self.base_x_vel_cmd
            #self.base_x_vel_diff = self.base_x_vel_cmd - self.base_x_vel_act
    
    def odom_callback(self, msg):
        
        if msg is None:
            self.base_x_vel = 0
        else :
            self.base_x_vel = msg.twist.twist.linear.x
            
    def base_x_vel_diff_callback(self, msg):
        
        if msg is None:
            self.base_x_vel_diff = 0
        else :
            self.base_x_vel_diff = msg.x_vel_diff
    
    def base_x_acc_callback(self, msg):
        
        if msg is None:
            self.base_x_acc = 0
        else :
            self.base_x_acc = msg.x_acc
    
    def base_x_jerk_callback(self, msg):
        
        if msg is None:
            self.base_x_jerk = 0
        else :
            self.base_x_jer = msg.x_jerk
            

    def callback(self,base_vel, base_x_vel_diff, base_x_acc,base_x_jerk ):
        
        assert base_vel.header.stamp== base_x_vel_diff.header.stamp== base_x_acc.header.stamp==base_x_jerk.header.stamp
        
        vel_x = base_vel.twist.twist.linear.x
        x_vel_diff = base_x_vel_diff.x_vel_diff
        x_acc = base_x_acc.x_acc
        x_jerk = base_x_jerk.x_jerk
        #print x_jerk
        obs = [0]*5
        obs[0] = base_x_vel
        obs[1] = base_x_vel_diff
        obs[2] = x_acc 
        obs[3] = x_jerk 
        obs[4] = self.vel_cmd_status
        obs_array= np.array([obs])
        
        #print obs_array
        self.prediction_matrix=self.model.predict_proba(obs_array)
        print 'wb= ', self.prediction_matrix
        self.predicted_state_no = self.prediction_matrix.argmax()
        print self.predicted_state_no
        #print self.states[self.predicted_state_no]
        self.talker()

    def predict(self):
        
        self.obs = [0]*5
        self.obs[0] = self.base_x_vel
        self.obs[1] = self.base_x_vel_diff
        self.obs[2] = self.base_x_acc
        self.obs[3] = self.base_x_jerk
        self.obs[4] = self.vel_cmd_status
        #obs_array= np.array([obs])
        
        #print obs_array
        self.obs_seq.append(self.obs)
        obs_array = np.array(self.obs_seq)
        self.prediction_matrix=self.model.predict_proba(obs_array)
        print 'wb= ', self.prediction_matrix
        #self.predicted_state_no = self.prediction_matrix.argmax()
        self.predicted_state_no = self.prediction_matrix[len(self.prediction_matrix)-1].argmax()
        print self.predicted_state_no
        self.prob_dist =self.prediction_matrix[len(self.prediction_matrix)-1]
        print self.prob_dist
        #print self.states[self.predicted_state_no]
        self.talker()

    def talker(self):
        time_now = rospy.Time.now()
        pub = rospy.Publisher('brsu_hmm_eid_engine/parallel_base_diagnosis', diag_base)
        msg2send = diag_base()
        msg2send.header.stamp.secs = time_now.secs
        msg2send.header.stamp.nsecs = time_now.nsecs
        msg2send.diagnosis_base = self.states[self.predicted_state_no]  # armj_act_positions
        msg2send.diagnosis_base_no = self.predicted_state_no
        msg2send.prob_dist = self.prob_dist
        pub.publish(msg2send)#rospy.loginfo("I predict: ", prediction)
        
        


if __name__ == '__main__':
    #initialization
    rospy.init_node("hmm_brsu_eid_engine_parallel_base_diagnosis")
    gcHMM=get_continuous_HMM()
    gcHMM.set_model()


    while not rospy.is_shutdown():
        try:
            gcHMM.from_ros_topics()
            gcHMM.predict()
            rospy.sleep(1)
        except rospy.ROSInterruptException:
            pass

