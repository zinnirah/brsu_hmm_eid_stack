#include "ros/ros.h"

#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>

#include <geometry_msgs/Twist.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/JointState.h>

#include <brsu_hmm_eid_messages/obs_base_x_vel_diff.h>
#include <brsu_hmm_eid_messages/obs_base_x_vel_cmd.h>
#include <brsu_hmm_eid_messages/obs_base_x_acc.h>
#include <brsu_hmm_eid_messages/obs_base_x_jerk.h>
#include <brsu_hmm_eid_messages/obs_base_x_vel_act.h>
#include <brsu_hmm_eid_messages/diag_base.h>

using namespace ros;
using namespace message_filters;
using namespace geometry_msgs;
using namespace nav_msgs;
using namespace sensor_msgs;
using namespace brsu_hmm_eid_messages;

void callback(const TwistConstPtr& odom, const obs_base_x_vel_diff& x_vel_diff, const obs_base_x_accConstPtr& x_acc, const obs_base_x_jerkConstPtr& x_jerk )
{
  // Solve all of perception here...
	ros::
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "hmm_brsu_eid_engine_parallel_base_diagnosis");

  ros::NodeHandle nh;
  message_filters::Subscriber<Twist> odom(nh, "odom", 1);
  message_filters::Subscriber<obs_base_x_vel_diff> x_vel_diff(nh, "brsu_hmm_eid_observations/base_x_vel_diff", 1);
  message_filters::Subscriber<obs_base_x_acc> x_acc(nh, "brsu_hmm_eid_observations/base_x_acc", 1);
  message_filters::Subscriber<obs_base_x_jerk> x_jerk(nh, "brsu_hmm_eid_observations/base_x_jerk", 1);

  typedef sync_policies::ApproximateTime<Twist, obs_base_x_vel_diff, obs_base_x_acc, obs_base_x_jerk> MySyncPolicy;
  // ApproximateTime takes a queue size as its constructor argument, hence MySyncPolicy(10)
  Synchronizer<MySyncPolicy> sync(MySyncPolicy(10), odom, x_vel_diff, x_acc, x_jerk);
  sync.registerCallback(boost::bind(&callback, _1, _2, _3, _4));

  ros::spin();

  return 0;
}
