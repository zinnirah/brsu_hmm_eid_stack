/* Auto-generated by genmsg_cpp for file /home/zinnirah/ros/workspace/thesis/brsu_hmm_eid_stack/brsu_hmm_eid_messages/msg/obs_conclude.msg */
#ifndef BRSU_HMM_EID_MESSAGES_MESSAGE_OBS_CONCLUDE_H
#define BRSU_HMM_EID_MESSAGES_MESSAGE_OBS_CONCLUDE_H
#include <string>
#include <vector>
#include <map>
#include <ostream>
#include "ros/serialization.h"
#include "ros/builtin_message_traits.h"
#include "ros/message_operations.h"
#include "ros/time.h"

#include "ros/macros.h"

#include "ros/assert.h"

#include "std_msgs/Header.h"

namespace brsu_hmm_eid_messages
{
template <class ContainerAllocator>
struct obs_conclude_ {
  typedef obs_conclude_<ContainerAllocator> Type;

  obs_conclude_()
  : header()
  , base_diagnosis()
  , arm_diagnosis()
  , gripper_diagnosis()
  {
  }

  obs_conclude_(const ContainerAllocator& _alloc)
  : header(_alloc)
  , base_diagnosis(_alloc)
  , arm_diagnosis(_alloc)
  , gripper_diagnosis(_alloc)
  {
  }

  typedef  ::std_msgs::Header_<ContainerAllocator>  _header_type;
   ::std_msgs::Header_<ContainerAllocator>  header;

  typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _base_diagnosis_type;
  std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  base_diagnosis;

  typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _arm_diagnosis_type;
  std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  arm_diagnosis;

  typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _gripper_diagnosis_type;
  std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  gripper_diagnosis;


  typedef boost::shared_ptr< ::brsu_hmm_eid_messages::obs_conclude_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::brsu_hmm_eid_messages::obs_conclude_<ContainerAllocator>  const> ConstPtr;
  boost::shared_ptr<std::map<std::string, std::string> > __connection_header;
}; // struct obs_conclude
typedef  ::brsu_hmm_eid_messages::obs_conclude_<std::allocator<void> > obs_conclude;

typedef boost::shared_ptr< ::brsu_hmm_eid_messages::obs_conclude> obs_concludePtr;
typedef boost::shared_ptr< ::brsu_hmm_eid_messages::obs_conclude const> obs_concludeConstPtr;


template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const  ::brsu_hmm_eid_messages::obs_conclude_<ContainerAllocator> & v)
{
  ros::message_operations::Printer< ::brsu_hmm_eid_messages::obs_conclude_<ContainerAllocator> >::stream(s, "", v);
  return s;}

} // namespace brsu_hmm_eid_messages

namespace ros
{
namespace message_traits
{
template<class ContainerAllocator> struct IsMessage< ::brsu_hmm_eid_messages::obs_conclude_<ContainerAllocator> > : public TrueType {};
template<class ContainerAllocator> struct IsMessage< ::brsu_hmm_eid_messages::obs_conclude_<ContainerAllocator>  const> : public TrueType {};
template<class ContainerAllocator>
struct MD5Sum< ::brsu_hmm_eid_messages::obs_conclude_<ContainerAllocator> > {
  static const char* value() 
  {
    return "e8a5b05882c8f89b91ac24e01c2bc5bb";
  }

  static const char* value(const  ::brsu_hmm_eid_messages::obs_conclude_<ContainerAllocator> &) { return value(); } 
  static const uint64_t static_value1 = 0xe8a5b05882c8f89bULL;
  static const uint64_t static_value2 = 0x91ac24e01c2bc5bbULL;
};

template<class ContainerAllocator>
struct DataType< ::brsu_hmm_eid_messages::obs_conclude_<ContainerAllocator> > {
  static const char* value() 
  {
    return "brsu_hmm_eid_messages/obs_conclude";
  }

  static const char* value(const  ::brsu_hmm_eid_messages::obs_conclude_<ContainerAllocator> &) { return value(); } 
};

template<class ContainerAllocator>
struct Definition< ::brsu_hmm_eid_messages::obs_conclude_<ContainerAllocator> > {
  static const char* value() 
  {
    return "Header header\n\
\n\
string base_diagnosis\n\
string arm_diagnosis\n\
string gripper_diagnosis\n\
\n\
================================================================================\n\
MSG: std_msgs/Header\n\
# Standard metadata for higher-level stamped data types.\n\
# This is generally used to communicate timestamped data \n\
# in a particular coordinate frame.\n\
# \n\
# sequence ID: consecutively increasing ID \n\
uint32 seq\n\
#Two-integer timestamp that is expressed as:\n\
# * stamp.secs: seconds (stamp_secs) since epoch\n\
# * stamp.nsecs: nanoseconds since stamp_secs\n\
# time-handling sugar is provided by the client library\n\
time stamp\n\
#Frame this data is associated with\n\
# 0: no frame\n\
# 1: global frame\n\
string frame_id\n\
\n\
";
  }

  static const char* value(const  ::brsu_hmm_eid_messages::obs_conclude_<ContainerAllocator> &) { return value(); } 
};

template<class ContainerAllocator> struct HasHeader< ::brsu_hmm_eid_messages::obs_conclude_<ContainerAllocator> > : public TrueType {};
template<class ContainerAllocator> struct HasHeader< const ::brsu_hmm_eid_messages::obs_conclude_<ContainerAllocator> > : public TrueType {};
} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

template<class ContainerAllocator> struct Serializer< ::brsu_hmm_eid_messages::obs_conclude_<ContainerAllocator> >
{
  template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
  {
    stream.next(m.header);
    stream.next(m.base_diagnosis);
    stream.next(m.arm_diagnosis);
    stream.next(m.gripper_diagnosis);
  }

  ROS_DECLARE_ALLINONE_SERIALIZER;
}; // struct obs_conclude_
} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::brsu_hmm_eid_messages::obs_conclude_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const  ::brsu_hmm_eid_messages::obs_conclude_<ContainerAllocator> & v) 
  {
    s << indent << "header: ";
s << std::endl;
    Printer< ::std_msgs::Header_<ContainerAllocator> >::stream(s, indent + "  ", v.header);
    s << indent << "base_diagnosis: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.base_diagnosis);
    s << indent << "arm_diagnosis: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.arm_diagnosis);
    s << indent << "gripper_diagnosis: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.gripper_diagnosis);
  }
};


} // namespace message_operations
} // namespace ros

#endif // BRSU_HMM_EID_MESSAGES_MESSAGE_OBS_CONCLUDE_H

