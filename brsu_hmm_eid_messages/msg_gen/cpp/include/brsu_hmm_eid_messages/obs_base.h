/* Auto-generated by genmsg_cpp for file /home/zinnirah/ros/workspace/thesis/brsu_hmm_eid_stack/brsu_hmm_eid_messages/msg/obs_base.msg */
#ifndef BRSU_HMM_EID_MESSAGES_MESSAGE_OBS_BASE_H
#define BRSU_HMM_EID_MESSAGES_MESSAGE_OBS_BASE_H
#include <string>
#include <vector>
#include <map>
#include <ostream>
#include "ros/serialization.h"
#include "ros/builtin_message_traits.h"
#include "ros/message_operations.h"
#include "ros/time.h"

#include "ros/macros.h"

#include "ros/assert.h"

#include "std_msgs/Header.h"

namespace brsu_hmm_eid_messages
{
template <class ContainerAllocator>
struct obs_base_ {
  typedef obs_base_<ContainerAllocator> Type;

  obs_base_()
  : header()
  , x_cmd_vel(0.0)
  , x_vel_diff(0.0)
  , x_acc(0.0)
  , x_jerk(0.0)
  , y_cmd_vel(0.0)
  , y_vel_diff(0.0)
  , y_acc(0.0)
  , y_jerk(0.0)
  {
  }

  obs_base_(const ContainerAllocator& _alloc)
  : header(_alloc)
  , x_cmd_vel(0.0)
  , x_vel_diff(0.0)
  , x_acc(0.0)
  , x_jerk(0.0)
  , y_cmd_vel(0.0)
  , y_vel_diff(0.0)
  , y_acc(0.0)
  , y_jerk(0.0)
  {
  }

  typedef  ::std_msgs::Header_<ContainerAllocator>  _header_type;
   ::std_msgs::Header_<ContainerAllocator>  header;

  typedef double _x_cmd_vel_type;
  double x_cmd_vel;

  typedef double _x_vel_diff_type;
  double x_vel_diff;

  typedef double _x_acc_type;
  double x_acc;

  typedef double _x_jerk_type;
  double x_jerk;

  typedef double _y_cmd_vel_type;
  double y_cmd_vel;

  typedef double _y_vel_diff_type;
  double y_vel_diff;

  typedef double _y_acc_type;
  double y_acc;

  typedef double _y_jerk_type;
  double y_jerk;


  typedef boost::shared_ptr< ::brsu_hmm_eid_messages::obs_base_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::brsu_hmm_eid_messages::obs_base_<ContainerAllocator>  const> ConstPtr;
  boost::shared_ptr<std::map<std::string, std::string> > __connection_header;
}; // struct obs_base
typedef  ::brsu_hmm_eid_messages::obs_base_<std::allocator<void> > obs_base;

typedef boost::shared_ptr< ::brsu_hmm_eid_messages::obs_base> obs_basePtr;
typedef boost::shared_ptr< ::brsu_hmm_eid_messages::obs_base const> obs_baseConstPtr;


template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const  ::brsu_hmm_eid_messages::obs_base_<ContainerAllocator> & v)
{
  ros::message_operations::Printer< ::brsu_hmm_eid_messages::obs_base_<ContainerAllocator> >::stream(s, "", v);
  return s;}

} // namespace brsu_hmm_eid_messages

namespace ros
{
namespace message_traits
{
template<class ContainerAllocator> struct IsMessage< ::brsu_hmm_eid_messages::obs_base_<ContainerAllocator> > : public TrueType {};
template<class ContainerAllocator> struct IsMessage< ::brsu_hmm_eid_messages::obs_base_<ContainerAllocator>  const> : public TrueType {};
template<class ContainerAllocator>
struct MD5Sum< ::brsu_hmm_eid_messages::obs_base_<ContainerAllocator> > {
  static const char* value() 
  {
    return "014af600329d4994f1cdc51981838cfa";
  }

  static const char* value(const  ::brsu_hmm_eid_messages::obs_base_<ContainerAllocator> &) { return value(); } 
  static const uint64_t static_value1 = 0x014af600329d4994ULL;
  static const uint64_t static_value2 = 0xf1cdc51981838cfaULL;
};

template<class ContainerAllocator>
struct DataType< ::brsu_hmm_eid_messages::obs_base_<ContainerAllocator> > {
  static const char* value() 
  {
    return "brsu_hmm_eid_messages/obs_base";
  }

  static const char* value(const  ::brsu_hmm_eid_messages::obs_base_<ContainerAllocator> &) { return value(); } 
};

template<class ContainerAllocator>
struct Definition< ::brsu_hmm_eid_messages::obs_base_<ContainerAllocator> > {
  static const char* value() 
  {
    return "\n\
Header header\n\
float64 x_cmd_vel\n\
float64 x_vel_diff\n\
float64 x_acc\n\
float64 x_jerk\n\
float64 y_cmd_vel\n\
float64 y_vel_diff\n\
float64 y_acc\n\
float64 y_jerk\n\
\n\
================================================================================\n\
MSG: std_msgs/Header\n\
# Standard metadata for higher-level stamped data types.\n\
# This is generally used to communicate timestamped data \n\
# in a particular coordinate frame.\n\
# \n\
# sequence ID: consecutively increasing ID \n\
uint32 seq\n\
#Two-integer timestamp that is expressed as:\n\
# * stamp.secs: seconds (stamp_secs) since epoch\n\
# * stamp.nsecs: nanoseconds since stamp_secs\n\
# time-handling sugar is provided by the client library\n\
time stamp\n\
#Frame this data is associated with\n\
# 0: no frame\n\
# 1: global frame\n\
string frame_id\n\
\n\
";
  }

  static const char* value(const  ::brsu_hmm_eid_messages::obs_base_<ContainerAllocator> &) { return value(); } 
};

template<class ContainerAllocator> struct HasHeader< ::brsu_hmm_eid_messages::obs_base_<ContainerAllocator> > : public TrueType {};
template<class ContainerAllocator> struct HasHeader< const ::brsu_hmm_eid_messages::obs_base_<ContainerAllocator> > : public TrueType {};
} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

template<class ContainerAllocator> struct Serializer< ::brsu_hmm_eid_messages::obs_base_<ContainerAllocator> >
{
  template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
  {
    stream.next(m.header);
    stream.next(m.x_cmd_vel);
    stream.next(m.x_vel_diff);
    stream.next(m.x_acc);
    stream.next(m.x_jerk);
    stream.next(m.y_cmd_vel);
    stream.next(m.y_vel_diff);
    stream.next(m.y_acc);
    stream.next(m.y_jerk);
  }

  ROS_DECLARE_ALLINONE_SERIALIZER;
}; // struct obs_base_
} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::brsu_hmm_eid_messages::obs_base_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const  ::brsu_hmm_eid_messages::obs_base_<ContainerAllocator> & v) 
  {
    s << indent << "header: ";
s << std::endl;
    Printer< ::std_msgs::Header_<ContainerAllocator> >::stream(s, indent + "  ", v.header);
    s << indent << "x_cmd_vel: ";
    Printer<double>::stream(s, indent + "  ", v.x_cmd_vel);
    s << indent << "x_vel_diff: ";
    Printer<double>::stream(s, indent + "  ", v.x_vel_diff);
    s << indent << "x_acc: ";
    Printer<double>::stream(s, indent + "  ", v.x_acc);
    s << indent << "x_jerk: ";
    Printer<double>::stream(s, indent + "  ", v.x_jerk);
    s << indent << "y_cmd_vel: ";
    Printer<double>::stream(s, indent + "  ", v.y_cmd_vel);
    s << indent << "y_vel_diff: ";
    Printer<double>::stream(s, indent + "  ", v.y_vel_diff);
    s << indent << "y_acc: ";
    Printer<double>::stream(s, indent + "  ", v.y_acc);
    s << indent << "y_jerk: ";
    Printer<double>::stream(s, indent + "  ", v.y_jerk);
  }
};


} // namespace message_operations
} // namespace ros

#endif // BRSU_HMM_EID_MESSAGES_MESSAGE_OBS_BASE_H

