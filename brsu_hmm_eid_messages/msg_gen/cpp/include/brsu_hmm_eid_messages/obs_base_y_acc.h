/* Auto-generated by genmsg_cpp for file /home/zinnirah/ros/workspace/thesis/brsu_hmm_eid_stack/brsu_hmm_eid_messages/msg/obs_base_y_acc.msg */
#ifndef BRSU_HMM_EID_MESSAGES_MESSAGE_OBS_BASE_Y_ACC_H
#define BRSU_HMM_EID_MESSAGES_MESSAGE_OBS_BASE_Y_ACC_H
#include <string>
#include <vector>
#include <map>
#include <ostream>
#include "ros/serialization.h"
#include "ros/builtin_message_traits.h"
#include "ros/message_operations.h"
#include "ros/time.h"

#include "ros/macros.h"

#include "ros/assert.h"

#include "std_msgs/Header.h"

namespace brsu_hmm_eid_messages
{
template <class ContainerAllocator>
struct obs_base_y_acc_ {
  typedef obs_base_y_acc_<ContainerAllocator> Type;

  obs_base_y_acc_()
  : header()
  , y_acc(0.0)
  {
  }

  obs_base_y_acc_(const ContainerAllocator& _alloc)
  : header(_alloc)
  , y_acc(0.0)
  {
  }

  typedef  ::std_msgs::Header_<ContainerAllocator>  _header_type;
   ::std_msgs::Header_<ContainerAllocator>  header;

  typedef double _y_acc_type;
  double y_acc;


  typedef boost::shared_ptr< ::brsu_hmm_eid_messages::obs_base_y_acc_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::brsu_hmm_eid_messages::obs_base_y_acc_<ContainerAllocator>  const> ConstPtr;
  boost::shared_ptr<std::map<std::string, std::string> > __connection_header;
}; // struct obs_base_y_acc
typedef  ::brsu_hmm_eid_messages::obs_base_y_acc_<std::allocator<void> > obs_base_y_acc;

typedef boost::shared_ptr< ::brsu_hmm_eid_messages::obs_base_y_acc> obs_base_y_accPtr;
typedef boost::shared_ptr< ::brsu_hmm_eid_messages::obs_base_y_acc const> obs_base_y_accConstPtr;


template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const  ::brsu_hmm_eid_messages::obs_base_y_acc_<ContainerAllocator> & v)
{
  ros::message_operations::Printer< ::brsu_hmm_eid_messages::obs_base_y_acc_<ContainerAllocator> >::stream(s, "", v);
  return s;}

} // namespace brsu_hmm_eid_messages

namespace ros
{
namespace message_traits
{
template<class ContainerAllocator> struct IsMessage< ::brsu_hmm_eid_messages::obs_base_y_acc_<ContainerAllocator> > : public TrueType {};
template<class ContainerAllocator> struct IsMessage< ::brsu_hmm_eid_messages::obs_base_y_acc_<ContainerAllocator>  const> : public TrueType {};
template<class ContainerAllocator>
struct MD5Sum< ::brsu_hmm_eid_messages::obs_base_y_acc_<ContainerAllocator> > {
  static const char* value() 
  {
    return "fbaae85d72421fc779e4c7d467c9841d";
  }

  static const char* value(const  ::brsu_hmm_eid_messages::obs_base_y_acc_<ContainerAllocator> &) { return value(); } 
  static const uint64_t static_value1 = 0xfbaae85d72421fc7ULL;
  static const uint64_t static_value2 = 0x79e4c7d467c9841dULL;
};

template<class ContainerAllocator>
struct DataType< ::brsu_hmm_eid_messages::obs_base_y_acc_<ContainerAllocator> > {
  static const char* value() 
  {
    return "brsu_hmm_eid_messages/obs_base_y_acc";
  }

  static const char* value(const  ::brsu_hmm_eid_messages::obs_base_y_acc_<ContainerAllocator> &) { return value(); } 
};

template<class ContainerAllocator>
struct Definition< ::brsu_hmm_eid_messages::obs_base_y_acc_<ContainerAllocator> > {
  static const char* value() 
  {
    return "\n\
Header header\n\
\n\
float64 y_acc\n\
\n\
\n\
================================================================================\n\
MSG: std_msgs/Header\n\
# Standard metadata for higher-level stamped data types.\n\
# This is generally used to communicate timestamped data \n\
# in a particular coordinate frame.\n\
# \n\
# sequence ID: consecutively increasing ID \n\
uint32 seq\n\
#Two-integer timestamp that is expressed as:\n\
# * stamp.secs: seconds (stamp_secs) since epoch\n\
# * stamp.nsecs: nanoseconds since stamp_secs\n\
# time-handling sugar is provided by the client library\n\
time stamp\n\
#Frame this data is associated with\n\
# 0: no frame\n\
# 1: global frame\n\
string frame_id\n\
\n\
";
  }

  static const char* value(const  ::brsu_hmm_eid_messages::obs_base_y_acc_<ContainerAllocator> &) { return value(); } 
};

template<class ContainerAllocator> struct HasHeader< ::brsu_hmm_eid_messages::obs_base_y_acc_<ContainerAllocator> > : public TrueType {};
template<class ContainerAllocator> struct HasHeader< const ::brsu_hmm_eid_messages::obs_base_y_acc_<ContainerAllocator> > : public TrueType {};
} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

template<class ContainerAllocator> struct Serializer< ::brsu_hmm_eid_messages::obs_base_y_acc_<ContainerAllocator> >
{
  template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
  {
    stream.next(m.header);
    stream.next(m.y_acc);
  }

  ROS_DECLARE_ALLINONE_SERIALIZER;
}; // struct obs_base_y_acc_
} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::brsu_hmm_eid_messages::obs_base_y_acc_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const  ::brsu_hmm_eid_messages::obs_base_y_acc_<ContainerAllocator> & v) 
  {
    s << indent << "header: ";
s << std::endl;
    Printer< ::std_msgs::Header_<ContainerAllocator> >::stream(s, indent + "  ", v.header);
    s << indent << "y_acc: ";
    Printer<double>::stream(s, indent + "  ", v.y_acc);
  }
};


} // namespace message_operations
} // namespace ros

#endif // BRSU_HMM_EID_MESSAGES_MESSAGE_OBS_BASE_Y_ACC_H

