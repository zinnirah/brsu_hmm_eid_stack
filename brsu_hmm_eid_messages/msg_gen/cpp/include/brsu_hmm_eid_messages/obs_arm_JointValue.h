/* Auto-generated by genmsg_cpp for file /home/zinnirah/ros/workspace/thesis/brsu_hmm_eid_stack/brsu_hmm_eid_messages/msg/obs_arm_JointValue.msg */
#ifndef BRSU_HMM_EID_MESSAGES_MESSAGE_OBS_ARM_JOINTVALUE_H
#define BRSU_HMM_EID_MESSAGES_MESSAGE_OBS_ARM_JOINTVALUE_H
#include <string>
#include <vector>
#include <map>
#include <ostream>
#include "ros/serialization.h"
#include "ros/builtin_message_traits.h"
#include "ros/message_operations.h"
#include "ros/time.h"

#include "ros/macros.h"

#include "ros/assert.h"

#include "std_msgs/Header.h"

namespace brsu_hmm_eid_messages
{
template <class ContainerAllocator>
struct obs_arm_JointValue_ {
  typedef obs_arm_JointValue_<ContainerAllocator> Type;

  obs_arm_JointValue_()
  : header()
  , joint_uri()
  , unit()
  , value(0.0)
  {
  }

  obs_arm_JointValue_(const ContainerAllocator& _alloc)
  : header(_alloc)
  , joint_uri(_alloc)
  , unit(_alloc)
  , value(0.0)
  {
  }

  typedef  ::std_msgs::Header_<ContainerAllocator>  _header_type;
   ::std_msgs::Header_<ContainerAllocator>  header;

  typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _joint_uri_type;
  std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  joint_uri;

  typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _unit_type;
  std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  unit;

  typedef double _value_type;
  double value;


  typedef boost::shared_ptr< ::brsu_hmm_eid_messages::obs_arm_JointValue_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::brsu_hmm_eid_messages::obs_arm_JointValue_<ContainerAllocator>  const> ConstPtr;
  boost::shared_ptr<std::map<std::string, std::string> > __connection_header;
}; // struct obs_arm_JointValue
typedef  ::brsu_hmm_eid_messages::obs_arm_JointValue_<std::allocator<void> > obs_arm_JointValue;

typedef boost::shared_ptr< ::brsu_hmm_eid_messages::obs_arm_JointValue> obs_arm_JointValuePtr;
typedef boost::shared_ptr< ::brsu_hmm_eid_messages::obs_arm_JointValue const> obs_arm_JointValueConstPtr;


template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const  ::brsu_hmm_eid_messages::obs_arm_JointValue_<ContainerAllocator> & v)
{
  ros::message_operations::Printer< ::brsu_hmm_eid_messages::obs_arm_JointValue_<ContainerAllocator> >::stream(s, "", v);
  return s;}

} // namespace brsu_hmm_eid_messages

namespace ros
{
namespace message_traits
{
template<class ContainerAllocator> struct IsMessage< ::brsu_hmm_eid_messages::obs_arm_JointValue_<ContainerAllocator> > : public TrueType {};
template<class ContainerAllocator> struct IsMessage< ::brsu_hmm_eid_messages::obs_arm_JointValue_<ContainerAllocator>  const> : public TrueType {};
template<class ContainerAllocator>
struct MD5Sum< ::brsu_hmm_eid_messages::obs_arm_JointValue_<ContainerAllocator> > {
  static const char* value() 
  {
    return "e20935bda0649cf3cc328e353bd312c5";
  }

  static const char* value(const  ::brsu_hmm_eid_messages::obs_arm_JointValue_<ContainerAllocator> &) { return value(); } 
  static const uint64_t static_value1 = 0xe20935bda0649cf3ULL;
  static const uint64_t static_value2 = 0xcc328e353bd312c5ULL;
};

template<class ContainerAllocator>
struct DataType< ::brsu_hmm_eid_messages::obs_arm_JointValue_<ContainerAllocator> > {
  static const char* value() 
  {
    return "brsu_hmm_eid_messages/obs_arm_JointValue";
  }

  static const char* value(const  ::brsu_hmm_eid_messages::obs_arm_JointValue_<ContainerAllocator> &) { return value(); } 
};

template<class ContainerAllocator>
struct Definition< ::brsu_hmm_eid_messages::obs_arm_JointValue_<ContainerAllocator> > {
  static const char* value() 
  {
    return "#time timeStamp 		#time of the data \n\
Header header\n\
string joint_uri\n\
string unit 		#if empy expects si units, you can use boost::unit\n\
float64 value\n\
\n\
================================================================================\n\
MSG: std_msgs/Header\n\
# Standard metadata for higher-level stamped data types.\n\
# This is generally used to communicate timestamped data \n\
# in a particular coordinate frame.\n\
# \n\
# sequence ID: consecutively increasing ID \n\
uint32 seq\n\
#Two-integer timestamp that is expressed as:\n\
# * stamp.secs: seconds (stamp_secs) since epoch\n\
# * stamp.nsecs: nanoseconds since stamp_secs\n\
# time-handling sugar is provided by the client library\n\
time stamp\n\
#Frame this data is associated with\n\
# 0: no frame\n\
# 1: global frame\n\
string frame_id\n\
\n\
";
  }

  static const char* value(const  ::brsu_hmm_eid_messages::obs_arm_JointValue_<ContainerAllocator> &) { return value(); } 
};

template<class ContainerAllocator> struct HasHeader< ::brsu_hmm_eid_messages::obs_arm_JointValue_<ContainerAllocator> > : public TrueType {};
template<class ContainerAllocator> struct HasHeader< const ::brsu_hmm_eid_messages::obs_arm_JointValue_<ContainerAllocator> > : public TrueType {};
} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

template<class ContainerAllocator> struct Serializer< ::brsu_hmm_eid_messages::obs_arm_JointValue_<ContainerAllocator> >
{
  template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
  {
    stream.next(m.header);
    stream.next(m.joint_uri);
    stream.next(m.unit);
    stream.next(m.value);
  }

  ROS_DECLARE_ALLINONE_SERIALIZER;
}; // struct obs_arm_JointValue_
} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::brsu_hmm_eid_messages::obs_arm_JointValue_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const  ::brsu_hmm_eid_messages::obs_arm_JointValue_<ContainerAllocator> & v) 
  {
    s << indent << "header: ";
s << std::endl;
    Printer< ::std_msgs::Header_<ContainerAllocator> >::stream(s, indent + "  ", v.header);
    s << indent << "joint_uri: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.joint_uri);
    s << indent << "unit: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.unit);
    s << indent << "value: ";
    Printer<double>::stream(s, indent + "  ", v.value);
  }
};


} // namespace message_operations
} // namespace ros

#endif // BRSU_HMM_EID_MESSAGES_MESSAGE_OBS_ARM_JOINTVALUE_H

