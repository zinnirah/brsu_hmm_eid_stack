(cl:in-package brsu_hmm_eid_messages-msg)
(cl:export '(HEADER-VAL
          HEADER
          X_CMD_VEL-VAL
          X_CMD_VEL
          X_VEL_DIFF-VAL
          X_VEL_DIFF
          X_ACC-VAL
          X_ACC
          X_JERK-VAL
          X_JERK
          Y_CMD_VEL-VAL
          Y_CMD_VEL
          Y_VEL_DIFF-VAL
          Y_VEL_DIFF
          Y_ACC-VAL
          Y_ACC
          Y_JERK-VAL
          Y_JERK
))