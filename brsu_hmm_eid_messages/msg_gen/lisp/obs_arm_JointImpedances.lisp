; Auto-generated. Do not edit!


(cl:in-package brsu_hmm_eid_messages-msg)


;//! \htmlinclude obs_arm_JointImpedances.msg.html

(cl:defclass <obs_arm_JointImpedances> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (dampings
    :reader dampings
    :initarg :dampings
    :type (cl:vector brsu_hmm_eid_messages-msg:obs_arm_JointValue)
   :initform (cl:make-array 0 :element-type 'brsu_hmm_eid_messages-msg:obs_arm_JointValue :initial-element (cl:make-instance 'brsu_hmm_eid_messages-msg:obs_arm_JointValue)))
   (stiffnesses
    :reader stiffnesses
    :initarg :stiffnesses
    :type (cl:vector brsu_hmm_eid_messages-msg:obs_arm_JointValue)
   :initform (cl:make-array 0 :element-type 'brsu_hmm_eid_messages-msg:obs_arm_JointValue :initial-element (cl:make-instance 'brsu_hmm_eid_messages-msg:obs_arm_JointValue))))
)

(cl:defclass obs_arm_JointImpedances (<obs_arm_JointImpedances>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <obs_arm_JointImpedances>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'obs_arm_JointImpedances)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name brsu_hmm_eid_messages-msg:<obs_arm_JointImpedances> is deprecated: use brsu_hmm_eid_messages-msg:obs_arm_JointImpedances instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <obs_arm_JointImpedances>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader brsu_hmm_eid_messages-msg:header-val is deprecated.  Use brsu_hmm_eid_messages-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'dampings-val :lambda-list '(m))
(cl:defmethod dampings-val ((m <obs_arm_JointImpedances>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader brsu_hmm_eid_messages-msg:dampings-val is deprecated.  Use brsu_hmm_eid_messages-msg:dampings instead.")
  (dampings m))

(cl:ensure-generic-function 'stiffnesses-val :lambda-list '(m))
(cl:defmethod stiffnesses-val ((m <obs_arm_JointImpedances>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader brsu_hmm_eid_messages-msg:stiffnesses-val is deprecated.  Use brsu_hmm_eid_messages-msg:stiffnesses instead.")
  (stiffnesses m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <obs_arm_JointImpedances>) ostream)
  "Serializes a message object of type '<obs_arm_JointImpedances>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'dampings))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'dampings))
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'stiffnesses))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'stiffnesses))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <obs_arm_JointImpedances>) istream)
  "Deserializes a message object of type '<obs_arm_JointImpedances>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'dampings) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'dampings)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'brsu_hmm_eid_messages-msg:obs_arm_JointValue))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'stiffnesses) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'stiffnesses)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'brsu_hmm_eid_messages-msg:obs_arm_JointValue))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<obs_arm_JointImpedances>)))
  "Returns string type for a message object of type '<obs_arm_JointImpedances>"
  "brsu_hmm_eid_messages/obs_arm_JointImpedances")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'obs_arm_JointImpedances)))
  "Returns string type for a message object of type 'obs_arm_JointImpedances"
  "brsu_hmm_eid_messages/obs_arm_JointImpedances")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<obs_arm_JointImpedances>)))
  "Returns md5sum for a message object of type '<obs_arm_JointImpedances>"
  "3af9aac0ff1ed30aaa02e71b2a589f5a")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'obs_arm_JointImpedances)))
  "Returns md5sum for a message object of type 'obs_arm_JointImpedances"
  "3af9aac0ff1ed30aaa02e71b2a589f5a")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<obs_arm_JointImpedances>)))
  "Returns full string definition for message of type '<obs_arm_JointImpedances>"
  (cl:format cl:nil "#Poison poisonStamp~%Header header~%obs_arm_JointValue[] dampings~%obs_arm_JointValue[] stiffnesses~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: brsu_hmm_eid_messages/obs_arm_JointValue~%#time timeStamp 		#time of the data ~%Header header~%string joint_uri~%string unit 		#if empy expects si units, you can use boost::unit~%float64 value~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'obs_arm_JointImpedances)))
  "Returns full string definition for message of type 'obs_arm_JointImpedances"
  (cl:format cl:nil "#Poison poisonStamp~%Header header~%obs_arm_JointValue[] dampings~%obs_arm_JointValue[] stiffnesses~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: brsu_hmm_eid_messages/obs_arm_JointValue~%#time timeStamp 		#time of the data ~%Header header~%string joint_uri~%string unit 		#if empy expects si units, you can use boost::unit~%float64 value~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <obs_arm_JointImpedances>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'dampings) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'stiffnesses) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <obs_arm_JointImpedances>))
  "Converts a ROS message object to a list"
  (cl:list 'obs_arm_JointImpedances
    (cl:cons ':header (header msg))
    (cl:cons ':dampings (dampings msg))
    (cl:cons ':stiffnesses (stiffnesses msg))
))
