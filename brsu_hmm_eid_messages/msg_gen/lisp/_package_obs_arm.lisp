(cl:in-package brsu_hmm_eid_messages-msg)
(cl:export '(HEADER-VAL
          HEADER
          JNAME-VAL
          JNAME
          JACT_POSITION-VAL
          JACT_POSITION
          JACT_VEL-VAL
          JACT_VEL
          JACC-VAL
          JACC
          JJERK-VAL
          JJERK
          JACT_EFFORT-VAL
          JACT_EFFORT
))