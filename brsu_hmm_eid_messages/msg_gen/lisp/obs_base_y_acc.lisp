; Auto-generated. Do not edit!


(cl:in-package brsu_hmm_eid_messages-msg)


;//! \htmlinclude obs_base_y_acc.msg.html

(cl:defclass <obs_base_y_acc> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (y_acc
    :reader y_acc
    :initarg :y_acc
    :type cl:float
    :initform 0.0))
)

(cl:defclass obs_base_y_acc (<obs_base_y_acc>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <obs_base_y_acc>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'obs_base_y_acc)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name brsu_hmm_eid_messages-msg:<obs_base_y_acc> is deprecated: use brsu_hmm_eid_messages-msg:obs_base_y_acc instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <obs_base_y_acc>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader brsu_hmm_eid_messages-msg:header-val is deprecated.  Use brsu_hmm_eid_messages-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'y_acc-val :lambda-list '(m))
(cl:defmethod y_acc-val ((m <obs_base_y_acc>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader brsu_hmm_eid_messages-msg:y_acc-val is deprecated.  Use brsu_hmm_eid_messages-msg:y_acc instead.")
  (y_acc m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <obs_base_y_acc>) ostream)
  "Serializes a message object of type '<obs_base_y_acc>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'y_acc))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <obs_base_y_acc>) istream)
  "Deserializes a message object of type '<obs_base_y_acc>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'y_acc) (roslisp-utils:decode-double-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<obs_base_y_acc>)))
  "Returns string type for a message object of type '<obs_base_y_acc>"
  "brsu_hmm_eid_messages/obs_base_y_acc")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'obs_base_y_acc)))
  "Returns string type for a message object of type 'obs_base_y_acc"
  "brsu_hmm_eid_messages/obs_base_y_acc")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<obs_base_y_acc>)))
  "Returns md5sum for a message object of type '<obs_base_y_acc>"
  "fbaae85d72421fc779e4c7d467c9841d")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'obs_base_y_acc)))
  "Returns md5sum for a message object of type 'obs_base_y_acc"
  "fbaae85d72421fc779e4c7d467c9841d")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<obs_base_y_acc>)))
  "Returns full string definition for message of type '<obs_base_y_acc>"
  (cl:format cl:nil "~%Header header~%~%float64 y_acc~%~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'obs_base_y_acc)))
  "Returns full string definition for message of type 'obs_base_y_acc"
  (cl:format cl:nil "~%Header header~%~%float64 y_acc~%~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <obs_base_y_acc>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <obs_base_y_acc>))
  "Converts a ROS message object to a list"
  (cl:list 'obs_base_y_acc
    (cl:cons ':header (header msg))
    (cl:cons ':y_acc (y_acc msg))
))
