(cl:in-package brsu_hmm_eid_messages-msg)
(cl:export '(HEADER-VAL
          HEADER
          DIAGNOSIS_ARM-VAL
          DIAGNOSIS_ARM
          DIAGNOSIS_ARM_NO-VAL
          DIAGNOSIS_ARM_NO
          PROB_DIST-VAL
          PROB_DIST
))