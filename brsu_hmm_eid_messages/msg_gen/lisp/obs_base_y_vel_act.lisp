; Auto-generated. Do not edit!


(cl:in-package brsu_hmm_eid_messages-msg)


;//! \htmlinclude obs_base_y_vel_act.msg.html

(cl:defclass <obs_base_y_vel_act> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (y_vel_act
    :reader y_vel_act
    :initarg :y_vel_act
    :type cl:float
    :initform 0.0))
)

(cl:defclass obs_base_y_vel_act (<obs_base_y_vel_act>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <obs_base_y_vel_act>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'obs_base_y_vel_act)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name brsu_hmm_eid_messages-msg:<obs_base_y_vel_act> is deprecated: use brsu_hmm_eid_messages-msg:obs_base_y_vel_act instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <obs_base_y_vel_act>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader brsu_hmm_eid_messages-msg:header-val is deprecated.  Use brsu_hmm_eid_messages-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'y_vel_act-val :lambda-list '(m))
(cl:defmethod y_vel_act-val ((m <obs_base_y_vel_act>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader brsu_hmm_eid_messages-msg:y_vel_act-val is deprecated.  Use brsu_hmm_eid_messages-msg:y_vel_act instead.")
  (y_vel_act m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <obs_base_y_vel_act>) ostream)
  "Serializes a message object of type '<obs_base_y_vel_act>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'y_vel_act))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <obs_base_y_vel_act>) istream)
  "Deserializes a message object of type '<obs_base_y_vel_act>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'y_vel_act) (roslisp-utils:decode-double-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<obs_base_y_vel_act>)))
  "Returns string type for a message object of type '<obs_base_y_vel_act>"
  "brsu_hmm_eid_messages/obs_base_y_vel_act")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'obs_base_y_vel_act)))
  "Returns string type for a message object of type 'obs_base_y_vel_act"
  "brsu_hmm_eid_messages/obs_base_y_vel_act")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<obs_base_y_vel_act>)))
  "Returns md5sum for a message object of type '<obs_base_y_vel_act>"
  "2c1c2f6238d093c0b243d3324ae69688")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'obs_base_y_vel_act)))
  "Returns md5sum for a message object of type 'obs_base_y_vel_act"
  "2c1c2f6238d093c0b243d3324ae69688")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<obs_base_y_vel_act>)))
  "Returns full string definition for message of type '<obs_base_y_vel_act>"
  (cl:format cl:nil "~%Header header~%~%float64 y_vel_act~%~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'obs_base_y_vel_act)))
  "Returns full string definition for message of type 'obs_base_y_vel_act"
  (cl:format cl:nil "~%Header header~%~%float64 y_vel_act~%~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <obs_base_y_vel_act>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <obs_base_y_vel_act>))
  "Converts a ROS message object to a list"
  (cl:list 'obs_base_y_vel_act
    (cl:cons ':header (header msg))
    (cl:cons ':y_vel_act (y_vel_act msg))
))
