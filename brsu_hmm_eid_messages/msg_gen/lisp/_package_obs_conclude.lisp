(cl:in-package brsu_hmm_eid_messages-msg)
(cl:export '(HEADER-VAL
          HEADER
          BASE_DIAGNOSIS-VAL
          BASE_DIAGNOSIS
          ARM_DIAGNOSIS-VAL
          ARM_DIAGNOSIS
          GRIPPER_DIAGNOSIS-VAL
          GRIPPER_DIAGNOSIS
))