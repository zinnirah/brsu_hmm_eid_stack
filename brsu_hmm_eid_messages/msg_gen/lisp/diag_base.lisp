; Auto-generated. Do not edit!


(cl:in-package brsu_hmm_eid_messages-msg)


;//! \htmlinclude diag_base.msg.html

(cl:defclass <diag_base> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (diagnosis_base
    :reader diagnosis_base
    :initarg :diagnosis_base
    :type cl:string
    :initform "")
   (diagnosis_base_no
    :reader diagnosis_base_no
    :initarg :diagnosis_base_no
    :type cl:float
    :initform 0.0)
   (prob_dist
    :reader prob_dist
    :initarg :prob_dist
    :type (cl:vector cl:float)
   :initform (cl:make-array 0 :element-type 'cl:float :initial-element 0.0)))
)

(cl:defclass diag_base (<diag_base>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <diag_base>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'diag_base)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name brsu_hmm_eid_messages-msg:<diag_base> is deprecated: use brsu_hmm_eid_messages-msg:diag_base instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <diag_base>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader brsu_hmm_eid_messages-msg:header-val is deprecated.  Use brsu_hmm_eid_messages-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'diagnosis_base-val :lambda-list '(m))
(cl:defmethod diagnosis_base-val ((m <diag_base>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader brsu_hmm_eid_messages-msg:diagnosis_base-val is deprecated.  Use brsu_hmm_eid_messages-msg:diagnosis_base instead.")
  (diagnosis_base m))

(cl:ensure-generic-function 'diagnosis_base_no-val :lambda-list '(m))
(cl:defmethod diagnosis_base_no-val ((m <diag_base>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader brsu_hmm_eid_messages-msg:diagnosis_base_no-val is deprecated.  Use brsu_hmm_eid_messages-msg:diagnosis_base_no instead.")
  (diagnosis_base_no m))

(cl:ensure-generic-function 'prob_dist-val :lambda-list '(m))
(cl:defmethod prob_dist-val ((m <diag_base>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader brsu_hmm_eid_messages-msg:prob_dist-val is deprecated.  Use brsu_hmm_eid_messages-msg:prob_dist instead.")
  (prob_dist m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <diag_base>) ostream)
  "Serializes a message object of type '<diag_base>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'diagnosis_base))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'diagnosis_base))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'diagnosis_base_no))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'prob_dist))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let ((bits (roslisp-utils:encode-double-float-bits ele)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream)))
   (cl:slot-value msg 'prob_dist))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <diag_base>) istream)
  "Deserializes a message object of type '<diag_base>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'diagnosis_base) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'diagnosis_base) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'diagnosis_base_no) (roslisp-utils:decode-double-float-bits bits)))
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'prob_dist) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'prob_dist)))
    (cl:dotimes (i __ros_arr_len)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:aref vals i) (roslisp-utils:decode-double-float-bits bits))))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<diag_base>)))
  "Returns string type for a message object of type '<diag_base>"
  "brsu_hmm_eid_messages/diag_base")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'diag_base)))
  "Returns string type for a message object of type 'diag_base"
  "brsu_hmm_eid_messages/diag_base")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<diag_base>)))
  "Returns md5sum for a message object of type '<diag_base>"
  "65150177668d13c4c268201f1601142e")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'diag_base)))
  "Returns md5sum for a message object of type 'diag_base"
  "65150177668d13c4c268201f1601142e")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<diag_base>)))
  "Returns full string definition for message of type '<diag_base>"
  (cl:format cl:nil "~%Header header~%string diagnosis_base~%float64 diagnosis_base_no~%float64[] prob_dist~%~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'diag_base)))
  "Returns full string definition for message of type 'diag_base"
  (cl:format cl:nil "~%Header header~%string diagnosis_base~%float64 diagnosis_base_no~%float64[] prob_dist~%~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <diag_base>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4 (cl:length (cl:slot-value msg 'diagnosis_base))
     8
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'prob_dist) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 8)))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <diag_base>))
  "Converts a ROS message object to a list"
  (cl:list 'diag_base
    (cl:cons ':header (header msg))
    (cl:cons ':diagnosis_base (diagnosis_base msg))
    (cl:cons ':diagnosis_base_no (diagnosis_base_no msg))
    (cl:cons ':prob_dist (prob_dist msg))
))
