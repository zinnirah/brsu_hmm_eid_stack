(cl:in-package brsu_hmm_eid_messages-msg)
(cl:export '(HEADER-VAL
          HEADER
          DIAGNOSIS_BASE-VAL
          DIAGNOSIS_BASE
          DIAGNOSIS_BASE_NO-VAL
          DIAGNOSIS_BASE_NO
          PROB_DIST-VAL
          PROB_DIST
))