(cl:in-package brsu_hmm_eid_messages-msg)
(cl:export '(HEADER-VAL
          HEADER
          DIAGNOSIS_FINAL-VAL
          DIAGNOSIS_FINAL
          DIAGNOSIS_FINAL_NO-VAL
          DIAGNOSIS_FINAL_NO
          PROB_DIST-VAL
          PROB_DIST
))