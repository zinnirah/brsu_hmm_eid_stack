; Auto-generated. Do not edit!


(cl:in-package brsu_hmm_eid_messages-msg)


;//! \htmlinclude obs_gripper_status.msg.html

(cl:defclass <obs_gripper_status> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (gripper_status
    :reader gripper_status
    :initarg :gripper_status
    :type cl:fixnum
    :initform 0))
)

(cl:defclass obs_gripper_status (<obs_gripper_status>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <obs_gripper_status>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'obs_gripper_status)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name brsu_hmm_eid_messages-msg:<obs_gripper_status> is deprecated: use brsu_hmm_eid_messages-msg:obs_gripper_status instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <obs_gripper_status>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader brsu_hmm_eid_messages-msg:header-val is deprecated.  Use brsu_hmm_eid_messages-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'gripper_status-val :lambda-list '(m))
(cl:defmethod gripper_status-val ((m <obs_gripper_status>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader brsu_hmm_eid_messages-msg:gripper_status-val is deprecated.  Use brsu_hmm_eid_messages-msg:gripper_status instead.")
  (gripper_status m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <obs_gripper_status>) ostream)
  "Serializes a message object of type '<obs_gripper_status>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let* ((signed (cl:slot-value msg 'gripper_status)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 256) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <obs_gripper_status>) istream)
  "Deserializes a message object of type '<obs_gripper_status>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'gripper_status) (cl:if (cl:< unsigned 128) unsigned (cl:- unsigned 256))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<obs_gripper_status>)))
  "Returns string type for a message object of type '<obs_gripper_status>"
  "brsu_hmm_eid_messages/obs_gripper_status")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'obs_gripper_status)))
  "Returns string type for a message object of type 'obs_gripper_status"
  "brsu_hmm_eid_messages/obs_gripper_status")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<obs_gripper_status>)))
  "Returns md5sum for a message object of type '<obs_gripper_status>"
  "bfd1a49cc6ddb4e1571e37138a54b61d")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'obs_gripper_status)))
  "Returns md5sum for a message object of type 'obs_gripper_status"
  "bfd1a49cc6ddb4e1571e37138a54b61d")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<obs_gripper_status>)))
  "Returns full string definition for message of type '<obs_gripper_status>"
  (cl:format cl:nil "Header header~%int8 gripper_status~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'obs_gripper_status)))
  "Returns full string definition for message of type 'obs_gripper_status"
  (cl:format cl:nil "Header header~%int8 gripper_status~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <obs_gripper_status>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <obs_gripper_status>))
  "Converts a ROS message object to a list"
  (cl:list 'obs_gripper_status
    (cl:cons ':header (header msg))
    (cl:cons ':gripper_status (gripper_status msg))
))
