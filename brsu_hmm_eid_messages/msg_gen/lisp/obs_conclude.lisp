; Auto-generated. Do not edit!


(cl:in-package brsu_hmm_eid_messages-msg)


;//! \htmlinclude obs_conclude.msg.html

(cl:defclass <obs_conclude> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (base_diagnosis
    :reader base_diagnosis
    :initarg :base_diagnosis
    :type cl:string
    :initform "")
   (arm_diagnosis
    :reader arm_diagnosis
    :initarg :arm_diagnosis
    :type cl:string
    :initform "")
   (gripper_diagnosis
    :reader gripper_diagnosis
    :initarg :gripper_diagnosis
    :type cl:string
    :initform ""))
)

(cl:defclass obs_conclude (<obs_conclude>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <obs_conclude>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'obs_conclude)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name brsu_hmm_eid_messages-msg:<obs_conclude> is deprecated: use brsu_hmm_eid_messages-msg:obs_conclude instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <obs_conclude>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader brsu_hmm_eid_messages-msg:header-val is deprecated.  Use brsu_hmm_eid_messages-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'base_diagnosis-val :lambda-list '(m))
(cl:defmethod base_diagnosis-val ((m <obs_conclude>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader brsu_hmm_eid_messages-msg:base_diagnosis-val is deprecated.  Use brsu_hmm_eid_messages-msg:base_diagnosis instead.")
  (base_diagnosis m))

(cl:ensure-generic-function 'arm_diagnosis-val :lambda-list '(m))
(cl:defmethod arm_diagnosis-val ((m <obs_conclude>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader brsu_hmm_eid_messages-msg:arm_diagnosis-val is deprecated.  Use brsu_hmm_eid_messages-msg:arm_diagnosis instead.")
  (arm_diagnosis m))

(cl:ensure-generic-function 'gripper_diagnosis-val :lambda-list '(m))
(cl:defmethod gripper_diagnosis-val ((m <obs_conclude>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader brsu_hmm_eid_messages-msg:gripper_diagnosis-val is deprecated.  Use brsu_hmm_eid_messages-msg:gripper_diagnosis instead.")
  (gripper_diagnosis m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <obs_conclude>) ostream)
  "Serializes a message object of type '<obs_conclude>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'base_diagnosis))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'base_diagnosis))
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'arm_diagnosis))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'arm_diagnosis))
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'gripper_diagnosis))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'gripper_diagnosis))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <obs_conclude>) istream)
  "Deserializes a message object of type '<obs_conclude>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'base_diagnosis) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'base_diagnosis) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'arm_diagnosis) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'arm_diagnosis) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'gripper_diagnosis) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'gripper_diagnosis) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<obs_conclude>)))
  "Returns string type for a message object of type '<obs_conclude>"
  "brsu_hmm_eid_messages/obs_conclude")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'obs_conclude)))
  "Returns string type for a message object of type 'obs_conclude"
  "brsu_hmm_eid_messages/obs_conclude")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<obs_conclude>)))
  "Returns md5sum for a message object of type '<obs_conclude>"
  "e8a5b05882c8f89b91ac24e01c2bc5bb")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'obs_conclude)))
  "Returns md5sum for a message object of type 'obs_conclude"
  "e8a5b05882c8f89b91ac24e01c2bc5bb")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<obs_conclude>)))
  "Returns full string definition for message of type '<obs_conclude>"
  (cl:format cl:nil "Header header~%~%string base_diagnosis~%string arm_diagnosis~%string gripper_diagnosis~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'obs_conclude)))
  "Returns full string definition for message of type 'obs_conclude"
  (cl:format cl:nil "Header header~%~%string base_diagnosis~%string arm_diagnosis~%string gripper_diagnosis~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <obs_conclude>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4 (cl:length (cl:slot-value msg 'base_diagnosis))
     4 (cl:length (cl:slot-value msg 'arm_diagnosis))
     4 (cl:length (cl:slot-value msg 'gripper_diagnosis))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <obs_conclude>))
  "Converts a ROS message object to a list"
  (cl:list 'obs_conclude
    (cl:cons ':header (header msg))
    (cl:cons ':base_diagnosis (base_diagnosis msg))
    (cl:cons ':arm_diagnosis (arm_diagnosis msg))
    (cl:cons ':gripper_diagnosis (gripper_diagnosis msg))
))
