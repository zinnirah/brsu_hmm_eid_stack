; Auto-generated. Do not edit!


(cl:in-package brsu_hmm_eid_messages-msg)


;//! \htmlinclude obs_gripper_pos_diff.msg.html

(cl:defclass <obs_gripper_pos_diff> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (gripper_pos_diff
    :reader gripper_pos_diff
    :initarg :gripper_pos_diff
    :type (cl:vector cl:float)
   :initform (cl:make-array 0 :element-type 'cl:float :initial-element 0.0)))
)

(cl:defclass obs_gripper_pos_diff (<obs_gripper_pos_diff>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <obs_gripper_pos_diff>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'obs_gripper_pos_diff)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name brsu_hmm_eid_messages-msg:<obs_gripper_pos_diff> is deprecated: use brsu_hmm_eid_messages-msg:obs_gripper_pos_diff instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <obs_gripper_pos_diff>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader brsu_hmm_eid_messages-msg:header-val is deprecated.  Use brsu_hmm_eid_messages-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'gripper_pos_diff-val :lambda-list '(m))
(cl:defmethod gripper_pos_diff-val ((m <obs_gripper_pos_diff>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader brsu_hmm_eid_messages-msg:gripper_pos_diff-val is deprecated.  Use brsu_hmm_eid_messages-msg:gripper_pos_diff instead.")
  (gripper_pos_diff m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <obs_gripper_pos_diff>) ostream)
  "Serializes a message object of type '<obs_gripper_pos_diff>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'gripper_pos_diff))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let ((bits (roslisp-utils:encode-double-float-bits ele)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream)))
   (cl:slot-value msg 'gripper_pos_diff))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <obs_gripper_pos_diff>) istream)
  "Deserializes a message object of type '<obs_gripper_pos_diff>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'gripper_pos_diff) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'gripper_pos_diff)))
    (cl:dotimes (i __ros_arr_len)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:aref vals i) (roslisp-utils:decode-double-float-bits bits))))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<obs_gripper_pos_diff>)))
  "Returns string type for a message object of type '<obs_gripper_pos_diff>"
  "brsu_hmm_eid_messages/obs_gripper_pos_diff")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'obs_gripper_pos_diff)))
  "Returns string type for a message object of type 'obs_gripper_pos_diff"
  "brsu_hmm_eid_messages/obs_gripper_pos_diff")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<obs_gripper_pos_diff>)))
  "Returns md5sum for a message object of type '<obs_gripper_pos_diff>"
  "4604aa0fc0ba60749d15cbcbace63d5e")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'obs_gripper_pos_diff)))
  "Returns md5sum for a message object of type 'obs_gripper_pos_diff"
  "4604aa0fc0ba60749d15cbcbace63d5e")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<obs_gripper_pos_diff>)))
  "Returns full string definition for message of type '<obs_gripper_pos_diff>"
  (cl:format cl:nil "Header header~%~%float64[] gripper_pos_diff~%~%~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'obs_gripper_pos_diff)))
  "Returns full string definition for message of type 'obs_gripper_pos_diff"
  (cl:format cl:nil "Header header~%~%float64[] gripper_pos_diff~%~%~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <obs_gripper_pos_diff>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'gripper_pos_diff) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 8)))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <obs_gripper_pos_diff>))
  "Converts a ROS message object to a list"
  (cl:list 'obs_gripper_pos_diff
    (cl:cons ':header (header msg))
    (cl:cons ':gripper_pos_diff (gripper_pos_diff msg))
))
