; Auto-generated. Do not edit!


(cl:in-package brsu_hmm_eid_messages-msg)


;//! \htmlinclude obs_arm_JointConstraint.msg.html

(cl:defclass <obs_arm_JointConstraint> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (type
    :reader type
    :initarg :type
    :type cl:string
    :initform "")
   (value
    :reader value
    :initarg :value
    :type brsu_hmm_eid_messages-msg:obs_arm_JointValue
    :initform (cl:make-instance 'brsu_hmm_eid_messages-msg:obs_arm_JointValue)))
)

(cl:defclass obs_arm_JointConstraint (<obs_arm_JointConstraint>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <obs_arm_JointConstraint>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'obs_arm_JointConstraint)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name brsu_hmm_eid_messages-msg:<obs_arm_JointConstraint> is deprecated: use brsu_hmm_eid_messages-msg:obs_arm_JointConstraint instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <obs_arm_JointConstraint>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader brsu_hmm_eid_messages-msg:header-val is deprecated.  Use brsu_hmm_eid_messages-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'type-val :lambda-list '(m))
(cl:defmethod type-val ((m <obs_arm_JointConstraint>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader brsu_hmm_eid_messages-msg:type-val is deprecated.  Use brsu_hmm_eid_messages-msg:type instead.")
  (type m))

(cl:ensure-generic-function 'value-val :lambda-list '(m))
(cl:defmethod value-val ((m <obs_arm_JointConstraint>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader brsu_hmm_eid_messages-msg:value-val is deprecated.  Use brsu_hmm_eid_messages-msg:value instead.")
  (value m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <obs_arm_JointConstraint>) ostream)
  "Serializes a message object of type '<obs_arm_JointConstraint>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'type))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'type))
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'value) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <obs_arm_JointConstraint>) istream)
  "Deserializes a message object of type '<obs_arm_JointConstraint>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'type) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'type) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'value) istream)
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<obs_arm_JointConstraint>)))
  "Returns string type for a message object of type '<obs_arm_JointConstraint>"
  "brsu_hmm_eid_messages/obs_arm_JointConstraint")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'obs_arm_JointConstraint)))
  "Returns string type for a message object of type 'obs_arm_JointConstraint"
  "brsu_hmm_eid_messages/obs_arm_JointConstraint")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<obs_arm_JointConstraint>)))
  "Returns md5sum for a message object of type '<obs_arm_JointConstraint>"
  "cd395dc7e2140a895c7904508e75ab9b")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'obs_arm_JointConstraint)))
  "Returns md5sum for a message object of type 'obs_arm_JointConstraint"
  "cd395dc7e2140a895c7904508e75ab9b")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<obs_arm_JointConstraint>)))
  "Returns full string definition for message of type '<obs_arm_JointConstraint>"
  (cl:format cl:nil "Header header~%string type  		#smaller, greater, equal or <, >, =~%obs_arm_JointValue value~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: brsu_hmm_eid_messages/obs_arm_JointValue~%#time timeStamp 		#time of the data ~%Header header~%string joint_uri~%string unit 		#if empy expects si units, you can use boost::unit~%float64 value~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'obs_arm_JointConstraint)))
  "Returns full string definition for message of type 'obs_arm_JointConstraint"
  (cl:format cl:nil "Header header~%string type  		#smaller, greater, equal or <, >, =~%obs_arm_JointValue value~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: brsu_hmm_eid_messages/obs_arm_JointValue~%#time timeStamp 		#time of the data ~%Header header~%string joint_uri~%string unit 		#if empy expects si units, you can use boost::unit~%float64 value~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <obs_arm_JointConstraint>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4 (cl:length (cl:slot-value msg 'type))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'value))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <obs_arm_JointConstraint>))
  "Converts a ROS message object to a list"
  (cl:list 'obs_arm_JointConstraint
    (cl:cons ':header (header msg))
    (cl:cons ':type (type msg))
    (cl:cons ':value (value msg))
))
