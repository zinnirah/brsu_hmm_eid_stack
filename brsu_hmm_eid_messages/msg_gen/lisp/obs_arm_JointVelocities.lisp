; Auto-generated. Do not edit!


(cl:in-package brsu_hmm_eid_messages-msg)


;//! \htmlinclude obs_arm_JointVelocities.msg.html

(cl:defclass <obs_arm_JointVelocities> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (velocities
    :reader velocities
    :initarg :velocities
    :type (cl:vector cl:float)
   :initform (cl:make-array 0 :element-type 'cl:float :initial-element 0.0)))
)

(cl:defclass obs_arm_JointVelocities (<obs_arm_JointVelocities>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <obs_arm_JointVelocities>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'obs_arm_JointVelocities)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name brsu_hmm_eid_messages-msg:<obs_arm_JointVelocities> is deprecated: use brsu_hmm_eid_messages-msg:obs_arm_JointVelocities instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <obs_arm_JointVelocities>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader brsu_hmm_eid_messages-msg:header-val is deprecated.  Use brsu_hmm_eid_messages-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'velocities-val :lambda-list '(m))
(cl:defmethod velocities-val ((m <obs_arm_JointVelocities>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader brsu_hmm_eid_messages-msg:velocities-val is deprecated.  Use brsu_hmm_eid_messages-msg:velocities instead.")
  (velocities m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <obs_arm_JointVelocities>) ostream)
  "Serializes a message object of type '<obs_arm_JointVelocities>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'velocities))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let ((bits (roslisp-utils:encode-double-float-bits ele)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream)))
   (cl:slot-value msg 'velocities))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <obs_arm_JointVelocities>) istream)
  "Deserializes a message object of type '<obs_arm_JointVelocities>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'velocities) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'velocities)))
    (cl:dotimes (i __ros_arr_len)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:aref vals i) (roslisp-utils:decode-double-float-bits bits))))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<obs_arm_JointVelocities>)))
  "Returns string type for a message object of type '<obs_arm_JointVelocities>"
  "brsu_hmm_eid_messages/obs_arm_JointVelocities")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'obs_arm_JointVelocities)))
  "Returns string type for a message object of type 'obs_arm_JointVelocities"
  "brsu_hmm_eid_messages/obs_arm_JointVelocities")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<obs_arm_JointVelocities>)))
  "Returns md5sum for a message object of type '<obs_arm_JointVelocities>"
  "463170b3bd12ca54e910f60078a94e72")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'obs_arm_JointVelocities)))
  "Returns md5sum for a message object of type 'obs_arm_JointVelocities"
  "463170b3bd12ca54e910f60078a94e72")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<obs_arm_JointVelocities>)))
  "Returns full string definition for message of type '<obs_arm_JointVelocities>"
  (cl:format cl:nil "#Poison poisonStamp~%Header header~%float64[] velocities~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'obs_arm_JointVelocities)))
  "Returns full string definition for message of type 'obs_arm_JointVelocities"
  (cl:format cl:nil "#Poison poisonStamp~%Header header~%float64[] velocities~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <obs_arm_JointVelocities>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'velocities) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 8)))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <obs_arm_JointVelocities>))
  "Converts a ROS message object to a list"
  (cl:list 'obs_arm_JointVelocities
    (cl:cons ':header (header msg))
    (cl:cons ':velocities (velocities msg))
))
