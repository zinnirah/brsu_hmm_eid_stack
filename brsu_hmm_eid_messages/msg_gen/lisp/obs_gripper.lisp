; Auto-generated. Do not edit!


(cl:in-package brsu_hmm_eid_messages-msg)


;//! \htmlinclude obs_gripper.msg.html

(cl:defclass <obs_gripper> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (cmd_finger_position
    :reader cmd_finger_position
    :initarg :cmd_finger_position
    :type (cl:vector cl:float)
   :initform (cl:make-array 0 :element-type 'cl:float :initial-element 0.0))
   (act_finger_position
    :reader act_finger_position
    :initarg :act_finger_position
    :type (cl:vector cl:float)
   :initform (cl:make-array 0 :element-type 'cl:float :initial-element 0.0))
   (grasping_status
    :reader grasping_status
    :initarg :grasping_status
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass obs_gripper (<obs_gripper>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <obs_gripper>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'obs_gripper)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name brsu_hmm_eid_messages-msg:<obs_gripper> is deprecated: use brsu_hmm_eid_messages-msg:obs_gripper instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <obs_gripper>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader brsu_hmm_eid_messages-msg:header-val is deprecated.  Use brsu_hmm_eid_messages-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'cmd_finger_position-val :lambda-list '(m))
(cl:defmethod cmd_finger_position-val ((m <obs_gripper>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader brsu_hmm_eid_messages-msg:cmd_finger_position-val is deprecated.  Use brsu_hmm_eid_messages-msg:cmd_finger_position instead.")
  (cmd_finger_position m))

(cl:ensure-generic-function 'act_finger_position-val :lambda-list '(m))
(cl:defmethod act_finger_position-val ((m <obs_gripper>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader brsu_hmm_eid_messages-msg:act_finger_position-val is deprecated.  Use brsu_hmm_eid_messages-msg:act_finger_position instead.")
  (act_finger_position m))

(cl:ensure-generic-function 'grasping_status-val :lambda-list '(m))
(cl:defmethod grasping_status-val ((m <obs_gripper>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader brsu_hmm_eid_messages-msg:grasping_status-val is deprecated.  Use brsu_hmm_eid_messages-msg:grasping_status instead.")
  (grasping_status m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <obs_gripper>) ostream)
  "Serializes a message object of type '<obs_gripper>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'cmd_finger_position))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let ((bits (roslisp-utils:encode-double-float-bits ele)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream)))
   (cl:slot-value msg 'cmd_finger_position))
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'act_finger_position))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let ((bits (roslisp-utils:encode-double-float-bits ele)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream)))
   (cl:slot-value msg 'act_finger_position))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'grasping_status) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <obs_gripper>) istream)
  "Deserializes a message object of type '<obs_gripper>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'cmd_finger_position) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'cmd_finger_position)))
    (cl:dotimes (i __ros_arr_len)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:aref vals i) (roslisp-utils:decode-double-float-bits bits))))))
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'act_finger_position) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'act_finger_position)))
    (cl:dotimes (i __ros_arr_len)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:aref vals i) (roslisp-utils:decode-double-float-bits bits))))))
    (cl:setf (cl:slot-value msg 'grasping_status) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<obs_gripper>)))
  "Returns string type for a message object of type '<obs_gripper>"
  "brsu_hmm_eid_messages/obs_gripper")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'obs_gripper)))
  "Returns string type for a message object of type 'obs_gripper"
  "brsu_hmm_eid_messages/obs_gripper")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<obs_gripper>)))
  "Returns md5sum for a message object of type '<obs_gripper>"
  "a8e62d77bd7c938f9c8d5565a573a429")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'obs_gripper)))
  "Returns md5sum for a message object of type 'obs_gripper"
  "a8e62d77bd7c938f9c8d5565a573a429")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<obs_gripper>)))
  "Returns full string definition for message of type '<obs_gripper>"
  (cl:format cl:nil "Header header~%~%float64[] cmd_finger_position~%float64[] act_finger_position~%bool grasping_status~%~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'obs_gripper)))
  "Returns full string definition for message of type 'obs_gripper"
  (cl:format cl:nil "Header header~%~%float64[] cmd_finger_position~%float64[] act_finger_position~%bool grasping_status~%~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.secs: seconds (stamp_secs) since epoch~%# * stamp.nsecs: nanoseconds since stamp_secs~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <obs_gripper>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'cmd_finger_position) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 8)))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'act_finger_position) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 8)))
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <obs_gripper>))
  "Converts a ROS message object to a list"
  (cl:list 'obs_gripper
    (cl:cons ':header (header msg))
    (cl:cons ':cmd_finger_position (cmd_finger_position msg))
    (cl:cons ':act_finger_position (act_finger_position msg))
    (cl:cons ':grasping_status (grasping_status msg))
))
