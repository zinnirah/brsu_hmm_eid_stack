(cl:in-package brsu_hmm_eid_messages-msg)
(cl:export '(HEADER-VAL
          HEADER
          CMD_FINGER_POSITION-VAL
          CMD_FINGER_POSITION
          ACT_FINGER_POSITION-VAL
          ACT_FINGER_POSITION
          GRASPING_STATUS-VAL
          GRASPING_STATUS
))